// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Dialog } from '../ui/dialog';
import { FileListing } from '../ui/file_listing';
import { FileSystem } from '../file_system';

/**
 * Represents the file copy dialog.
 */
export class NewFileDialog extends Dialog {
    constructor(fileInfo) {
        super('dialog-new-file');

        this.open();

        // Update the input field to be new-file.s and selected
        this._input = this.dialog.querySelector('#dialog-new-file-name');
        this._input.value = "new-file.s";

        // Get references cancel/create buttons
        this._cancelButton = this.dialog.querySelector("#button-cancel-create");
        this._createButton = this.dialog.querySelector("#button-create-new-file");

        // Bind the specific events for the dialog
        this.bindEvents();

        // Focus on the input field for the file name
        window.requestAnimationFrame( () => {
            this._input.focus();
            this._input.setSelectionRange(0, 8);
        });
    }

    /**
     * Binds events to the dialog to handle the file create and close buttons.
     *
     * This is called internally when the dialog is created.
     */
    bindEvents() {
        // Bail if this was called before
        if (this.dialog.classList.contains("bound")) {
            return;
        }

        // Do not allow the events to be bound twice
        this.dialog.classList.add("bound");

        // Close on pressing cancel
        this._cancelButton.addEventListener("click", this.close.bind(this));
    }

    /**
     * When the form is submitted.
     */
    submit(form) {
        this.trigger('create');
        super.submit(form);
    }

    /**
     * The name of the file to create.
     */
    get name() {
        return this._input.value;
    }
}
