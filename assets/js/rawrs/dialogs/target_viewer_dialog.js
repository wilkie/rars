// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Dialog } from '../ui/dialog';

/**
 * Represents the target selection viewer.
 */
export class TargetViewerDialog extends Dialog {
    constructor(basepath) {
        super('dialog-target-viewer');

        // Retain our basepath
        this._basepath = basepath;

        // Set our target info to an empty block
        this._targets = {};

        // Open the dialog
        this.open();

        // Gather items
        let items = [];
        this.dialog.querySelectorAll("li.target.original").forEach( (el) => {
            items.push(el);
        });
        this._items = items;

        // Gather the details pane
        this._details = this.dialog.querySelector(".target-details");

        // Get buttons
        this._selectButton = this.dialog.querySelector("#button-select-target");
        this._cancelButton = this.dialog.querySelector("#button-cancel-target");

        // Initially select nothing (it will select when it renders)
        this._selected = null;

        // Render
        this.render();
    }

    /**
     * Returns the base path for the application.
     */
    get basepath() {
        return this._basepath;
    }

    render() {
        // Clear the form
        let carousel = this.dialog.querySelector("ul.target-carousel-items");
        carousel.style.left = "";
        carousel.innerHTML = "";

        // Add at least 7 elements from our items list.
        let left = -36 - 6;
        for (let i = 0; i < 7; i++) {
            let item = this._items[i % this._items.length].cloneNode(true);
            item.classList.remove("original");
            item.removeAttribute("hidden");
            item.setAttribute("data-index", i % this._items.length);

            let img = item.querySelector("img");
            if (img) {
                img.setAttribute("src", img.getAttribute("data-src"));
            }

            item.style.left = left + "rem";
            left += 12;
            carousel.appendChild(item);
        }

        // Bind events
        this.bindEvents();

        // Activate the middle one
        this.view(carousel.querySelector("li.target:nth-child(4)"));

        this.loadInfo();
    }

    async loadInfo() {
        let response = await fetch(`${this.basepath}targets.json`);
        let data = await response.json();
        this._targets = {};

        data.forEach( (info) => {
            this._targets[info.tag] = info.info;
        });

        this.loadSelectedInfo();
    }

    view(item) {
        this.dialog.querySelectorAll("li.target").forEach( (el) => {
            el.classList.remove("next");
            el.classList.remove("active");
        });

        let carousel = this.dialog.querySelector("ul.target-carousel-items");

        // Ensure there are 3 items to each side of the selected item
        [-1, 1].forEach( (direction) => {
            let i = parseInt(item.getAttribute("data-index"));
            let current = item;
            let j = 0;
            let left = parseInt(item.style.left);

            while (current || j <= 3) {
                j++;
                left += 12 * direction;
                i = ((i + direction) + this._items.length) % this._items.length;

                let next = null;

                if (current) {
                    if (direction < 0) {
                        next = current.previousElementSibling;
                    }
                    else {
                        next = current.nextElementSibling;
                    }
                }

                // Add items to ensure there are three to each side
                if (!next && j <= 3) {
                    console.log("adding", j);
                    let newItem = this._items[i].cloneNode(true);
                    newItem.classList.remove("original");
                    newItem.removeAttribute("hidden");
                    newItem.setAttribute("data-index", i);

                    // Progressively load the images
                    let img = newItem.querySelector("img");
                    if (img) {
                        img.setAttribute("src", img.getAttribute("data-src"));
                    }

                    newItem.style.left = left + "rem";
                    if (direction < 0) {
                        carousel.insertBefore(newItem, current);
                    }
                    else {
                        carousel.insertBefore(newItem, current.nextElementSibling);
                    }
                    next = newItem;
                    this.bindItemEvents(newItem);
                }
                else if (current && j > 3) {
                    current.remove();
                }

                current = next;
            }
        });

        // Transition
        item.previousElementSibling.classList.add("next");
        item.classList.add("active");
        item.nextElementSibling.classList.add("next");

        // Reposition the selector
        if (this._selected) {
            let delta = this._selected.offsetLeft - item.offsetLeft;
            let panel = this.dialog.querySelector("ul.target-carousel-items");
            let left = (panel.offsetLeft + delta) + "px";
            panel.style.left = left;
        }

        this._selected = item;

        this.loadSelectedInfo();

        if (this._selectButton) {
            this._selectButton.removeAttribute("disabled");
        }
    }

    loadSelectedInfo() {
        if (!this._selected) {
            return;
        }

        let info = this._targets[this._selected.getAttribute("data-target")];

        if (!info) {
            return;
        }

        // Render the info block into the details pane
        console.log("loading", info);

        // Load name
        this.dialog.querySelector("h2 span.caption").textContent = info.name;

        // Clear details view
        this._details.innerHTML = "";

        ["target", "cpu", "gpu", "apu"].forEach( (section) => {
            let header = this.dialog.querySelector(`h3.${section}.original`);
            let descriptionHeader = this.dialog.querySelector(`h4.${section}.description.original`);
            let description = this.dialog.querySelector(`p.${section}.description.original`);
            let table = this.dialog.querySelector(`table.${section}.original:not(.mode)`);
            let modesHeader = this.dialog.querySelector(`h4.${section}.original.mode`);
            let modesTable = this.dialog.querySelector(`table.${section}.original.mode`);

            let sectionInfo = info;
            if (section !== "target") {
                sectionInfo = sectionInfo[section];
            }

            // Load information
            if (sectionInfo) {
                header = header.cloneNode(true);
                header.removeAttribute("hidden");
                this._details.appendChild(header);

                let items = sectionInfo;
                if (!Array.isArray(items)) {
                    items = [items];
                }

                items.forEach( (item) => {
                    if (item.description) {
                        if (descriptionHeader) {
                            descriptionHeader = descriptionHeader.cloneNode(true);
                            descriptionHeader.removeAttribute("hidden");
                            this._details.appendChild(descriptionHeader);
                        }

                        if (description) {
                            description = description.cloneNode(true);
                            description.removeAttribute("hidden");
                            description.textContent = info.description.en;
                            this._details.appendChild(description);
                        }
                    }

                    if (table) {
                        let newTable = table.cloneNode(true);
                        newTable.removeAttribute("hidden");

                        newTable.querySelectorAll("td").forEach( (cell) => {
                            let field = cell.getAttribute("data-field");
                            if (field && item[field]) {
                                if (Array.isArray(item[field])) {
                                    cell.textContent = item[field][0] + " " + item[field][1];
                                }
                                else {
                                    cell.textContent = item[field];
                                }
                            }
                            else {
                                cell.textContent = "";
                            }
                        });

                        this._details.appendChild(newTable);
                    }

                    if (modesTable && item.modes) {
                        if (modesHeader) {
                            modesHeader = modesHeader.cloneNode(true);
                            modesHeader.removeAttribute("hidden");
                            this._details.appendChild(modesHeader);
                        }

                        item.modes.forEach( (mode) => {
                            let newModesTable = modesTable.cloneNode(true);
                            newModesTable.removeAttribute("hidden");

                            newModesTable.querySelectorAll("td").forEach( (cell) => {
                                let field = cell.getAttribute("data-field");
                                if (field && mode[field]) {
                                    if (Array.isArray(mode[field])) {
                                        cell.textContent = mode[field][0] + " " + mode[field][1];
                                    }
                                    else {
                                        cell.textContent = mode[field];
                                    }
                                }
                                else {
                                    cell.textContent = "";
                                }
                            });

                            this._details.appendChild(newModesTable);
                        });
                    }
                });
            }
        });
    }

    bindItemEvents(item) {
        item.addEventListener("click", (event) => {
            this.view(item);
        });
    }

    bindEvents() {
        this._cancelButton.addEventListener("click", this.close.bind(this));
        this._selectButton.addEventListener("click", this.select.bind(this));

        this.dialog.querySelectorAll("li.target").forEach( (el) => {
            this.bindItemEvents(el);
        });

        let left = this.dialog.querySelector("button.left");
        if (left) {
            left.addEventListener("click", (event) => {
                this.view(this._selected.previousElementSibling);
            });
        }

        let right = this.dialog.querySelector("button.right");
        if (right) {
            right.addEventListener("click", (event) => {
                this.view(this._selected.nextElementSibling);
            });
        }
    }

    select() {
        this.trigger("target", this._selected.getAttribute("data-target"));
        this.close();
    }
}
