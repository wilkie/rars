// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Process } from '../process';

/**
 * The base class to wrap a high-level compiler program for a target.
 */
export class Compiler extends Process {
    /**
     * Performs the compilation routine.
     *
     * The file given should have been passed to `upload` with the name being
     * given by `filename` previous to this call.
     *
     * The resulting object file should be passed to `resolve`.
     *
     * Calling `reject` will throw an exception for the error.
     *
     * This function should be overloaded by the individual compiler
     * implementations.
     *
     * @param {function} resolve - The callback function upon success.
     * @param {function} reject - The callback function upon failure.
     * @param {Object} options - A set of options for this specific invocation.
     */
    perform(resolve, reject, options) {
        throw new Error("Unimplemented compiler");
    }

    /**
     * Invokes the process.
     */
    async invoke(options = {}) {
        if (!this.working[0]) {
            throw new Error("Compiler requires at least one file.");
        }

        let filename = this.working[0].name;

        return new Promise( (resolve, reject) => {
            this.perform(resolve, reject, options);
        });
    }

    get description() {
        return "Compiling";
    }
}
