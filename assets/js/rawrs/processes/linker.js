// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Process } from '../process';

export class Linker extends Process {
    /**
     * Performs the link routine and produces a binary.
     *
     * Generally, all inputs will be passed by the first stage using the `add`
     * function and available as `this.working`.
     *
     * This function should be overloaded by the individual linker
     * implementations.
     *
     * @param {function} resolve - The callback function upon success.
     * @param {function} reject - The callback function upon failure.
     * @param {Object} options - A set of options for this specific invocation.
     */
    perform(resolve, reject, options) {
        throw new Error("Unimplemented linker");
    }

    async invoke(options = {}) {
        return new Promise( (resolve, reject) => {
            this.perform(resolve, reject, options);
        });
    }

    get description() {
        return "Linking";
    }
}
