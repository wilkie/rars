// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { EventComponent } from './event_component';

export class Debugger extends EventComponent {
    /**
     * Constructs a new Debugger.
     *
     * @param {string} basepath - The base path for any URL.
     * @param {Object} settings - The initial configuration of the debugger.
     * @param {Simulator} simulator - The simulator we will attach to.
     * @param {Target} target - The target architecture of this debugger.
     */
    constructor(basepath, settings, simulator, target) {
        super();

        // Retain basepath
        this.__basepath = basepath;

        // Retain settings (allowing passed settings to override options)
        this.__settings = {
            ...this.options,
            ...settings
        };

        // Retain simulator
        this.__simulator = simulator;

        // Retain target
        this.__target = target;

        // The list of files to mount
        this.__files = [];
    }

    /**
     * Returns the name of the target this simulates.
     */
    get target() {
        return this.__target.name;
    }

    /**
     * Retrieves the options.
     */
    get options() {
        return this.constructor.options;
    }

    /**
     * Returns information about the capabilities of the simulator.
     */
    static get options() {
        return {};
    }

    /**
     * Returns the base path for the application.
     */
    get basepath() {
        return this.__basepath;
    }

    /**
     * Returns the configuration for the simulation.
     */
    get settings() {
        return this.__settings;
    }

    /**
     * Initializes the debugger to prepare to attach.
     */
    async load() {
        // Does nothing, if unimplemented
    }

    /**
     * Tells the debugger of a simulation reset.
     */
    async reset() {
        // Does nothing, if unimplemented
    }

    /**
     * Returns the attached Simulator.
     */
    get simulator() {
        return this.__simulator;
    }

    /**
     * Adds a text file to the file-system.
     *
     * @param {string} name - The file name to add to the '/input' directory.
     * @param {string} source - The source text.
     */
    upload(name, source) {
        // Ensure it has a newline at the end
        source = source + "\n";

        this.__files.push({
            name: name,
            data: source
        });
    }

    /**
     * Retrieves a set of blobs for uploaded files in '/input'.
     */
    get files() {
        return this.__files;
    }

    /**
     * Reports a set breakpoint to the debugger.
     *
     * @param {string} address - The hex encoded address such as `40000c`.
     */
    setBreakpoint(address) {
    }

    /**
     * Reports a breakpoint has been cleared to the debugger.
     *
     * @param {string} address - The hex encoded address such as `40000c`.
     */
    clearBreakpoint(address) {
    }
}
