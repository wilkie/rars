// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Util } from '../util.js';

import { Editor } from '../editor.js';

import { HexEditor } from './hex_editor.js';

import { Tabs } from '../ui/tabs.js';

// TODO:
// header section... magic number, endian, 32/64 bit, etc
//
// TODO:
// hex editor annotations for elf info that link to their description
//
// TODO:
// program header info...
//
// TODO:
// sections info... expandable tree that shows each section header and info

/**
 * Represents a widget for viewing and understanding ELF executables.
 */
export class ELFEditor extends Editor {
    constructor(element, options) {
        super(element, options);

        ELFEditor.__count ||= 1;
        ELFEditor.__count++;

        let id = ELFEditor.__count.toString(16);
        this._id = id;

        // Update the DOM to replace %id% to make unique ids
        element.querySelectorAll('li.tab > button').forEach( (el) => {
            let attr = el.getAttribute('aria-controls');
            attr = attr.replace('%id%', id);
            el.setAttribute('aria-controls', attr);
        });

        element.querySelectorAll('li.tab-panel').forEach( (el) => {
            let attr = el.getAttribute('id');
            attr = attr.replace('%id%', id);
            el.setAttribute('id', attr);
        });

        // Load tabs
        let tabsElement = element.querySelector("ol.tabs.side");
        this._tabs = Tabs.load(tabsElement);

        // Load the hex editor into a pane
        this._hexEditorElement = element.querySelector('.hex-container');
        this._hexEditorOptions = (options || {}).hexEditorOptions || {};
        this._hexEditor = new HexEditor(this._hexEditorElement, this._hexEditorOptions);
    }

    static get view() {
        return "elf-editor";
    }

    load(data, name, info) {
        // Set the data source
        if (!(data instanceof Uint8Array)) {
            data = (new TextEncoder('utf-8')).encode(data);
        }
        this._source = data;

        // Pass along to the hex editor as well.
        this._hexEditor.load(data, name, info);

        // Get our own data view.
        this._view = new DataView(data.buffer);

        // Get the ELF info.
        this._header = {};
        this._header.magic = Array.from(data.slice(0, 4));
        this._header.bits = this._view.getUint8(4) == 1 ? 32 : 64;
        this._header.endian = this._view.getUint8(5);

        // Set the endianness we use to interpret values
        this._endian = this._header.endian == 1;

        this._header.version = this._view.getUint8(6);
        this._header.abi = this._view.getUint8(7);
        this._header.abiVersion = this._view.getUint8(8);
        this._header.type = this._view.getUint16(0x10, this._endian);
        this._header.machine = this._view.getUint16(0x12, this._endian);
        this._header.elfVersion = this._view.getUint32(0x14, this._endian);

        let address = 0x18;
        if (this._header.bits == 32) {
            this._header.entry = this._view.getUint32(address, this._endian);
            address += 4;
            this._header.phoff = this._view.getUint32(address, this._endian);
            address += 4;
            this._header.shoff = this._view.getUint32(address, this._endian);
            address += 4;
        }
        else {
            this._header.entry = this._view.getBigInt64(address, this._endian);
            address += 8;
            this._header.phoff = this._view.getBigInt64(address, this._endian);
            address += 8;
            this._header.shoff = this._view.getBigInt64(address, this._endian);
            address += 8;
        }

        this._header.flags = this._view.getUint32(address, this._endian);
        this._header.ehsize = this._view.getUint16(address + 0x4, this._endian);
        this._header.phentsize = this._view.getUint16(address + 0x6, this._endian);
        this._header.phnum = this._view.getUint16(address + 0x8, this._endian);
        this._header.shentsize = this._view.getUint16(address + 0xa, this._endian);
        this._header.shnum = this._view.getUint16(address + 0xc, this._endian);
        this._header.shstrndx = this._view.getUint16(address + 0xe, this._endian);

        // Read program header
        this._segments = []
        address = Number(this._header.phoff);

        for (let i = 0; i < this._header.phnum; i++) {
            let segment = {};
            let startAddress = address;

            segment.type = this._view.getUint32(address, this._endian);
            address += 4;

            if (this._header.bits == 64) {
                segment.flags = this._view.getUint32(address, this._endian);
                address += 4;

                segment.offset = this._view.getBigInt64(address, this._endian);
                address += 8;

                segment.vaddr = this._view.getBigInt64(address, this._endian);
                address += 8;

                segment.paddr = this._view.getBigInt64(address, this._endian);
                address += 8;

                segment.filesize = this._view.getBigInt64(address, this._endian);
                address += 8;

                segment.memsize = this._view.getBigInt64(address, this._endian);
                address += 8;

                segment.align = this._view.getBigInt64(address, this._endian);
                address += 8;
            }
            else {
                segment.offset = this._view.getUint32(address, this._endian);
                address += 4;

                segment.vaddr = this._view.getUint32(address, this._endian);
                address += 4;

                segment.paddr = this._view.getUint32(address, this._endian);
                address += 4;

                segment.filesize = this._view.getUint32(address, this._endian);
                address += 4;

                segment.memsize = this._view.getUint32(address, this._endian);
                address += 4;

                segment.flags = this._view.getUint32(address, this._endian);
                address += 4;

                segment.align = this._view.getUint32(address, this._endian);
                address += 4;
            }

            this._segments.push(segment);
            address = startAddress + this._header.phentsize;
        }

        // Read section header
        this._sections = []
        address = Number(this._header.shoff);

        for (let i = 0; i < this._header.shnum; i++) {
            let section = {};
            let startAddress = address;

            if (i == this._header.shstrndx) {
                this._stringTable = section;
            }

            section.name = this._view.getUint32(address, this._endian);
            address += 4;

            section.type = this._view.getUint32(address, this._endian);
            address += 4;

            if (this._header.bits == 64) {
                section.flags = this._view.getBigInt64(address, this._endian);
                address += 8;

                section.addr = this._view.getBigInt64(address, this._endian);
                address += 8;

                section.offset = this._view.getBigInt64(address, this._endian);
                address += 8;

                section.size = this._view.getBigInt64(address, this._endian);
                address += 8;

            }
            else {
                section.flags = this._view.getUint32(address, this._endian);
                address += 4;

                section.addr = this._view.getUint32(address, this._endian);
                address += 4;

                section.offset = this._view.getUint32(address, this._endian);
                address += 4;

                section.size = this._view.getUint32(address, this._endian);
                address += 4;
            }

            section.link = this._view.getUint32(address, this._endian);
            address += 4;

            section.info = this._view.getUint32(address, this._endian);
            address += 4;

            if (this._header.bits == 64) {
                section.addralign = this._view.getBigInt64(address, this._endian);
                address += 8;

                section.entsize = this._view.getBigInt64(address, this._endian);
                address += 8;

            }
            else {
                section.addralign = this._view.getUint32(address, this._endian);
                address += 4;

                section.entsize = this._view.getUint32(address, this._endian);
                address += 4;
            }

            this._sections.push(section);
            address = startAddress + this._header.shentsize;
        }

        // Get section names
        for (let i = 0; i < this._header.shnum; i++) {
            let offset = this._sections[i].name;
            offset += Number(this._sections[this._header.shstrndx].offset);
            let start = offset;

            // Find the null terminator
            while(this._source[offset] && (offset - start < 25)) {
                offset++;
            }

            let slice = this._source.slice(start, offset);
            let name = (new TextDecoder('utf-8')).decode(slice);
            this._sections[i].name = name;
        }

        // Set all keys
        this.element.querySelectorAll('span.elf-editor-header-field').forEach( (span) => {
            let key = span.getAttribute('data-key');

            if (Array.isArray(this._header[key])) {
                this._header[key].forEach( (b) => {
                    let item = document.createElement("span");
                    item.textContent = b.toString(16).padStart(2, '0');
                    item.classList.add('value');
                    span.appendChild(item);
                });
            }
            else {
                // Interpret to a string, if possible
                const text = `0x${this._header[key].toString(16)}`;
                if (ELFEditor[key.toUpperCase()]) {
                    span.innerHTML = ELFEditor[key.toUpperCase()][this._header[key]] + ` (<span class='value'>${text}</span>)`;
                }
                else {
                    // Otherwise, just display the value as hex
                    span.textContent = text;
                    span.classList.add('value');
                }
            }
        });

        // Craft segments
        let segments = this.element.querySelector('ol.segments');
        for (let i = 0; i < this._segments.length; i++) {
            const segment = this._segments[i];
            let template = this.element.querySelector('template.segment');
            let item = Util.createElementFromTemplate(template);
            let h3 = item.querySelector('h3.type');
            if (h3) {
                let button = h3.querySelector('button');
                if (button) {
                    if (ELFEditor.SEGMENT_TYPE[segment.type]) {
                        h3.classList.remove('no-name');
                        button.textContent = ELFEditor.SEGMENT_TYPE[segment.type];
                    }

                    let attr = button.getAttribute('aria-controls');
                    attr = attr.replace('%id%', this._id);
                    attr = attr.replace('%index%', i);
                    button.setAttribute('aria-controls', attr);

                    let items = item.querySelector('ul.items');
                    items.setAttribute('id', attr);

                    // Expand items listing
                    button.addEventListener('click', (event) => {
                        event.preventDefault();
                        event.stopPropagation();

                        h3.classList.toggle('open');
                    });
                }
            }

            // Update item values
            item.querySelectorAll('span.elf-editor-segment-field').forEach( (span) => {
                let key = span.getAttribute('data-key');

                if (Array.isArray(segment[key])) {
                    segment[key].forEach( (b) => {
                        let item = document.createElement("span");
                        item.textContent = b.toString(16).padStart(2, '0');
                        item.classList.add('value');
                        span.appendChild(item);
                    });
                }
                else {
                    // Interpret to a string, if possible
                    const text = `0x${segment[key].toString(16)}`;
                    if (ELFEditor["SEGMENT_" + key.toUpperCase()]) {
                        span.innerHTML = ELFEditor["SEGMENT_" + key.toUpperCase()][segment[key]] + ` (<span class='value'>${text}</span>)`;
                    }
                    else {
                        // Otherwise, just display the value as hex
                        span.textContent = text;
                        span.classList.add('value');
                    }
                }
            });

            segments.appendChild(item);
        }

        // Craft sections
        let sections = this.element.querySelector('ol.sections');
        for (let i = 0; i < this._sections.length; i++) {
            const section = this._sections[i];
            let template = this.element.querySelector('template.section');
            let item = Util.createElementFromTemplate(template);
            let h3 = item.querySelector('h3.name');
            if (h3) {
                let button = h3.querySelector('button');
                if (button) {
                    if (section.name) {
                        h3.classList.remove('no-name');
                        button.textContent = section.name;
                    }

                    let attr = button.getAttribute('aria-controls');
                    attr = attr.replace('%id%', this._id);
                    attr = attr.replace('%index%', i);
                    button.setAttribute('aria-controls', attr);

                    let items = item.querySelector('ul.items');
                    items.setAttribute('id', attr);

                    // Expand items listing
                    button.addEventListener('click', (event) => {
                        event.preventDefault();
                        event.stopPropagation();

                        h3.classList.toggle('open');
                    });
                }
            }

            // Update item values
            item.querySelectorAll('span.elf-editor-section-field').forEach( (span) => {
                let key = span.getAttribute('data-key');

                if (Array.isArray(section[key])) {
                    section[key].forEach( (b) => {
                        let item = document.createElement("span");
                        item.textContent = b.toString(16).padStart(2, '0');
                        item.classList.add('value');
                        span.appendChild(item);
                    });
                }
                else {
                    // Interpret to a string, if possible
                    const text = `0x${section[key].toString(16)}`;
                    if (ELFEditor["SECTION_" + key.toUpperCase()]) {
                        span.innerHTML = ELFEditor["SECTION_" + key.toUpperCase()][section[key]] + ` (<span class='value'>${text}</span>)`;
                    }
                    else {
                        // Otherwise, just display the value as hex
                        span.textContent = text;
                        span.classList.add('value');
                    }
                }
            });

            sections.appendChild(item);
        }
    }
}

ELFEditor.ABI = [
    "System V",
    "HP-UX",
    "NetBSD",
    "Linux",
    "GNU Hurd",
    "Solaris",
    "AIX",
    "IRIX",
    "FreeBSD",
    "Tru64",
    "Novell Modesto",
    "OpenBSD",
    "OpenVMS",
    "NonStop Kernel",
    "AROS",
    "FenixOS",
    "Nuxi CloudABI",
    "Stratus Technologies OpenVOS"
];

ELFEditor.TYPE = [
    "Unknown",
    "Relocatable File",
    "Executable File",
    "Shared Object",
    "Core File"
];

ELFEditor.MACHINE = {
    0x00: 'No specific instruction set',
    0x01: 'AT&T WE 32100',
    0x02: 'SPARC',
    0x03: 'x86',
    0x04: 'Motorola 68000',
    0x05: 'Motorola 88000',
    0x06: 'Intel MCU',
    0x07: 'Intel 80860',
    0x08: 'MIPS',
    0x09: 'IBM System/370',
    0x0a: 'MIPS RS3000 Little-endian',
    0x0e: 'Hewlett-Packard PA-RISC',
    0x13: 'Intel 80960',
    0x14: 'PowerPC',
    0x15: 'PowerPC (64-bit)',
    0x16: 'S390',
    0x17: 'IBM SPU/SPC',
    0x24: 'NEC V800',
    0x25: 'Fujitsu FR20',
    0x26: 'TRW RH-32',
    0x27: 'Motorola RCE',
    0x28: 'Arm',
    0x29: 'Digital Alpha',
    0x2a: 'SuperH',
    0x2b: 'SPARC Version 9',
    0x2c: 'Siemens TriCore embedded processor',
    0x2d: 'Argonaut RISC Core',
    0x2e: 'Hitachi H8/300',
    0x2f: 'Hitachi H8/300H',
    0x30: 'Hitachi H8S',
    0x31: 'Hitachi H8/500',
    0x32: 'IA-64',
    0x33: 'Stanford MIPS-X',
    0x34: 'Motorola ColdFire',
    0x35: 'Motorola M68HC12',
    0x36: 'Fujitsu MMA Multimedia Accelerator',
    0x37: 'Siemens PCP',
    0x38: 'Sony nCPU embedded RISC processor',
    0x39: 'Denso NDR1 microprocessor',
    0x3a: 'Motorola Star*Core processor',
    0x3b: 'Toyota ME16 processor',
    0x3c: 'STMicroelectronics ST100 processor',
    0x3d: 'Advanced Logic Corp. TinyJ embedded processor family',
    0x3e: 'AMD x86-64',
    0x3f: 'Sony DSP Processor',
    0x40: 'Digital Equipment Corp. PDP-10',
    0x41: 'Digital Equipment Corp. PDP-11',
    0x42: 'Siemens FX66 microcontroller',
    0x43: 'STMicroelectronics ST9+ 8/16 bit microcontroller',
    0x44: 'STMicroelectronics ST7 8-bit microcontroller',
    0x45: 'Motorola MC68HC16 Microcontroller',
    0x46: 'Motorola MC68HC11 Microcontroller',
    0x47: 'Motorola MC68HC08 Microcontroller',
    0x48: 'Motorola MC68HC05 Microcontroller',
    0x49: 'Silicon Graphics SVx',
    0x4a: 'STMicroelectronics ST19 8-bit microcontroller',
    0x4b: 'Digital VAX',
    0x4c: 'Axis Communications 32-bit embedded processor',
    0x4d: 'Infineon Technologies 32-bit embedded processor',
    0x4e: 'Element 14 64-bit DSP Processor',
    0x4f: 'LSI Logic 16-bit DSP Processor',
    0x8c: 'TMS320C6000 Family',
    0xaf: 'MCST Elbrus e2k',
    0xb7: 'Arm 64-bits (Armv8/AArch64)',
    0xdc: 'Zilog Z80',
    0xf3: 'RISC-V',
    0xf7: 'Berkeley Packet Filter',
    0x101: 'WDC 65C816'
};

ELFEditor.BITS = {
    32: "32-bit",
    64: "64-bit"
};

ELFEditor.SEGMENT_TYPE = {
    0x00: "Unused Segment Type",
    0x01: "Loadable Segment",
    0x02: "Dynamic Linking Information",
    0x03: "Interpreter Information",
    0x04: "Auxiliary Information",
    0x06: "Program Header",
    0x07: "Thread-Local Storage",
};

ELFEditor.SECTION_TYPE = {
    0x00: "Null",
    0x01: "Program Data",
    0x02: "Symbol Data",
    0x03: "String Table",
    0x04: "Relocation Entries (With Addends)",
    0x05: "Symbol Hash Table",
    0x06: "Dynamic Linking Information",
    0x07: "Notes",
    0x08: "Program Space With No Data",
    0x09: "Relocation Entries (No Addends)",
    0x0b: "Dynamic Linker Symbol Table",
    0x0e: "Array of Constructors",
    0x0f: "Array of Destructors",
    0x10: "Array of Pre-constructors",
    0x11: "Section Group",
    0x12: "Extended Section Indices",
    0x13: "Number of Defined Types"
};

ELFEditor.LITTLE_ENDIAN = 1;
ELFEditor.BIG_ENDIAN = 2;

ELFEditor.ENDIAN = {
    1: "Little Endian",
    2: "Big Endian"
};
