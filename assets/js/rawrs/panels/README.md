# Panels

In RAWRS, a ***Panel*** is a visual widget that visualizes some interaction with
the simulation.

Targets may provide their own panels, but common ones appear here.

## Console

The `Console` panel provides a fully functional terminal that can be used to be
the output of a simulation or debugging session.

The options include `rows` and `columns` which define the desired size of the
terminal screen.

## Video

The `Video` panel provides a canvas element that can be used with any kind of framebuffer driver or WebGL target.

The options include `width` and `height` which define the size of the
framebuffer screen in pixels. The actual canvas might be stretched or shrunk to
fit the interface.

## Creating a Panel

One can create a panel by extending the `Panel` base class.

```javascript
import { Panel } from 'panel.js';

export class MyPanel extends Panel {
    load() {
        // Create my panel contents
        this._canvas = document.createElement("canvas");

        // You can access any settings using `this.settings`
        if (this.settings.myOption == 42) {
            alert("WHOA");
        }

        // Add to the base element
        this.element.appendChild(canvas);
    }

    shown() {
        // This happens when the panel is made visible
    }

    hidden() {
        // This happens when the panel is hidden from view
    }
}
```

And then using it by adding it as an output in a `Simulator` or `Debugger`:

```javascript
import { Simulator } from 'simulator.js';

export class MySimulator extends Simulator {
    static get options() {
        return {
            outputs: [
                {
                    panel: MyPanel, // The class to instantiate to create the panel
                    myOption: 42    // Accessed via `this.settings` within panel
                }
            ]
        }
    }
}
```
