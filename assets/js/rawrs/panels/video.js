// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Panel } from '../panel.js';

/**
 * This represents the video device.
 */
export class Video extends Panel {
    get name() {
        return this.settings.name || "run.panels.video";
    }

    async load() {
        this._width = this.settings.width;
        this._height = this.settings.height;
        this._originalWidth = this.settings.width;
        this._originalHeight = this.settings.height;

        this._canvas = document.createElement("canvas");
        this._canvas.setAttribute("tabindex", "1");
        this._canvas.setAttribute("width", this._originalWidth);
        this._canvas.setAttribute("height", this._originalHeight);
        this._active = false;
        this._scale = 1;

        // Ensure clicking on canvas focuses on it
        this._canvas.addEventListener('click', (event) => {
            this._canvas.focus();
        });

        // Get context
        this._context = this._canvas.getContext('2d');

        // Bind events
        this._canvas.addEventListener('keyup', (event) => {
            this.trigger('keyup', event);
        });

        this._canvas.addEventListener('keydown', (event) => {
            this.trigger('keydown', event);
        });

        // Create an image to hold the frame
        this._image = this.context.createImageData(this.width, this.height);

        // Add the canvas to the panel
        this.element.appendChild(this.canvas);

        // Clear screen
        this.clear();
    }

    shown() {
        this.animate();
    }

    /**
     * Returns whether or not the device has been written to.
     */
    get active() {
        return this._active;
    }

    /**
     * Sets this video screen as active.
     */
    set active(value) {
        this._active = value;

        if (this.visible && !this._active) {
            this.animate();
        }
    }

    /**
     * Returns the scale of the video.
     */
    get scale() {
        return this._scale;
    }

    /**
     * Returns the current width of the display.
     */
    get width() {
        return this._width;
    }

    /**
     * Returns the current height of the display.
     */
    get height() {
        return this._height;
    }

    /**
     * Returns the canvas element representing the display.
     */
    get canvas() {
        return this._canvas;
    }

    /**
     * Returns the drawing context for the display.
     */
    get context() {
        return this._context;
    }

    blit(imageData, destX, destY, srcX, srcY, width, height, deviceWidth, deviceHeight) {
        // Update the scale
        if (deviceWidth != this.width || deviceHeight != this.height) {
            this._width = deviceWidth;
            this._height = deviceHeight;
            this.canvas.setAttribute('width', this.width);
            this.canvas.setAttribute('height', this.height);
        }

        // Show this panel if it is the first time the video buffer is updated.
        if (!this.visible && !this._active) {
            this.show();
        }
        this._active = true;

        // Blit
        this.context.putImageData(imageData, destX, destY, srcX, srcY, width, height);
    }

    async reset() {
        this._width = this._originalWidth;
        this._height = this._originalHeight;
        this.canvas.setAttribute('width', this.width);
        this.canvas.setAttribute('height', this.height);
        this._active = false;
        this.clear();
        this.animate();
    }

    clear() {
        if (this.active) {
            // Clear to black
            this.context.fillStyle = "#000";
            this.context.fillRect(0, 0, this.width, this.height);
        }
        else {
            // Clear to static
            let buffer = new Uint32Array(this._image.data.buffer);
            let step = 4; // The size of the pixel
            let pixels = step * step;

            for (let y = 0; y < this.height; y += step) {
                for (let x = 0; x < this.width; x += step) {
                    let pos = (y * this.width) + x;
                    let value = 0x888888 | ((255 * Math.random()) << 24);

                    for (let j = 0; j < pixels; j++) {
                        buffer[pos + (j % step) + (this.width * Math.floor(j / step))] = value;
                    }
                }
            }

            this.context.putImageData(this._image, 0, 0);
        }
    }

    animate() {
        // If not written to, show the static
        let time = 0;
        let step = (when) => {
            if (!time) {
                time = when;
            }

            let elapsed = when - time;
            if (elapsed > 50) {
                time = when;

                // Handle race condition when the tab is revealed and a
                // simulation happens quickly.
                if (this.visible && !this.active) {
                    this.clear();
                }
            }

            if (this.visible && !this.active) {
                window.requestAnimationFrame(step);
            }
        };

        window.requestAnimationFrame(step);
    }
}
