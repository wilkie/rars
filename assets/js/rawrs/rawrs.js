// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Tabs }            from './ui/tabs';
import { Editors }         from './ui/editors';
import { Panels }          from './ui/panels';
import { Toolbar }         from './ui/toolbar';
import { Console }         from './ui/console';
import { FileListing }     from './ui/file_listing';
import { CodeListing }     from './ui/code_listing';
import { MemoryListing }   from './ui/memory_listing';
import { LabelListing }    from './ui/label_listing';
import { RegisterListing } from './ui/register_listing';
import { TargetSelector }  from './ui/target_selector';
import { Separator }       from './ui/separator';

import { NewProjectDialog } from './dialogs/new_project_dialog.js';

import { KeyBindings }     from './key_bindings';
import { Util }            from './util';
import { Processor }       from './processor';
import { Locale }          from './locale';
import { Simulator }       from './simulator';

export class RAWRS {
    // Load the application
    constructor() {
        // Set rootpath
        document.body.setAttribute('data-rootpath', this.rootpath);

        // Bind default keybindings
        this._keyBindings = new KeyBindings(document.body);

        // Load all separators
        let separators = Array.from(
            document.querySelectorAll(".separator")
        ).map( (el) => {
            return new Separator(el);
        });

        // Ensure we go to the indicated anchor
        if (window.location.hash) {
            let item = document.querySelector(window.location.hash);
            if (item) {
                item.scrollIntoView();
            }
        }

        // Create the interface elements
        this._console = new Console(document.body);
        this._panels = new Panels(document.body);
        this._toolbar  = new Toolbar(document.body);
        this._codeListing = new CodeListing(document.body);
        this._memoryListing = new MemoryListing(document.body);
        this._labelListing = new LabelListing(document.body);
        this._targetSelector = new TargetSelector(this.basepath, document.body);
        this._registerListing = new RegisterListing(document.body);
        this._fileListing = new FileListing(this.basepath, document.body, this.targetSelector.target);

        this.console.on('open', (file) => {
            // Copy the file reference
            file = {
                data: file.data,
                name: file.name,
                path: file.path,
                type: file.type
            };
            file.path = ".console/" + (file.path || 'cache') + "/" + file.name;
            this.editors.open(file).then( (editor) => {
                editor.readonly = true;

                // Focus on the edit panel
                this.tabs.select('edit-panel');

                // Focus on the editor
                this.editors.focus();
            });
        });

        // We need to populate the file listing with a new target upon change
        this.targetSelector.on('change', async (target) => {
            // Load the target, if it needs to be
            await Util.loadScript(`${this.basepath}js/compiled-${target}.js`);

            // Stop the current simulation
            if (this.simulator && !this.simulator.done) {
                // Ignore any errors stopping the simulator
                try {
                    await this.stop();
                }
                catch {}
            }

            // Get the target
            this.target = window.RAWRSTargetFor(target);
        });

        // Pull in the target list and then set the target from last load (or
        // whatever is overridden.)
        this.targetSelector.populate().then( () => {
            // If we are loading from a guidance page for a particular target, load
            // that target specifically.
            let m = this.rootpath.match("/guidance/([^/]+)/");
            if (m && m[1]) {
                this.targetSelector.target = m[1];
            }
            else {
                this.targetSelector.target = this.fileListing.target;
            }
        });

        // Ensure we localize any new panels
        this.panels.on('dom-update', () => {
            if (this.locale) {
                this.locale.update(this.panels.element);
            }
        });

        // The active annotations of other files
        this._annotations = {};
        this._annotationsCache = {};

        // Load the tabs
        Tabs.load();
        this._tabs = Tabs.load(document.querySelector('#main-tabs'));
        this.tabs.on('change.blur-editor', (button) => {
            if (button.getAttribute('aria-controls') !== 'edit-panel') {
                // Unfocus the editor
                this.editors.blur();
            }
        });

        // Open the previous file in a file tab
        this._editors = new Editors(document.querySelector('#editors'));

        // Sets up a save timer for the editor
        this.editors.on('select', (file) => {
            // Select that file in the FileListing
            let item = this.fileListing.itemFor(file.path);
            if (item) {
                this.fileListing.select(item, true);
            }
        });

        this.editors.on('change', (editor) => {
            if (!editor.loading && !editor.readonly) {
                if (editor.__saveTimer) {
                    window.clearTimeout(editor.__saveTimer);
                }

                editor.__saveTimer = null;
                editor.__saveTimer = window.setTimeout( () => {
                    if (!editor.loading && !editor.readonly) {
                        var data = editor.value;
                        this.fileListing.save(data);
                    }
                }, 500);
            }
        });

        // When a file item has changed
        this.fileListing.on('change', (item) => {
            let info = this.fileListing.infoFor(item);
        });

        // When the file listing suggests somebody wants to open a file
        this.fileListing.on('load', (info) => {
            // Load the file into the editor
            let targetFileInfo = this.target.typeFor(info.name);
            info.type = info.type || targetFileInfo.type;
            info.options = targetFileInfo.options || {};

            this.editors.open(info).then( (editor) => {
                // Now it is loaded, allow saving.
                editor.readonly = false;

                // Focus on the edit panel
                this.tabs.select('edit-panel');

                // Focus on the editor
                this.editors.focus();

                // Load any annotations for this file
                if (info.path in this._annotations) {
                    editor.annotations = this._annotations[info.path];
                }
            });
        });

        // When we create a new project.
        this.fileListing.on('action', (action) => {
            if (action === 'new-project') {
                let newProjectDialog = new NewProjectDialog(this.target);
                newProjectDialog.on('create', async (info) => {
                    let name = info.name;
                    let files = info.project.files;

                    // TODO: do not interact directly with the _storage layer
                    for (let i = 0; i < files.length; i++) {
                        let file = files[i];
                        let path = `/${this.target.name}/${name}/${file.name || 'main.s'}`;
                        if (i == 0) {
                            await this.fileListing._storage.save(path, file.data);

                            this.fileListing.loadRoot().then( () => {
                                this.fileListing.revealPath(path).then( () => {
                                    let item = this.fileListing.itemFor(path);
                                    if (item) {
                                        this.fileListing.loadItem(item);
                                    }
                                });
                            });
                        }
                        else {
                            await this.fileListing._storage.save(path, file.data);
                        }
                    }
                });
            }
        });

        // When a breakpoint is set
        this.codeListing.on("breakpoint-set", (address) => {
            // Tell the simulator
            if (this.simulator) {
                this.simulator.setBreakpoint(address);
            }

            // Tell the debugger
            if (this.debugger) {
                this.debugger.setBreakpoint(address);
            }
        });

        // When a breakpoint is cleared
        this.codeListing.on("breakpoint-clear", (address) => {
            // Tell the simulator
            if (this.simulator) {
                this.simulator.clearBreakpoint(address);
            }

            // Tell the debugger
            if (this.debugger) {
                this.debugger.clearBreakpoint(address);
            }
        });

        this.memoryListing.on("change", (info) => {
            try {
                if (this.simulator) {
                    this.simulator.write(info.address, info.data);
                }
            }
            catch (e) {
            }
        });

        // Toolbar events
        this.toolbar.on('click', async (button) => {
            switch (button.getAttribute("id")) {
                case "assemble":
                    this.toolbar.setStatus("run", "disabled");
                    this.toolbar.setStatus("assemble", "active");

                    // Ensure runtime is suspended.
                    if (!this.simulator.done &&
                        !this.simulator.uninitialized &&
                        !this.simulator.ready) {
                        await this.stop();
                    }

                    // Assemble
                    await this.assemble();
                    break;
                case "run":
                    // If it is already running, stop it
                    if (this.simulator && this.simulator.running) {
                        await this.pause();
                        await this.stop();
                    }
                    else {
                        // Assemble first, if needed.
                        if (this.toolbar.getStatus("assemble") !== "success") {
                            this.toolbar.setStatus("run", "disabled");
                            this.toolbar.setStatus("assemble", "active");
                            await this.assemble();
                        }

                        this.toolbar.setStatus("step", "disabled");
                        this.toolbar.setStatus("run", "active");
                        await this.run();
                    }
                    break;
                case "step":
                    if (this.simulator && this.simulator.running) {
                        // Pause, if still running
                        await this.pause();
                    }
                    else {
                        // Otherwise, step
                        await this.step();
                    }
                    break;
                default:
                    // Unknown button
                    break;
            }
        });

        // Load the locale.
        this.locale = new Locale(this.basepath, "en");

        // And pass it along to things that use it.
        this.toolbar.locale = this.locale;
        this.editors.locale = this.locale;
    }

    /**
     * Retrieves the current locale data.
     */
    get locale() {
        return this._locale;
    }

    /**
     * Updates the locale and re-translates the application.
     */
    set locale(value) {
        this._locale = value;

        // When the locale loads, translate the document based on our locale
        this.locale.update(document.body);
    }

    /**
     * Retrieves the current target.
     */
    get target() {
        return this._target;
    }

    /**
     * Updates the current Target.
     */
    set target(value) {
        this._target = value;

        // Update the file listing with the target name.
        this.fileListing.target = this.target.name;

        // Update the editors pane with the target instance.
        this.editors.target = this.target;

        // Reload register listing
        this.registerListing.description = this.target.registers;

        // Update panels
        this.panels.clear();

        // Hide all guidance panels
        let guidancePanel = document.body.querySelector('#guidance-panel');
        guidancePanel.querySelectorAll(`li.tab[data-target]:not([data-target="${this.target.name}"])`).forEach( (el) => {
            el.setAttribute('hidden', '');
            el.setAttribute('aria-hidden', 'true');
        });

        // Activate the first guidance tab (if current tab is hidden)
        let activeGuidanceTab = guidancePanel.querySelector('li.tab.active');
        if (activeGuidanceTab.hasAttribute('hidden')) {
            guidancePanel.querySelector('li.tab > button').click();
        }

        // Reveal target guidance panels
        guidancePanel.querySelectorAll(`li.tab[data-target="${this.target.name}"]`).forEach( (el) => {
            el.removeAttribute('hidden');
            el.removeAttribute('aria-hidden');
        });

        // Create the simulator instance for this target
        if (this.target.simulator) {
            let settings = {
                registers: this.target.registers || {},
                outputs: this.target.simulator.options.outputs || []
            };

            // Create output tabs
            (settings.outputs || []).forEach( (output, i) => {
                settings.outputs[i] = new output.panel(this.basepath, output);
                this.panels.add(settings.outputs[i]);
            });

            this._simulator = new this.target.simulator(this.console, this.basepath, settings, this.target, this.labelListing);
        }
        else {
            this._simulator = null;
        }

        if (this.target.debugger) {
            // Create the simulator instance for this target
            let debuggerSettings = {
                registers: this.target.registers || {},
                outputs: this.target.debugger.options.outputs || []
            };

            // Create output tabs
            (debuggerSettings.outputs || []).forEach( (output, i) => {
                // Debugger tab is always labeled as such
                if (i == 0) {
                    output.name = "run.panels.debugger";
                }
                debuggerSettings.outputs[i] = new output.panel(this.basepath, output);
                this.panels.add(debuggerSettings.outputs[i]);
            });

            // Pass the files to a new debugger (if any; when it is ready)
            this._debugger = new this.target.debugger(this.basepath, debuggerSettings, this.simulator, this.target);

            this.debugger.on("step", () => {
                if (!this.debugger.simulator.done) {
                    this.step();
                }
            });

            this.debugger.on("continue", () => {
                if (!this.debugger.simulator.done) {
                    this.run();
                }
            });

            this.debugger.on("breakpoint-set", (address) => {
                this.codeListing.check(address);
                if (this.simulator) {
                    this.simulator.setBreakpoint(address);
                }
            });

            this.debugger.on("breakpoint-clear", (address) => {
                this.codeListing.uncheck(address);
                if (this.simulator) {
                    this.simulator.clearBreakpoint(address);
                }
            });
        }
        else {
            this._debugger = null;
        }
    }

    /**
     * Returns the active debugger, if any.
     */
    get debugger() {
        return this._debugger;
    }

    /**
     * Returns the runtime panel interface.
     */
    get panels() {
        return this._panels;
    }

    /**
     * Returns the output console.
     */
    get console() {
        return this._console;
    }

    /**
     * Returns the CodeListing element of the interface.
     */
    get codeListing() {
        return this._codeListing;
    }

    /**
     * Returns the LabelListing element of the interface.
     */
    get labelListing() {
        return this._labelListing;
    }

    /**
     * Returns the TargetSelector element of the interface.
     */
    get targetSelector() {
        return this._targetSelector;
    }

    /**
     * Returns the RegisterListing element of the interface.
     */
    get registerListing() {
        return this._registerListing;
    }

    /**
     * Returns the MemoryListing element of the interface.
     */
    get memoryListing() {
        return this._memoryListing;
    }

    /**
     * Returns the Tabs element of the main tabs on the interface.
     */
    get tabs() {
        return this._tabs;
    }

    /**
     * Returns the Editors element of the interface.
     */
    get editors() {
        return this._editors;
    }

    /**
     * Returns the FileListing element of the interface.
     */
    get fileListing() {
        return this._fileListing;
    }

    /**
     * Returns the Toolbar element of the interface.
     */
    get toolbar() {
        return this._toolbar;
    }

    /**
     * Returns the base URL path for any resources.
     *
     * This is useful when the application is hosted off a subroute or different
     * routes of the static site are accessed upon first load.
     */
    get rootpath() {
        // Determine the rootpath for any relative ajax calls later on
        let path = window.location.pathname;
        path = path.split("/");
        path = path.slice(0, path.length - 1);
        path = path.join("/");
        if (!path.startsWith("/")) {
            path = "/" + path;
        }
        if (!path.endsWith("/")) {
            path = path + "/";
        }
        return window.location.origin + path;
    }

    /**
     * Retrieves the base path for any access.
     */
    get basepath() {
        return document.body.getAttribute('data-basepath');
    }

    /**
     * Retrieves the current active Simulator instance.
     */
    get simulator() {
        return this._simulator;
    }

    async run() {
        if (this.simulator.paused) {
            // Resume simulation
            this.toolbar.setStatus("step", "");
            this.toolbar.setStatus("run", "active");

            // Resume
            await this.simulator.resume();
        }
        else {
            // Run from start!

            // Stop if the simulation is running
            if (this.simulator.running) {
                await this.stop();
            }

            // Reset if the simulation is in its 'done' state
            if (this.simulator.done) {
                await this.simulator.reset();
                await this.debugger.reset();
                await this.panels.reset();

                if (this.simulator.state !== Simulator.STATE_READY) {
                    throw new Error("simulator not ready after reset");
                }
            }

            // Start the simulation
            await this.simulator.run();

            // Enable the pause button
            this.toolbar.setStatus("step", "");
        }
    }

    async stop() {
        await this.simulator.stop();

        if (this.simulator.state !== Simulator.STATE_DONE) {
            throw new Error("simulator did not shut down properly");
        }
    }

    async pause() {
        await this.simulator.pause();

        if (this.simulator.state !== Simulator.STATE_PAUSED) {
            throw new Error("simulator failed to pause");
        }

        // Update toolbar
        this.toolbar.setStatus("run", "");
        this.toolbar.setStatus("step", "active");
    }

    async step() {
        // Get the instruction highlighted that we want to run
        var info = this.codeListing.highlightedLine;

        // Unhighlight
        this.codeListing.unhighlight();

        // Have the simulator 'step' that instruction
        await this.simulator.step(info);
    }

    async assemble() {
        // Clear code listing
        this.codeListing.clear();

        // Clear registers
        this.registerListing.clear();

        // Clear memory
        this.memoryListing.clear();

        // Clear labels
        this.labelListing.clear();

        // Clear error flags on the file listing
        this.fileListing.clearAnnotations();

        // Clear the console
        this.console.clear();

        // TODO: move ebreak knowledge out of the file processor
        this._ebreaks = [];

        // For each file that has changed, assemble it and then link them.
        // That is: Iterate over the directory of the current project and
        // assemble every file that is newer than any cached assembled result.

        // Get the parent project directory of the open file.
        let projectDirectory = null;
        let current = this.fileListing.active;
        while (current) {
            if (current.classList.contains("project")) {
                projectDirectory = current;
                break;
            }
            current = this.fileListing.parentOf(current);
        }

        // Get and retain the list of files for the project
        let projectInfo = this.fileListing.infoFor(projectDirectory);
        this._projectInfo = projectInfo;

        let listing = await this.fileListing.list(projectDirectory);

        // Set up disassembler
        let disassembler = null;
        if (this.target.disassembler) {
            disassembler = new this.target.disassembler(this.console, projectInfo.name, this.basepath, this.target);

            disassembler.on('instruction', (instruction) => {
                this.codeListing.add(instruction);

                if (instruction.code.match('ebreak')) {
                    this.codeListing.check(instruction.address);
                    this._ebreaks.push(instruction.address);
                }
            });
        }

        // Set up dumper
        let memoryDumper = null;
        if (this.target.memoryDumper) {
            memoryDumper = new this.target.memoryDumper(this.console, projectInfo.name, this.basepath, this.target);

            memoryDumper.on('update', (row) => {
                this.memoryListing.update(row.address, row.data);
            });
        }

        // Set up label dumper
        let labelDumper = null;
        if (this.target.labelDumper) {
            labelDumper = new this.target.labelDumper(this.console, projectInfo.name, this.basepath, this.target);

            let labelArray = [];
            labelDumper.on('update', (row) => {
                labelArray.push(row);
            });

            labelDumper.on('done', (row) => {
                labelArray.sort((a, b) => parseInt(a.address, 16) - parseInt(b.address, 16));
                labelArray.forEach( (element) => this.labelListing.update(element.label, element.section, element.bind, element.address) );
            });
        }

        let processor = new Processor(this.basepath, this.target, this.fileListing, this.codeListing, this.console, this.simulator, disassembler);

        processor.on('invoke', (info) => {
            // Clear annotations for pending files
            if (this._annotations[info.path]) {
                delete this._annotations[info.path];
            }

            if (this._annotationsCache[info.path]) {
                delete this._annotationsCache[info.path];
            }

            let editor = this.editors.editorFor(info);
            if (editor) {
                // Set annotations for the current editor
                editor.annotations = [];
            }
        });

        this._binary = await processor.build();


        // Parse errors
        if (processor.errors) {
            for (let error of processor.errors) {
                let errorPath = projectInfo.path + "/" + error.file;
                await this.fileListing.annotate(errorPath, error.type);
                this._annotations[errorPath] = this._annotations[errorPath] || [];
                this._annotations[errorPath].push(error);

                let editor = this.editors.editorFor({ path: errorPath });
                if (editor) {
                    // Set annotations for the current editor
                    editor.annotations = this._annotations[errorPath];
                }
            }
        }

        // Do we have a binary?
        if (!this._binary) {
            this.toolbar.setStatus("assemble", "failure");

            for (let error of linker.errors) {
                let errorPath = projectInfo.path + "/" + error.file;
                await this.fileListing.annotate(errorPath, error.type);
                this._annotations[errorPath].push(error);

                let editor = this.editors.editorFor({ path: errorPath });
                if (editor) {
                    // Set annotations for the current editor
                    editor.annotations = this._annotations[errorPath];
                }
            }
            return;
        }

        // Also, disassemble the binary
        if (this.target.disassembler) {
            disassembler.add(this._binary);
            await disassembler.invoke();
        }

        // And attach the static memory dumper
        if (this.target.memoryDumper) {
            memoryDumper.add(this._binary);
            await memoryDumper.invoke();
        }

        // And the label dumper
        if (this.target.labelDumper) {
            labelDumper.add(this._binary);
            await labelDumper.invoke();
        }

        this.toolbar.setStatus("assemble", "success");
        this.toolbar.setStatus("run", "");

        // On success, go to the run tab
        this.tabs.select('run-panel');

        // Pass along the binary
        this.simulator.runBinary = this._binary;

        // When the simulator ends
        this.simulator.on("change", (reason) => {
            let state = this.simulator.state;
            if (state === Simulator.STATE_READY) {
            }
            else if (state === Simulator.STATE_RUNNING) {
                // Update console
                this.console.writeln(this._process, "");
                this.console.writeln(this._process, "Simulation started.");

                // When the simulator is running, unhighlight
                this.codeListing.unhighlight();

                // Tell the debugger the simulation is running
                // TODO: debugger call-in
            }
            else if (state === Simulator.STATE_DONE) {
                // Update console
                this.console.writeln(this._process, "Simulation ended.");
                this.console.done(this._process, false);

                // Update the toolbar buttons
                this.toolbar.setStatus("run", "success");
                this.toolbar.setStatus("step", "disabled");

                // Update the memory view
                // TODO: have the memory listing do that work via the memory interface
                /*
                    let numRows = this.memoryListing.numberOfRows;
                    let addresses = this.memoryListing.addresses;
                    this.memoryListing.clear();
                    for (let i = 0; i < numRows; i++) {
                        let data = this.simulator.readMemory(parseInt(addresses[i], 16), 32);
                        this.memoryListing.update(addresses[i], data);
                    }
                */
            }
            else if (state === Simulator.STATE_PAUSED) {
                // Update console
                if (reason) {
                    this.console.writeln(this._process, "Simulation paused: " + reason);
                }
                else {
                    this.console.writeln(this._process, "Simulation paused.");
                }

                this.toolbar.setStatus("run", "paused");
                this.toolbar.setStatus("step", "active");

                // Get updated memory
                // TODO: Move to some memory interface
                /*
                let numRows = this.memoryListing.numberOfRows;
                let addresses = this.memoryListing.addresses;
                this.memoryListing.clear();
                for (let i = 0; i < numRows; i++) {
                    let data = this.simulator.readMemory(parseInt(addresses[i], 16), 32);
                    this.memoryListing.update(addresses[i], data);
                }
                */

                // Highlight code line and scroll to it
                // (really this happens because of the prior event)
                this.codeListing.highlight(this.simulator.pc.value.toString(16));
            }
        });

        // Bind an event when the program counter updates
        this.simulator.pc.on('change', () => {
            // Highlight the address of the current instruction
            this.codeListing.highlight(this.simulator.pc.value.toString(16));
        });

        // When the simulator wants to warn us
        this.simulator.on('warning', (data) => {
            const warning = data.warning;
            const reg = data.reg;
            const pc = data.at;
            let info = this.codeListing.infoFor(pc);

            if (info) {
                // We need to append the path for the file name
                info.path = `${this._projectInfo.path}/${info.file}`;

                const line_warning = {
                    row: parseInt(info.row) - 1,
                    column: 0,
                    type: 'warning',
                    text: warning + " " + reg
                };

                // Set annotations for this path
                this._annotations[info.path] = this._annotations[info.path] || [];
                this._annotationsCache[info.path] = this._annotationsCache[info.path] || {};

                let key = `${info.path}@${line_warning.type}@${line_warning.text}@${line_warning.row}@${line_warning.column}`;
                if (!this._annotationsCache[info.path][key]) {
                    this._annotationsCache[info.path][key] = true;
                    this._annotations[info.path].push(line_warning);

                    // Adds the warning icon to the editor next to the corresponding line number
                    let editor = this.editors.editorFor(info);
                    if (editor) {
                        // Set annotations for the current editor
                        editor.annotations = this._annotations[info.path];
                    }

                    if (this._process && this.console) {
                        try {
                            this.console.writeln(this._process, `Warning: ${warning} for register ${reg} at ${info.file}:${info.row}`);
                        }
                        catch (e) {
                            //console.log(e);
                        }
                    }
                }
            }
        });

        // Wait until the simulator loads
        this._process = this.console.newProcess("Running", this._binary.name);

        // If the simulator is being reused, re-initialize
        if (!this.simulator.uninitialized) {
            await this.simulator.reload(this._process);
        }
        else {
            await this.simulator.load(this._process);
        }

        if (this.simulator.state !== Simulator.STATE_READY) {
            throw new Error(`simulator not ready: ${this.simulator.state}`);
        }

        // Load the register listing with the initial values from the simulator
        this.registerListing.update(this.simulator.registers);

        // And then connect the debugger
        if (this.debugger) {
            this.debugger.load().then( () => {
                this.debugger.connect();
            });
        }
    }
}
