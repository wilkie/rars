// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Base class to describe a project target.
 *
 * Every target will have a set of features tied to particular files to handle
 * compilation, assembly, linking, etc.
 *
 * Handlers for processing files are defined used `registerHandler` with a
 * regular expression. Only one registered handler will be instantiated during
 * the processing passes and only when such a file is found.
 *
 * Handlers might be used to process the output of another and will be
 * instantiated on demand in that case.
 *
 * Handlers might ask for the 'aggregate' of the files, which are all files
 * matching the matcher. This implies they are part of the second processing
 * pass. That means they will only be instantiated on a successful first
 * pass... when all input files are processed. This is useful for processes
 * such as linkers that act on all compiled files to produce a final binary.
 */
export class Target {
    constructor() {
        // Tracks the file handlers for processing.
        this._matchers = [];
        this._handlers = [];

        // Tracks the file types known to this target.
        this._fileMatchers = [];
        this._fileTypes = [];
        this._fileOptions = [];

        // Tracks the types of projects.
        this._projectNames = [];
        this._projects = {};
    }

    /**
     * Returns the name of the target.
     */
    get name() {
        return "none";
    }

    /**
     * Retrieves the human-legible name of this target.
     */
    get title() {
        return this.name;
    }

    /**
     * Returns the handler for the given file.
     */
    handlerFor(filename) {
        for (let i = 0; i < this._matchers.length; i++) {
            const matcher = this._matchers[i];
            if (filename.match(matcher)) {
                return this._handlers[i];
            }
        }

        return undefined;
    }

    /**
     * Registers a handler to process a particular file.
     *
     * For every file matching the given regular expression, pass to an
     * instantiation of the handler.
     *
     * @param {RegExp} match - The regular expression to match filenames.
     * @param {Process} handler - The process definition to instantiate.
     */
    registerHandler(match, handler) {
        this._matchers.push(match);
        this._handlers.push(handler);
    }

    /**
     * Returns the editor info for a given file, if one is specified.
     *
     * Returns something indicating a type of 'binary' if no match is found.
     *
     * @returns {object} The info block containing `type` and `options`.
     */
    typeFor(filename) {
        for (let i = 0; i < this._fileMatchers.length; i++) {
            const matcher = this._fileMatchers[i];
            if (filename.match(matcher)) {
                return {
                    type: this._fileTypes[i],
                    options: this._fileOptions[i]
                };
            }
        }

        return {
            type: 'binary',
            options: {}
        };
    }

    /**
     * Registers a type association to open and edit files.
     *
     * The type itself must have been registered as well if it is bespoke.
     *
     * Types are specified as either a generic string such as 'text' or 'binary'
     * or as a tuple using a '/' delimiter to further specify. For example
     * `assembly_riscv/text` will match a text viewer or, more specifically, a
     * `assembly_riscv/text` viewer, if one exists. The common editor for `text`
     * might react to the specified type. In our case, the `assembly_riscv` can
     * be used by the generic TextEditor to load syntax highlighting for that
     * particular language class.
     *
     * @param {RegExp} match - The regular expression to match filenames.
     * @param {string} type - The type expression.
     * @param {object} options - The options to pass to the editor.
     */
    registerType(match, type, options = {}) {
        this._fileMatchers.push(match);
        this._fileTypes.push(type);
        this._fileOptions.push(options);
    }

    /**
     * Returns the file listing, metadata and/or data for the given new project.
     *
     * The names of the projects can be requested via the `projects` property.
     *
     * Targets populate the projects via their own `registerProject` method.
     *
     * @param {string} name - The name of the project.
     */
    projectFor(name) {
        return this._projects[name];
    }

    /**
     * Registers a project type for this target.
     *
     * Each file in `files` is an object that consists of at least a `name`
     * which will be the name of the file. The `data` field can hold the initial
     * text of data of that file. The `type` field can optionally assign the
     * file type.
     *
     * @param {string} name - The human facing name of the project.
     * @param {Array} files - A list of file metadata blocks for each file.
     */
    registerProject(name, files = []) {
        this._projectNames.push(name);
        this._projects[name] = files;
    }

    /**
     * Returns a list of project names.
     *
     * This returns a list in the order the Target registered each.
     */
    get projects() {
        return this._projectNames;
    }

    /**
     * Retrieves the Disassember class definition, if any.
     *
     * Returns undefined if there is no way to disassemble for this target.
     */
    get disassembler() {
        return undefined;
    }

    /**
     * Retrieves the MemoryDumper class definition, if any.
     *
     * Returns undefined if there is no way to dump memory for this target.
     */
    get memoryDumper() {
        return undefined;
    }

    /**
     * Retrieves the LabelDumper class definition, if any.
     *
     * Returns undefined if there is no way to get labels for this target.
     */
    get labelDumper() {
        return undefined;
    }

    /**
     * Retrieves the Debugger class definition, if any.
     *
     * Returns undefined if there is no way to interactively debug this target.
     */
    get debugger() {
        return undefined;
    }
}

class Blah {
    /**
     * Handle hovering over a word in the document.
     *
     * @param {Editor} editor - The editor instance.
     * @param {String} line - The line under the cursor.
     * @param {String} word - The word under the cursor.
     * @param {String} kind - The category of highlight.
     * @param {number} x - The x coordinate of the cursor.
     * @param {number} y - The y coordinate of the cursor.
     */
    onMouseover(editor, line, word, kind, x, y) {
    }

    /**
     * Returns a set of completion items for the given editor context.
     *
     * Each item has a set of keys. The `name` key is the label to use to
     * describe the item. The `value` is the text that would be completed. The
     * `score` is the relevancy out of `100`. The `meta` tag is a category that
     * this item resides in. If there is a `docHTML` tag within the item, this
     * will render this documentation alongside the choice when it is
     * highlighted to provide extra context.
     *
     * @param {Editor} editor - The editor instance.
     * @param {String} line - The line under the cursor.
     * @param {String} prefix - The preceding context before the cursor.
     * @param {String} word - The word under the cursor.
     * @param {String} kind - The category of highlight.
     * @param {number} row - The line number currently.
     * @param {number} column - The column number currently.
     *
     * @returns {Array} A set of descriptions of completion items.
     */
    onCompletion(editor, line, prefix, word, kind, row, column) {
        return [];
    }

    /**
     * Called when a file is loaded.
     *
     * @param {Editor} editor - The editor instance.
     * @param {String} text - The source of the file.
     * @param {String} filename - The filename for the file.
     */
    onLoad(editor, text, filename) {
    }

    /**
     * Called when the caret position changes.
     *
     * @param {Editor} editor - The editor instance.
     * @param {number} row - The new line number we are on.
     * @param {number} column - The new column number we are on.
     * @param {String} line - The line under the cursor.
     * @param {String} word - The word under the cursor.
     * @param {String} kind - The category of highlight.
     * @param {Object} last - The old row, column, line, word, kind.
     */
    onPosition(editor, row, column, line, word, kind, last) {
    }
}
