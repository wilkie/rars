// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Extending this class allows a component to provide event handling and
 * callbacks.
 */
export class EventComponent {
    constructor() {
        this._events = {};
    }

    /**
     * A generic event callback.
     *
     * @callback eventCallback
     * @param {data} Any event data.
     */

    /**
     * Sets a callback for the given event.
     *
     * @param {string} name - The name of the event.
     * @param {eventCallback} callback - The callback function.
     */
    on(name, callback) {
        if (callback === undefined) {
          return this._events[name];
        }

        var eventName = name;
        var componentName = "__global__";

        if (name.includes(".")) {
            var parts = name.split(".");
            eventName = parts[0];
            componentName = parts[1];
        }

        if (!(eventName in this._events)) {
            this._events[eventName] = {};
        }
        this._events[eventName][componentName] = callback;

        return this;
    }

    /**
     * Removes a callback for the given event.
     *
     * @param {string} name The name of the event.
     */
    off(name) {
        var eventName = name;
        var componentName = "__global__";

        if (name.includes(".")) {
            var parts = name.split(".");
            eventName = parts[0];
            componentName = parts[1];
        }

        if (this._events[eventName] && this._events[eventName][componentName]) {
            delete this._events[eventName][componentName];
        }
        if (eventName in this._events && Object.keys(this._events[eventName]).length == 0) {
            delete this._events[eventName];
        }
        return this;
    }

    /**
     * Triggers an event of the given event type.
     *
     * @param {string} name - The name of the event.
     * @param {any} data - The data to send along with the event.
     */
    trigger(name, data) {
        if (this._events[name]) {
          Object.keys(this._events[name]).forEach( componentName => this._events[name][componentName].call(this, data) );
        }

        return this;
    }

    /**
     * Returns the @eventCallback of the given event name if it has been
     * registered.
     *
     * @param {string} name - The name of the event.
     *
     * @returns {eventCallback}
     */
    callbackFor(name) {
        if (!this._events[name]) {
            return null;
        }

        return this._events[name]["__global__"];
    }
}
