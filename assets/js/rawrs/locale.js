// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Represents the current locale for localization and translation.
 *
 * Localization will occur via the `update` function and will replace the text
 * content of any element with a `data-i18n-key` attribute with the translation
 * based on the value of that attribute as a key into the locale data file.
 *
 * The locale data file is a table of keys such that a key in the form 'a.b.c'
 * is translated to crawl that table from left to right each sub key 'a', 'b',
 * and then finally retrieving the string at key 'c'.
 *
 * The locale data files are translated from their YAML form in the source tree
 * which is used by the normal HTML renderer into a JSON form consumed here.
 * Otherwise, the structure of the data and the translation keys are exactly the
 * same as the normal application.
 *
 * The HTML views have to take care that they render the default locale into the
 * HTML and also, redundantly, apply a `data-i18n-key` attribute on any element
 * or content that would need to dynamically change if added late or when the
 * locale is changed during the application runtime and without a hard browser
 * refresh.
 */
export class Locale {
    /**
     * Creates an instance representing the given language code.
     */
    constructor(basepath, code = "en", fallback = null) {
        // Retain basepath
        this._basepath = basepath;

        // Retain code
        this._code = code;

        // Retain fallback
        this._fallback = fallback;
    }

    /**
     * Returns the base path for the application.
     */
    get basepath() {
        return this._basepath;
    }

    /**
     * Returns the raw locale data object.
     */
    get data() {
        return this._data;
    }

    /**
     * Returns the language code this locale instance represents.
     */
    get code() {
        return this._code;
    }

    /**
     * Returns the fallback Locale object, if any, to be used when a key is not found.
     */
    get fallback() {
        return this._fallback;
    }

    /**
     * Loads the locale information.
     */
    async load() {
        if (!this._data) {
            // Get the localization.
            let response = await fetch(this.basepath + "locales/" + this.code + ".json");
            this._data = await response.json();

            // Update the document direction based on the value of `direction`.
            document.body.parentNode.setAttribute('dir', this.direction);
        }
    }

    /**
     * Returns the `dir` string for the locale.
     */
    get direction() {
        return this.translate("direction");
    }

    /**
     * Translates provided the given key.
     *
     * @param {string} key - The translation key used to traverse the locale data.
     */
    translate(key) {
        let parts = key.split('.');
        let data = this.data;
        parts.forEach( (subkey) => {
            if (data) {
                data = data[subkey];
            }
        });

        if (data && data.endsWith) {
            return data;
        }

        return key;
    }

    /**
     * Convenience method for translate.
     *
     * @param {string} key - The translation key used to traverse the locale data.
     */
    t(key) {
        return this.translate(key);
    }

    /**
     * Translates the given element for locale keys.
     *
     * Any element with a `data-i18n-key` attribute will have its text content
     * filled in with the translation found in the locale.
     */
    async update(element) {
        await this.load();

        element.querySelectorAll('[data-i18n-key]').forEach( (el) => {
            el.textContent = this.translate(el.getAttribute('data-i18n-key'));
        });

        element.querySelectorAll('[data-title-i18n-key]').forEach( (el) => {
            el.setAttribute('title', this.translate(el.getAttribute('data-title-i18n-key')));
        });
    }
}
