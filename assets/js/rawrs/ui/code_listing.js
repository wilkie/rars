// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { EventComponent } from '../event_component';

/**
 * This widget shows the disassembled code and allows the setting of breakpoints
 * and shows the mapping between the machine code and the original source.
 */
export class CodeListing extends EventComponent {
    /**
     * Create a new instance of a code listing for the given root element.
     *
     * @param {HTMLElement} root The element to look for the code listing within.
     */
    constructor(root) {
        super();

        let element = root.querySelector("table.instructions");
        this._element = element;

        this._sourceLines = {};
    }

    /**
     * Adds a source file to the code listing knowledge-base.
     *
     * When an instruction references the source code, it will use this text to
     * fill out the listing.
     */
    addSource(filename, text) {
        this._sourceLines[filename] = text.split("\n");
    }

    /**
     * Returns the element associated with this code listing.
     *
     * @returns {HTMLElement} The element for this code listing.
     */
    get element() {
        return this._element;
    }

    /**
     * Clears the code listing.
     */
    clear() {
        this.unhighlight();
        this._element.classList.add("empty");
        this._element.querySelectorAll("tbody tr").forEach( (x) => x.remove() );
    }

    /**
     * Unhighlights any highlighted line.
     */
    unhighlight() {
        this._highlighted = null;
        this._element.querySelectorAll(".highlighted").forEach( (element) => {
            element.classList.remove("highlighted");
        });
    }

    /**
     * Highlights the instruction at the given address.
     *
     * Only one line can be highlighted at a time.
     *
     * @param {string} address The address as a hex string, i.e. "400004".
     */
    highlight(address) {
        this._highlighted = address;

        this._element.querySelectorAll(".highlighted").forEach( (element) => {
            element.classList.remove("highlighted");
        });

        var element = this._element.querySelector(".address-" + address);
        if (element) {
            element.parentNode.classList.add("highlighted");
        }
    }

    /**
     * Retrieves the highlighted instruction.
     */
    get highlightedLine() {
        let highlighted = this._element.querySelector(".highlighted");
        if (!highlighted) {
            return null;
        }

        let ret = {};

        ret.machineCode = highlighted.querySelector("td.machine-code").textContent;
        ret.address     = highlighted.querySelector("td.address").textContent;
        ret.code        = highlighted.querySelector("td.code").textContent;
        ret.row         = highlighted.querySelector("td.row").textContent;

        return ret;
    }

    /**
     * Marks the given address as having its breakpoint set.
     */
    check(address) {
        var element = this._element.querySelector(".address-" + address);
        if (element) {
            let breakpointCell = element.parentNode.querySelector("td.breakpoint");
            let checkbox = breakpointCell.querySelector("input");
            checkbox.checked = true;
            breakpointCell.classList.add("checked");
        }
    }

    /**
     * Removes the mark for the given address so as to clear its breakpoint.
     */
    uncheck(address) {
        var element = this._element.querySelector(".address-" + address);
        if (element) {
            let breakpointCell = element.parentNode.querySelector("td.breakpoint");
            let checkbox = breakpointCell.querySelector("input");
            checkbox.checked = false;
            breakpointCell.classList.remove("checked");
        }
    }

    /**
     * Adds an instruction.
     */
    add(info) {
        let file = info.file.substring("/input/".length);

        this._element.classList.remove("empty");
        var row = document.createElement("tr");
        row.setAttribute('data-address', info.address);

        function createCell(type, value) {
            var cell = document.createElement("td");
            cell.classList.add(type);
            cell.textContent = value;
            return cell;
        }

        var rowText = info.row;
        if (this._last && this._last.row == info.row) {
            rowText = "";
        }

        var breakpointCell = document.createElement("td");
        breakpointCell.classList.add("breakpoint");
        var checkbox = document.createElement("input");
        checkbox.setAttribute("type", "checkbox");
        checkbox.addEventListener("change", (event) => {
            if (checkbox.checked) {
                breakpointCell.classList.add("checked");
                this.trigger("breakpoint-set", info.address);
            }
            else {
                breakpointCell.classList.remove("checked");
                this.trigger("breakpoint-clear", info.address);
            }
        });
        breakpointCell.appendChild(checkbox);
        row.appendChild(breakpointCell);

        // Craft the row only if there is a matching source line
        if (this._sourceLines[file] && this._sourceLines[file][info.row - 1]) {
            var addressCell = createCell("address", info.address);
            addressCell.classList.add("address-" + info.address);
            row.appendChild(addressCell);
            row.appendChild(createCell("machine-code", info.machineCode));
            row.appendChild(createCell("code", info.code));
            row.appendChild(createCell("file", (rowText != "") ? file : ""));
            row.appendChild(createCell("row", rowText));
            row.appendChild(createCell("original", (rowText ? this._sourceLines[file][info.row - 1] : "")));

            if (info.address == this._highlighted) {
                row.classList.add('highlighted');
            }

            this._element.querySelector("tbody").appendChild(row);
        }

        this._last = info;
    }

    /**
     * Returns information about the code given the address.
     *
     * Only can report knowledge about this address as known by this
     * CodeListing. That is, if the code was modified during runtime, it might
     * not be reflected correctly here.
     *
     * Will return `undefined` if no such row is found.
     *
     * @param {string} address - The address to look up.
     *
     * @returns {object} The information about that address (or `undefined`).
     */
    infoFor(address) {
        let info = undefined;

        let row = this.element.querySelector(`tr[data-address="${address}"]`);
        if (row) {
            info = {};
            info.address = address;
            info.machineCode = row.querySelector("td.machine-code").textContent;
            info.code = row.querySelector("td.code").textContent;

            // These might be in the prior rows, though
            while (row && !row.querySelector("td.file").textContent.trim()) {
                row = row.previousElementSibling;
            }

            info.file = row.querySelector("td.file").textContent;
            info.row = row.querySelector("td.row").textContent;
            info.original = row.querySelector("td.original").textContent;
        }

        return info;
    }
}
