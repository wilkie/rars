// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { EventComponent } from '../event_component';

import { Dropdown } from './dropdown.js';

export class Tabs extends EventComponent {
    constructor(tabStrip) {
        super();

        this._tabStrip = tabStrip;
        this._tabPanelCount = 0;

        tabStrip.querySelectorAll(".tab").forEach( (tab) => {
            this.bindTabEvents(tab);
        });
    }

    bindTabEvents(tab) {
        tab.querySelectorAll("a, button:not(.actions):not(.action)").forEach( (tabButton) => {
            tabButton.addEventListener("click", (event) => {
                this.select(tabButton.getAttribute('aria-controls'));

                event.stopPropagation();
                event.preventDefault();
            });

            tabButton.addEventListener("mouseup", (event) => {
                if (event.button == 1) {
                    // Middle click... fire a 'close' action.
                    this.trigger('action', {
                        tab: tab,
                        action: 'close'
                    });
                }

                event.stopPropagation();
                event.preventDefault();
            });
        });

        // Initialize the dropdown
        let actionButton = tab.querySelector('button.actions');
        if (actionButton) {
            tab.dropdown = new Dropdown(actionButton);

            tab.dropdown.on("click", (action) => {
                this.trigger('action', {
                    tab: tab,
                    action: action
                });
            });
        }
    }

    get selected() {
        let activeTab = this._tabStrip.querySelector(".tab.active");
        if (!activeTab) {
            return null;
        }

        return activeTab.querySelector("a, button:not(.actions):not(.action)");
    }

    select(id) {
        // Unselect all tabs
        this._tabStrip.querySelectorAll(".tab").forEach( (tab) => {
            tab.classList.remove("active");
        });

        // Get the tab button corresponding to the identifier
        let tabButton = this._tabStrip.querySelector("a[aria-controls=\"" + id + "\"], button[aria-controls=\"" + id + "\"]");
        if (!tabButton) {
            // Bail if we cannot find the appropriate button for the tab.
            throw `tab at ${id} does not have a button`;
        }

        tabButton.parentNode.classList.add("active");

        // Let upstreams know about the change
        this.trigger("change", tabButton);

        // Find the tab panel
        var tabPanels = this._tabStrip.nextElementSibling;
        if (tabPanels) {
            tabPanels.querySelectorAll(":scope > .tab-panel").forEach( (tabPanel) => {
                tabPanel.classList.remove("active");
            });
        }

        var tabPanel = document.querySelector(".tab-panel#" + tabButton.getAttribute('aria-controls'));
        if (tabPanel) {
            let rootpath = document.body.getAttribute('data-rootpath');
            tabPanel.classList.add("active");
            let url = tabButton.getAttribute('href');
            if (tabButton.previousElementSibling) {
                url = tabButton.previousElementSibling.getAttribute('href');
            }
            if (tabPanel.querySelector("li.tab.active > a:not(.ajax)")) {
                url = tabPanel.querySelector("li.tab.active > a:not(.ajax)").getAttribute('href');
            }

            if (url) {
                url = rootpath + url;
                window.history.replaceState(window.history.start, "", url);
            }

            // Check if the tabPanel is PJAX loaded
            if (!tabPanel.classList.contains("pjax-loaded")) {
                var pjaxURL = tabPanel.getAttribute('data-pjax');
                if (tabButton.parentNode.querySelector("a.ajax")) {
                    pjaxURL = tabButton.parentNode.querySelector("a.ajax").getAttribute('href');
                }
                if (pjaxURL) {
                    pjaxURL = rootpath + pjaxURL;
                    // Fetch HTML page and get content at "body"
                    tabPanel.classList.add("pjax-loaded");
                    fetch(pjaxURL, {
                        credentials: 'include'
                    }).then(function(response) {
                        return response.text();
                    }).then(function(text) {
                        // Push text to dummy node
                        var dummy = document.createElement("div");
                        dummy.setAttribute('hidden', '');
                        dummy.innerHTML = text;
                        document.body.appendChild(dummy);
                        var innerElement = dummy.querySelector(".content.documentation");
                        tabPanel.innerHTML = "";
                        tabPanel.appendChild(innerElement);
                        dummy.remove();

                        window.history.replaceState(window.history.start, "", url);

                        if (tabPanel.querySelector("li.tab.active > a")) {
                            url = tabPanel.querySelector("li.tab.active > a").getAttribute('href');
                        }
                        window.history.replaceState(window.history.start, "", url);
                    });
                }
            }
        }

        return tabButton;
    }

    /**
     * Returns the HTMLElement representing the tab strip.
     */
    get element() {
        return this._tabStrip;
    }

    /**
     * Adds the given tab and panel to the tab strip.
     */
    add(tab, panel) {
        // Add tab to interface.
        this._tabStrip.appendChild(tab);

        // Ensure we know which were added.
        tab.classList.add('added-tab');
        panel.classList.add('added-tab-panel');

        // Add tab panel to interface.
        var tabPanels = this._tabStrip.nextElementSibling;
        if (tabPanels) {
            tabPanels.appendChild(panel);
        }

        // Let's ensure that the tab and tab panel are linked.
        let button = tab.querySelector('button:not(.actions)[aria-controls]');

        // Come up with a generic identifier.
        this._tabPanelCount++;
        let id = `tab-panel-${this._tabPanelCount}`
        if (button) {
            // We already have one.
            id = button.getAttribute('aria-controls');

            // Ensure the panel matches.
            if (id !== panel.getAttribute('id')) {
                throw 'aria-controls must match given tab panel id';
            }

            // Ensure the id is unique.
            if (document.body.querySelectorAll('#' + id).length > 1) {
                throw 'aria-controls/id must be unique';
            }
        }
        else {
            button = tab.querySelector('button:not(.actions):not(.action)');
            button.setAttribute('aria-controls', id);
            panel.setAttribute('id', id);

            // Also make sure that the dropdown menu has such an identifier,
            // if any.
            let actionButton = tab.querySelector('button.actions');
            if (actionButton) {
                actionButton.setAttribute('aria-controls', `dropdown-${id}`);

                let dropdown = tab.querySelector('ul.dropdown-menu');
                dropdown.setAttribute('id', `dropdown-${id}`);
            }
        }

        // Bind events.
        this.bindTabEvents(tab);
    }

    /**
     * Removes the given tab and tab panel.
     *
     * @param {HTMLElement} tab - The tab element.
     */
    remove(tab) {
        // Find the tab panel and destroy it.
        let button = tab.querySelector("a, button:not(.actions):not(.action)");
        if (!button) {
            return;
        }

        let id = button.getAttribute('aria-controls');
        var tabPanel = document.querySelector(`.tab-panel#${id}`);
        if (tabPanel) {
            tabPanel.remove();
        }

        // Destroy tab.
        tab.remove();

        // If there is no selected tab, select the first tab
        if (!this.selected) {
            let nextTab = this._tabStrip.querySelector(".tab");
            if (nextTab) {
                let nextButton = nextTab.querySelector("a, button:not(.actions):not(.action)");
                if (nextButton) {
                    let id = nextButton.getAttribute('aria-controls');
                    if (id) {
                        this.select(id);
                    }
                }
            }
        }
    }

    /**
     * Removes all added tabs.
     */
    clear() {
        this._tabStrip.querySelectorAll('li.added-tab').forEach( (el) => {
            el.remove();
        });

        var tabPanels = this._tabStrip.nextElementSibling;
        if (tabPanels) {
            tabPanels.querySelectorAll('li.added-tab-panel').forEach( (el) => {
                el.remove();
            });
        }
    }

    /**
     * Instantiates or returns an existing instantiation of a given tab element.
     *
     * If no element is given, it will load all tab strips found in the current
     * document.
     */
    static load(tabStrip) {
        if (tabStrip) {
            if (!Tabs._loaded.has(tabStrip)) {
                Tabs._loaded.set(tabStrip, new Tabs(tabStrip));
            }
            return Tabs._loaded.get(tabStrip);
        }
        else {
            document.querySelectorAll(".tabs:not(.example)").forEach( (tabStrip) => {
                Tabs.load(tabStrip);
            });
        }
    }
}

Tabs._loaded = new WeakMap();
