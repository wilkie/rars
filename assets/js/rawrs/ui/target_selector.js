// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { EventComponent } from '../event_component.js';

import { TargetViewerDialog } from '../dialogs/target_viewer_dialog.js';

/**
 * This represents the target selection element.
 *
 * Selecting a new target changes the file listing, the output panels in the run
 * tab, and generally changes the operations and simulation overall.
 *
 * The target is also the initial subdirectory in the file listing. When you
 * select, for instance, 'riscv64' as a target, it shows you the /riscv64/
 * directory. All new projects will be placed in that directory, as well.
 */
export class TargetSelector extends EventComponent {
    /**
     * Create a new instance of a target selector for the given element.
     *
     * @param {HTMLElement} root - The element to look for the target selector.
     */
    constructor(basepath, root) {
        super();

        // Retain the basepath
        this._basepath = basepath;

        // Find and retain the element
        this._element = root.querySelector("select.target-selector");

        // Find the target selector button as well
        this._button = this._element.parentNode.querySelector("button");

        // Allow an initial 'set' anyway, tho
        this._initial = true;

        // Set the initial target
        this._target = this.element.value;

        // Bind events
        this.bindEvents();
    }

    /**
     * Returns the element associated with this target selector.
     *
     * @returns {HTMLElement} The element for this target selector.
     */
    get element() {
        return this._element;
    }

    /**
     * Returns the base path for the application.
     */
    get basepath() {
        return this._basepath;
    }

    /**
     * Selects the given target.
     */
    set target(value) {
        if (this._initial || this._target !== value) {
            this.element.querySelectorAll("option").forEach( (option) => {
                if (option.getAttribute("data-target-tag") === value) {
                    this._target = value;
                    this.trigger('change', this._target);

                    this.element.value = option.textContent;
                }
            });
        }

        this._initial = false;
    }

    /**
     * Returns the current selected target.
     */
    get target() {
        return this._target;
    }

    /**
     * Populates the listing with the configured target list from `target.json`.
     */
    async populate() {
        let response = await fetch(`${this.basepath}targets.json`);
        this._targets = await response.json();

        this.element.innerHTML = "";

        this._targets.forEach( (target) => {
            let option = document.createElement("option");
            option.textContent = (target.info && target.info.name) || target.tag;
            option.setAttribute("data-target-tag", target.tag);

            this.element.appendChild(option);
        });

        this.element.value = this._targets[0];
    }

    /**
     * Internal function to bind interactive events.
     *
     * We want to know when a new target is chosen.
     */
    bindEvents() {
        this.element.addEventListener('change', (event) => {
            // Update the target
            this.target = this.element.querySelector("option:checked").getAttribute("data-target-tag");

            // Remove focus
            this.element.blur();
        });

        if (this._button) {
            this._button.addEventListener('click', (event) => {
                let targetViewerDialog = new TargetViewerDialog(this.basepath);
                targetViewerDialog.on("target", (tag) => {
                    this.target = tag;
                });
            });
        }
    }
}
