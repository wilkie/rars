// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { EventComponent } from '../event_component.js';
import { Tabs } from './tabs.js';
import { Util } from '../util.js';

/**
 * This represents the section of the interface containing run panels.
 *
 * Run panels are places where output or interaction with the underlying
 * simulation can happen.
 *
 * Targets can potentially have their own panels or use generic panels. Either
 * way, such panels are any class that extends the Panel base class.
 */
export class Panels extends EventComponent {
    /**
     * Binds to the panels area on the application interface.
     */
    constructor(element) {
        super();

        // Retain element
        this._element = element.querySelector(".run-panels");

        // Retain a list of all panels
        this._panels = [];

        // Get templates
        this._tabTemplate = element.querySelector("template.output-tab");
        this._tabPanelTemplate = element.querySelector("template.output-panel");

        // Get the side tabs
        this._tabs = Tabs.load(this.element.querySelector('ol.tabs.side'));
    }

    /**
     * Returns the element associated with the output panels.
     *
     * @returns {HTMLElement} The element containing all panels.
     */
    get element() {
        return this._element;
    }

    /**
     * Returns a list of added panels.
     */
    get panels() {
        return this._panels;
    }

    /**
     * Clears all run panels.
     */
    clear() {
        this._panels = [];
        this.element.querySelector("ol.tabs.side").innerHTML = "";
        this.element.querySelector("ol.tab-panels.side").innerHTML = "";
    }

    /**
     * Adds the given panel to the interface.
     */
    add(panel) {
        // Retain reference to the panel
        this._panels.push(panel);

        // Add the panel element
        let tab = Util.createElementFromTemplate(this._tabTemplate);
        let tabPanel = Util.createElementFromTemplate(this._tabPanelTemplate);

        // Set unique identifier
        let id = "run-panel-" + this._panels.length;
        tab.querySelector("button").setAttribute('aria-controls', id);
        tabPanel.setAttribute('id', id);

        // Retain a reference to the Panel itself on the element
        tabPanel.__panel = panel;

        // TODO: Update name, icon, etc
        if (panel.settings.name) {
            tab.querySelector("button span.caption").setAttribute('data-i18n-key', panel.settings.name);
        }

        // Add it to the interface
        tabPanel.querySelector("div.output").appendChild(panel.element);

        // Get the tab strip
        let tabs = this.element.querySelector("ol.tabs.side");
        let tabPanels = this.element.querySelector("ol.tab-panels.side");

        // Make it active if it is first to be added
        if (tabs.innerHTML === "") {
            tab.classList.add("active");
            tabPanel.classList.add("active");
        }

        // Add tab and panel
        // TODO: use Tabs class
        tabs.appendChild(tab);
        tabPanels.appendChild(tabPanel);

        // Bind events to the new tab
        this._tabs.bindTabEvents(tab);

        // When the tab strip activates this
        this._tabs.on('change.' + id, (button) => {
            if (button.getAttribute('aria-controls') === panel.tabPanel.getAttribute('id')) {
                panel.__visible = true;
                panel.shown();
            }
            else {
                if (panel.__visible) {
                    panel.__visible = false;
                    panel.hidden();
                }
            }
        });

        // Call 'load'
        panel.load();

        // Call 'shown' if the first and active panel
        if (tab.classList.contains("active")) {
            panel.__visible = true;
            panel.shown();
        }

        // Tell the application we messed with the interface
        this.trigger('dom-update');
    }

    /**
     * Instructs the panels that the simulation was reset.
     */
    async reset() {
        // Just invoke `.reset` for each panel.
        for (const panel of this.panels) {
            await panel.reset();
        }
    }
}
