// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { EventComponent } from '../event_component';

export class Toolbar extends EventComponent {
    /**
     * Create a new instance of a toolbar for the given root element.
     *
     * @param {HTMLElement} root The element to look for the toolbar within.
     */
    constructor(root) {
        super();

        // Get the main element and retain it.
        let element = root.querySelector(".toolbar");
        this._element = element;

        // Get the caption element.
        this._caption = element.querySelector(".caption");

        // Bind events
        element.querySelectorAll(":scope button").forEach( (button) => {
            button.addEventListener("click", (event) => {
                this.trigger('click', button);
            });

            // Show the caption for the button upon hover
            button.addEventListener("mouseenter", (event) => {
                if (!button.hasAttribute('disabled')) {
                    this._caption.textContent = button.getAttribute('title');
                    button.setAttribute('data-old-title', this._caption.textContent);
                    button.removeAttribute('title');
                    this._caption.removeAttribute('hidden');
                }
            });

            // Hide the caption when we leave hover
            button.addEventListener("mouseleave", (event) => {
                this._caption.setAttribute('hidden', '');
                if (button.hasAttribute('data-old-title')) {
                    button.setAttribute('title', button.getAttribute('data-old-title'));
                }
            });
        });
    }

    /**
     * Returns the element associated with this toolbar.
     *
     * @returns {HTMLElement} The element for this toolbar.
     */
    get element() {
        return this._element;
    }

    /**
     * Sets the status of the given button.
     *
     * @param {String} button The button identifier.
     */
    setStatus(button, status) {
        let buttonElement = this.element.querySelector("button#" + button);
        if (buttonElement) {
            // Set the button status
            if (status) {
                buttonElement.setAttribute("data-status", status);
            }
            else {
                buttonElement.removeAttribute("data-status");
            }

            // Establish the 'disabled' status as disabling the button
            if (status === "disabled") {
                buttonElement.setAttribute("disabled", "");

                // Hide the caption, if we are hovering over it
                if (!buttonElement.hasAttribute('title')) {
                    this._caption.setAttribute('hidden', '');
                    if (buttonElement.hasAttribute('data-old-title')) {
                        buttonElement.setAttribute('title', buttonElement.getAttribute('data-old-title'));
                    }
                }
            }
            else {
                // Enable the button
                buttonElement.removeAttribute("disabled");

                // Determine caption i18n key
                let key = buttonElement.getAttribute(`data-i18n-${status || 'default'}`); 
                key ||= buttonElement.getAttribute('data-title-i18n-key');

                // Get the localized string for the caption
                let newCaption = this.locale.translate(key);

                // Close the caption, if needed
                if (buttonElement.hasAttribute('title')) {
                    buttonElement.setAttribute("title", newCaption);
                }
                else {
                    this._caption.textContent = newCaption;
                    buttonElement.setAttribute("data-old-title", newCaption);
                }
            }
        }
    }

    /**
     * Retrieves the status of the specified button or undefined if not set.
     *
     * @param {String} button The button identifier.
     * @returns String The status.
     */
    getStatus(button) {
        let buttonElement = this.element.querySelector("button#" + button);

        if (buttonElement) {
            return buttonElement.getAttribute("data-status");
        }

        return undefined;
    }

    set locale(value) {
        this._locale = value;
    }

    get locale() {
        return this._locale;
    }
}
