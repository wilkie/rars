// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { EventComponent } from '../event_component';

/**
 * This widget shows the a partial reflection of the .symtab section, specifically
 * the labels that were defined as well as the addresses of the appropriate data.
 */
export class LabelListing extends EventComponent {
    /**
     * Create a new instance of a label listing for the given root element.
     *
     * @param {HTMLElement} root The element to look for the label listing within.
     */
    constructor(root) {
        super();

        let element = root.querySelector("table.labels");
        this._element = element;

        this._labels = {};
    }

    /**
     * Returns the element associated with this label listing.
     *
     * @returns {HTMLElement} The element for this label listing.
     */
    get element() {
        return this._element;
    }

    /**
     * Clears the label listing.
     */
    clear() {
        this._element.classList.add("empty");
        this._element.querySelectorAll("tbody tr").forEach( (x) => x.remove() );
    }

    /**
     * Updates a row within the label listing
     *
     * A row consists of a label and a corresponding address, both of them strings
     *
     * @param {string} label The name of the label
     * @param {string} address The address that the label points at
     */
    update(label, section, bind, address) {
        this._labels[label] = {
            section: section,
            address: address,
            bind, bind
        };

        // Truncate the address to something friendlier
        if (address.substring(0, 8) === "00000000") {
            address = address.substring(8);
        }

        this._element.classList.remove("empty");
        var row = document.createElement("tr");

        function createCell(type, value) {
            var cell = document.createElement("td");
            cell.classList.add(type);
            cell.textContent = value;
            return cell;
        }
        
        row.appendChild(createCell("label", label));
        row.appendChild(createCell("address", address));
        this._element.querySelector("tbody").appendChild(row);
    }

    retrieve(name) {
        return this._labels[name];
    }
}
