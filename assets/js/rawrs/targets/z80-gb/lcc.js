// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Assembler } from '../../processes/assembler';
import { Linker } from '../../processes/linker';

export class LCC {
    constructor(process) {
        this._process = process;
    }

    get suffix() {
        return ".gb";
    }

    get process() {
        return this._process;
    }

    async _loadZip(url) {
        // Fetch the data zip file
        let response = await fetch(url);
        return await response.blob();
    }

    /**
     * Helper routine to perform an 'exec' callout to a subprogram.
     */
    _exec(filename, argv) {
        return new Promise( (resolve, reject) => {
            // The URL of the worker
            let workerSource = "";

            // Whether or not we include the zipped system root
            let includeSystemRoot = true;

            // Whether or not we know the expected output to give back
            let output = null;

            // Whether or not we let the command 'succeed' without output
            // It will then 'inherit' the success of the following command
            let inheriting = false;
            let hasPreprocessed = false;

            // The environment variables
            let env = {
            };

            let basename = (filename.match(/([^/]+)[.].+$/) || [null, filename])[1];

            // Now select argv and specific options by the program we're running
            if (argv[0] == "lcc") {
                this._commands = [];
                workerSource = `${this.process.basepath}js/targets/${this.process.target}/gbdk/lcc.js`;
            }
            else if (argv[0] == "sdasgb") {
                // Run assembler
                workerSource = `${this.process.basepath}js/targets/${this.process.target}/sdcc/sdasgb.js`;
                output = basename + ".o";
                this.process.newStep("Assembling", output);
            }
            else if (argv[0] == "sdldgb") {
                // Run linker
                workerSource = `${this.process.basepath}js/targets/${this.process.target}/sdcc/sdld.js`;
                output = this.process.project + ".ihx";
                this.process.newStep("Linking", output);
            }
            else if (argv[0] == "sdcpp") {
                // Run pre-processor
                workerSource = `${this.process.basepath}js/targets/${this.process.target}/sdcc/sdcpp.js`;
                argv.push("-o");
                argv.push("/work/__preprocess.c");
                output = "__preprocess.c";
                this.process.newStep("Preprocessing", basename + ".i");
            }
            else if (argv[0] == "ihxcheck") {
                // Run checker
                workerSource = `${this.process.basepath}js/targets/${this.process.target}/gbdk/ihxcheck.js`;
                output = this.process.project + ".ihx";
                this.process.newStep("Checking", output);
            }
            else if (argv[0] == "makebin") {
                // Run checker
                workerSource = `${this.process.basepath}js/targets/${this.process.target}/sdcc/makebin.js`;
                output = this.process.project + ".gb";
                this.process.newStep("Creating", output);
            }
            else if (argv[0] == "sdcc") {
                // Run compiler
                for (let item of this._workingFS) {
                    if (item.name === "__preprocess.c") {
                        hasPreprocessed = true;
                    }
                }
                workerSource = `${this.process.basepath}js/targets/${this.process.target}/sdcc/sdcc.js`;
                output = basename + ".asm";
                if (hasPreprocessed) {
                    this.process.newStep("Compiling", output);
                }
            }

            var worker = new Worker(workerSource);

            // Local file-system
            let mounts = [
                {
                    type: "WORKERFS",
                    opts: {
                        blobs: this.process.files
                    },
                    "mountpoint": "/input"
                }
            ];

            if (includeSystemRoot) {
                mounts.push({
                    type: "ZIPFS",
                    opts: {
                        blob: this._systemRoot
                    },
                    "mountpoint": "/usr"
                });
                /*
                mounts.push({
                    type: "ZIPFS",
                    opts: {
                        blob: this._sdccRoot
                    },
                    "mountpoint": "/usr"
                });*/
            }

            worker.onmessage = async (e) => {
                var msg = e.data;

                switch(msg.type) {
                    case "ready":
                        worker.postMessage({
                            type: "run",
                            thisProgram: "/usr/bin/" + argv[0],
                            MEMFS: this._baseFS.concat(this._workingFS),
                            ENV: env,
                            output: output,
                            mounts: mounts,
                            arguments: argv.slice(1)
                        });
                        break;
                    case "stdout":
                        {
                            if (msg.data.indexOf("Invalid start of line token for line") >= 0) {
                                break;
                            }

                            if (argv[0] === "lcc" || argv[0] === "sdcc") {
                                // Look for our special messages that tell us when
                                // to delegate to subcommands.
                                let matches = LCC.DELEGATE_REGEX.exec(msg.data);
                                if (matches) {
                                    let matched = matches[1];
                                    if (matched.endsWith(", ]")) {
                                        matched = matched.substring(0, matched.length - 3) + "]";
                                    }
                                    let argvNext = JSON.parse(matched);
                                    // Remove blanks
                                    argvNext = argvNext.filter( (item) => {
                                        return item !== "";
                                    });
                                    argvNext[0] = argvNext[0].substring(argvNext[0].lastIndexOf('/') + 1);
                                    this._commands.push(argvNext);

                                    if (argv[0] === "sdcc" && argvNext[0] == "sdcpp") {
                                        // We need to re-run ourselves after the preprocess step
                                        inheriting = true;
                                        this._commands.push(argv);
                                    }
                                }
                                else {
                                    this.process.terminal.writeln(msg.data);
                                }
                            }
                            else {
                                this.process.terminal.writeln(msg.data);
                            }
                        }
                        break;
                    case "stderr":
                        {
                            // Remove "/input/" from prececding the message data
                            if (msg.data.startsWith("/input/")) {
                                msg.data = msg.data.substring("/input/".length);
                            }

                            // Ignore the end of line warnings
                            if (msg.data.indexOf("end of file not at end of a line") >= 0) {
                                break;
                            }

                            // Check for error statements
                            let matches = LCC.ERROR_REGEX.exec(msg.data);
                            if (matches) {
                                var error = {
                                    file: matches[1],
                                    row: parseInt(matches[2]) - 1,
                                    column: 0,
                                    type: 'error',
                                    text: matches[3]
                                };
                                this.process.errors.push(error);
                                this.process.trigger('error', error);
                            }

                            // Ignore some strange errors emscripten reports sometimes
                            if (msg.data.indexOf("warning: unsupported syscall:") >= 0) {
                                break;
                            }

                            // And other spurious messages
                            if (msg.data.indexOf(": Assembler messages:") >= 0) {
                                break;
                            }

                            this.process.terminal.writeln(msg.data);
                        }
                        break;
                    case "exit":
                        this._lastCode = msg.data;
                        break;
                    case "done":
                        if (argv[0] !== "lcc" && (argv[0] !== "sdcc" || (argv[0] === "sdcc" && hasPreprocessed))) {
                            if (msg.data.MEMFS[0]) {
                                if (msg.data.MEMFS[0].name.endsWith('.i')) {
                                    msg.data.MEMFS[0].type = 'c_cpp/text';
                                }
                                else if (msg.data.MEMFS[0].name.endsWith('.c')) {
                                    msg.data.MEMFS[0].type = 'c_cpp/text';
                                }
                                else if (msg.data.MEMFS[0].name.endsWith('.asm')) {
                                    msg.data.MEMFS[0].type = 'assembly_z80_gb/text';
                                }
                                else if (msg.data.MEMFS[0].name.endsWith('.o')) {
                                    msg.data.MEMFS[0].type = 'z80-gb/object';
                                }
                                else if (msg.data.MEMFS[0].name.endsWith('.ihx')) {
                                    msg.data.MEMFS[0].type = 'z80-gb/rom-map';
                                }
                                else if (msg.data.MEMFS[0].name.endsWith('.gb')) {
                                    msg.data.MEMFS[0].type = 'z80-gb/rom';
                                }
                                this.process.step.done(msg.data.MEMFS[0]);
                            }
                            else {
                                this.process.step.fail();
                            }
                        }

                        worker.terminate();

                        if (argv[0] === "lcc") {
                            // Set the working file-system
                            this._workingFS = msg.data.MEMFS;

                            // Delegate!
                            
                            let rejected = false;
                            for (let i = 0; i < this._commands.length; i++) {
                                const argv = this._commands[i];
                                try {
                                    this._workingFS = [await this._exec(filename, argv)];
                                    if (!this._workingFS[0]) {
                                        this._workingFS = [];
                                    }
                                }
                                catch (e) {
                                    rejected = true;
                                    reject(e);
                                    break;
                                }
                            }

                            if (!rejected) {
                                if (this._workingFS[0].name.endsWith('.i')) {
                                    this._workingFS[0].type = 'c_cpp/text';
                                }
                                else if (this._workingFS[0].name.endsWith('.c')) {
                                    this._workingFS[0].type = 'c_cpp/text';
                                }
                                else if (this._workingFS[0].name.endsWith('.asm')) {
                                    this._workingFS[0].type = 'assembly_z80_gb/text';
                                }
                                else if (this._workingFS[0].name.endsWith('.o')) {
                                    this._workingFS[0].type = 'z80-gb/object';
                                }
                                else if (this._workingFS[0].name.endsWith('.ihx')) {
                                    this._workingFS[0].type = 'z80-gb/rom-map';
                                }
                                else if (this._workingFS[0].name.endsWith('.gb')) {
                                    this._workingFS[0].type = 'z80-gb/rom';
                                }
                                resolve(this._workingFS[0]);
                            }
                        }
                        else {
                            if (msg.data.MEMFS[0]) {
                                if (msg.data.MEMFS[0].name.endsWith('.i')) {
                                    msg.data.MEMFS[0].type = 'c_cpp/text';
                                }
                                else if (msg.data.MEMFS[0].name.endsWith('.c')) {
                                    msg.data.MEMFS[0].type = 'c_cpp/text';
                                }
                                else if (msg.data.MEMFS[0].name.endsWith('.asm')) {
                                    msg.data.MEMFS[0].type = 'assembly_z80_gb/text';
                                }
                                else if (msg.data.MEMFS[0].name.endsWith('.o')) {
                                    msg.data.MEMFS[0].type = 'z80-gb/object';
                                }
                                else if (msg.data.MEMFS[0].name.endsWith('.ihx')) {
                                    msg.data.MEMFS[0].type = 'z80-gb/rom-map';
                                }
                                else if (msg.data.MEMFS[0].name.endsWith('.gb')) {
                                    msg.data.MEMFS[0].type = 'z80-gb/rom';
                                }
                                resolve(msg.data.MEMFS[0]);
                            }
                            else if (inheriting) {
                                // Pass along the thingy
                                if (this._workingFS[0]) {
                                    if (this._workingFS[0].name.endsWith('.i')) {
                                        this._workingFS[0].type = 'c_cpp/text';
                                    }
                                    else if (this._workingFS[0].name.endsWith('.c')) {
                                        this._workingFS[0].type = 'c_cpp/text';
                                    }
                                    else if (this._workingFS[0].name.endsWith('.asm')) {
                                        this._workingFS[0].type = 'assembly_z80_gb/text';
                                    }
                                    else if (this._workingFS[0].name.endsWith('.o')) {
                                        this._workingFS[0].type = 'z80-gb/object';
                                    }
                                    else if (this._workingFS[0].name.endsWith('.ihx')) {
                                        this._workingFS[0].type = 'z80-gb/rom-map';
                                    }
                                    else if (this._workingFS[0].name.endsWith('.gb')) {
                                        this._workingFS[0].type = 'z80-gb/rom';
                                    }
                                }
                                resolve(this._workingFS[0]);
                            }
                            else {
                                reject();
                            }
                        }
                        break;
                    default:
                        break;
                }
            };
        });
    }

    async _loadZip(url) {
        // Fetch the data zip file
        let response = await fetch(url);
        return await response.blob();
    }

    async _perform() {
        let filename = this.process.working[0].name;

        this._baseFS = this.process.working;

        // Load standard libraries
        if (!this._systemRoot) {
            this._systemRoot = await this._loadZip(`${this.process.basepath}static/${this.process.target}/gbdk.zip`);
            //this._sdccRoot = await this._loadZip(`${this.process.basepath}static/${this.process.target}/sdcc.zip`);
        }

        // Run 'lcc'
        let basename = (filename.match(/([^/]+)[.].+$/) || [null, filename])[1];
        this._workingFS = [];
        let args = [
            "lcc", "--prefix=/usr/gbdk/",
            "/input/" + filename,
            "-c", "-o", "/work/" + basename + ".o",
            "-Wp-I/usr/gbdk/lib/small/asxxxx/gb",
            "-Wp-I/usr/gbdk/lib/small/asxxxx",
            "-Wp-I/input", "-Wf-I/usr/gbdk/lib/small/asxxxx/gb",
            "-Wf-I/usr/gbdk/lib/small/asxxxx",
            "-Wf-I/input",
            "-Wa-I/usr/gbdk/lib/small/asxxxx/gb",
            "-Wa-I/usr/gbdk/lib/small/asxxxx",
            "-Wa-I/input"
        ];

        if (this.process instanceof Linker) {
            let linkerScriptFile = null;
            if (this.process.files[0]) {
                // Linker script file is here instead
                linkerScriptFile = "/input/" + this.process.files[0].name;
            }

            var files = this.process.working;
            args = files.map( (info) => info.name ).sort();
            args = ["lcc", "--prefix=/usr/gbdk/", "-o", this.process.project + ".gb", "-Wa-I/usr/gbdk/lib/small/asxxxx", "-Wa-l", "-Wl-m", "-Wl-j", "-Wa-I/input"].concat(args);
            if (linkerScriptFile) {
                args = args.concat(["-T", linkerScriptFile]);
            }
        }

        let result = await this._exec(filename, args);
        this.process.done();
        return result;
    }

    /**
     * Performs the compilation routine.
     *
     * @override
     */
    perform(resolve, reject) {
        try {
            (new Promise( async (innerResolve, innerReject) => {
                try {
                    let ret = await this._perform();
                    innerResolve(ret);
                }
                catch (e) {
                    innerReject(e);
                }
            })).then( (binary) => {
                resolve(binary);
            }).catch( (e) => {
                reject(e);
            });
        }
        catch (e) {
            reject(e);
        }
    }
}

LCC.ERROR_REGEX = /^(\S+):(\d+):\s+Error:\s+(.+)$/;
LCC.DELEGATE_REGEX = /^__rawrs__: running program: (.+)$/;
