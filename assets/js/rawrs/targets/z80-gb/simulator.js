// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Simulator } from '../../simulator.js';
import { Util } from '../../util.js';

// Gather panels
import { Video } from '../../panels/video.js';

/**
 * Represents a Visual Boy Advance session.
 */
export class Z80GBSimulator extends Simulator {
    /**
     * Returns information about the capabilities of the simulator.
     */
    static get options() {
        return {
            outputs: [
                {
                    panel: Video,
                    name: 'run.panels.video',
                    width: 640,
                    height: 480
                }
            ]
        };
    }

    /**
     * Returns the video panel.
     */
    get video() {
        return this._video;
    }

    /**
     * Returns the list of current breakpoints.
     */
    get breakpoints() {
        return this._breakpoints;
    }

    /**
     * Returns the value in the PC register.
     *
     * @returns {bigint} The value of the PC register as an integer.
     */
    get pc() {
        return this.registers.general.pc;
    }

    _start() {
        // Turn on the video signal
        this.video.active = true;
    }

    _quit() {
        // Turn off the video signal
        this.video.active = false;

        // Set state to 'done'
        this.state = Simulator.STATE_DONE;
    }

    /**
     * Queries controller information.
     */
    async initializeControllers() {
        let portCount = await this.waitFor('controller_port_count', []);
        this._ports = new Array(portCount);
        for (let portID = 0; portID < portCount; portID++) {
            let typeCount = await this.waitFor('controller_port_type_count', [portID]);
            this._ports[portID] = new Array(typeCount);

            for (let typeID = 0; typeID < typeCount; typeID++) {
                // Gather bind information
                let itemCount = await this.waitFor('controller_port_type_item_count', [portID, typeID]);
                let bind = new Array(itemCount);

                for (let itemID = 0; itemID < itemCount; itemID++) {
                    bind[itemID] = {
                        device: await this.waitFor('controller_port_type_item_device', [portID, typeID, itemID]),
                        id: await this.waitFor('controller_port_type_item_id', [portID, typeID, itemID]),
                        name: await this.waitFor('controller_port_type_item_name', [portID, typeID, itemID]),
                    };
                    bind[itemID].genericDevice = Z80GBSimulator.CONTROLLER_INFO[bind[itemID].device].name;
                    bind[itemID].genericId = Z80GBSimulator.CONTROLLER_INFO[bind[itemID].device].items[bind[itemID].id];
                }

                this._ports[portID][typeID] = {
                    id: await this.waitFor('controller_port_type_id', [portID, typeID]),
                    name: await this.waitFor('controller_port_type_name', [portID, typeID]),
                    bind: bind
                };
            }
        }

        // Attach joypad to port 0
        await this.waitFor('retro_set_controller_port_device', [0, 1]);

        console.log(this._ports);
    }

    /**
     * Creates audio buffers based on the information provided by the core.
     */
    async initializeAudio() {
        // Get audio information
        let avInfo = await this.retroGetSystemAVInfo();
        console.log("Audio: sample rate:", avInfo.timing.sampleRate);

        // Get the sample rate
        let sampleRate = avInfo.timing.sampleRate;

        // Get audio context
        this._audioCtx = new window.AudioContext();
        this._gainNode = this._audioCtx.createGain();
        this._gainNode.connect(this._audioCtx.destination);
        this._gainNode.gain.value = 1.0;

        // Create a small ring buffer of 3 buffers (stereo?) for 250 milliseconds of audio
        this._buffers = new Array(3);
        this._sources = new Array(3);

        for (let i = 0; i < this._buffers.length; i++) {
            this._buffers[i] = this._audioCtx.createBuffer(2, Math.floor(sampleRate * 0.25), sampleRate);

            // Create a sound source
            this._sources[i] = this._audioCtx.createBufferSource();
        }

        // Initialize the ring buffer
        this._bufferReadPointer = 0;
        this._bufferWritePointer = 0;

        // Send first buffer
        await this.attachAudioBuffer();
    }

    async appendAudioBuffer(channels, samples) {
        // Schedule current buffer
        let i = this._bufferWritePointer;
        this._bufferWritePointer = (this._bufferWritePointer + 1) % this._buffers.length;

        // Get source
        let source = this._sources[i];

        // And metadata
        let sampleRate = this._buffers[i].sampleRate;

        // Create a new sound source that can fit the same as last time plus a little
        this._buffers[i] = this._audioCtx.createBuffer(2, samples + 50, sampleRate);
        this._sources[i] = this._audioCtx.createBufferSource();

        // Send next buffer
        this.attachAudioBuffer();

        // Create new buffer
        let buffer = this._audioCtx.createBuffer(2, samples, sampleRate);
        buffer.copyToChannel(new Float32Array(channels[0]), 0, 0);
        buffer.copyToChannel(new Float32Array(channels[1]), 1, 0);

        source.buffer = buffer;

        // Determine the scheduled time for the buffer
        if (!this._audioTimestamp) {
            this._audioTimestamp = this._audioCtx.currentTime;
        }

        // Allow us to 'catch up' if our buffer is delayed perhaps
        this._audioTimestamp = Math.max(this._audioTimestamp, this._audioCtx.currentTime + 0.001);
        this._audioTimestamp = Math.min(this._audioTimestamp, this._audioCtx.currentTime + 0.025);

        // Add the length of the buffer to our timestamp
        let elapsed = samples / sampleRate;
        let end = this._audioTimestamp + elapsed;

        // Schedule the buffer
        source.connect(this._gainNode);
        source.start(this._audioTimestamp);

        // Set our next timestamp
        this._audioTimestamp = end;
    }

    async attachAudioBuffer() {
        // Pass along the next buffer to the worker
        this._bufferReadPointer = (this._bufferReadPointer + 1) % this._buffers.length;
        let i = this._bufferReadPointer;

        // Get left and right channel
        let left = this._buffers[i].getChannelData(0).buffer;
        let right = this._buffers[i].getChannelData(1).buffer;

        // Send buffers to worker to fill
        this.worker.postMessage({
            type: 'audio',
            channels: [left, right]
        }, [left, right]);
    }

    /**
     * Attaches the canvas to the worker in whatever way is supported.
     *
     * We want an offscreen canvas we can then render to the real canvas.
     *
     * We can fall back to an ImageBitmap or ArrayBuffer as needed.
     */
    async attachCanvas() {
        let canvas = this.video.canvas;
        let context = this.video.context;

        // Get dimensions for the canvas
        let avInfo = await this.retroGetSystemAVInfo();

        console.log("setting up canvas at", avInfo.geometry.baseWidth, avInfo.geometry.baseHeight);

        // Detect support

        // ArrayBuffer sharing
        const pixels = new ImageData(avInfo.geometry.baseWidth,
                                     avInfo.geometry.baseHeight);

        let buffer = pixels.data.buffer;
        this.worker.postMessage({
            type: 'canvas',
            buffer: buffer,
            width: avInfo.geometry.width,
            height: avInfo.geometry.height
        }, [buffer]);
    }

    waitFor(name, args) {
        return new Promise( (resolve, reject) => {
            this._responseResolve[name] = (ret, newArgs) => {
                for (let i = 0; i < args.length; i++) {
                    let arg = args[i];
                    if (arg instanceof Object.getPrototypeOf(Int8Array)) {
                        args[i] = new arg.constructor(newArgs[i]);
                    }
                    else {
                        args[i] = newArgs[i];
                    }
                }
                resolve(ret);
            };
            this._responseReject[name] = reject;

            const buffers = [];
            let postedArgs = [];
            for (let i = 0; i < args.length; i++) {
                let arg = args[i];
                if (arg instanceof Object.getPrototypeOf(Int8Array)) {
                    arg = arg.buffer;
                    buffers.push(arg);
                }
                postedArgs.push(arg);
            }

            this.worker.postMessage({
                type: 'api',
                name: name,
                args: postedArgs,
            }, buffers);
        });
    }

    /**
     * Returns the API version for the libretro component.
     */
    async retroApiVersion() {
        return await this.waitFor('retro_api_version', []);
    }

    /**
     * Retrieves the system AV info structure.
     */
    async retroGetSystemAVInfo() {
        let args = [{
          'geometry': {
            'baseWidth': 'i32',
            'baseHeight': 'i32',
            'maxWidth': 'i32',
            'maxHeight': 'i32',
            'aspectRatio': 'float',
            'padding': 'i32' // alignment padding
          },
          'timing': {
            'fps': 'double',
            'sampleRate': 'double'
          }
        }];

        await this.waitFor('retro_get_system_av_info', args);
        return args[0];
    }

    async press(genericDevice, genericId) {
        // Find the port/type for this device/id
        for (let portID = 0; portID < this._ports.length; portID++) {
            let types = this._ports[portID];
            for (let typeID = 0; typeID < types.length; typeID++) {
                let items = types[typeID].bind;
                for (let itemID = 0; itemID < items.length; itemID++) {
                    let item = items[itemID];

                    if (item.genericDevice === genericDevice &&
                        item.genericId === genericId) {
                        await this.waitFor('controller_port_type_item_set_state', [portID, typeID, itemID, 1]);
                    }
                }
            }
        }
    }

    async release(genericDevice, genericId) {
        // Find the port/type for this device/id
        for (let portID = 0; portID < this._ports.length; portID++) {
            let types = this._ports[portID];
            for (let typeID = 0; typeID < types.length; typeID++) {
                let items = types[typeID].bind;
                for (let itemID = 0; itemID < items.length; itemID++) {
                    let item = items[itemID];

                    if (item.genericDevice === genericDevice &&
                        item.genericId === genericId) {
                        await this.waitFor('controller_port_type_item_set_state', [portID, typeID, itemID, 0]);
                    }
                }
            }
        }
    }

    async updateRegisters() {
        // Gather the register state from the simulator
        let regs = new Uint16Array(6);
        let args = [regs];

        await this.waitFor('get_registers', args);

        // Pull the updated buffer out
        regs = args[0];
        console.log("register dump", regs);
        this.registers.general["af"].value = regs[0];
        this.registers.general["bc"].value = regs[1];
        this.registers.general["de"].value = regs[2];
        this.registers.general["hl"].value = regs[3];
        this.registers.general["sp"].value = regs[4];
        this.registers.general["pc"].value = regs[5];
    }

    writeRegisters() {
        // Submit all the register state to the simulator
    }

    async handleInput(event) {
        if (event.code == "Digit0" && event.type == "keydown") {
            if (this.state === Simulator.STATE_RUNNING) {
                console.log("pause!");
                await this.pause();
                await this.updateRegisters();
            }
            else if (this.state === Simulator.STATE_PAUSED) {
                console.log("resume!");
                this.resume();
            }
            return;
        }

        if (Z80GBSimulator.DEFAULT_INPUT_MAPPING[event.code]) {
            let bindings = Z80GBSimulator.DEFAULT_INPUT_MAPPING[event.code];

            for (let i = 0; i < bindings.length; i++) {
                let device = bindings[i][0];
                let id = bindings[i][1];

                if (event.type == "keydown") {
                    if (!event.repeat) {
                        this.press(device, id);
                    }
                }
                else {
                    this.release(device, id);
                }
            }
        }
    }

    /**
     * Initializes the simulation to prepare to run.
     */
    async load(process) {
        this._process = process;

        // Do not call twice.
        if (this._initialized) {
            return;
        }
        this._initialized = true;

        this._responseResolve = {};
        this._responseReject = {};

        // Retain output panels
        this._video = this.settings.outputs[0];

        // Attach input events
        this.video.on('keydown', (event) => {
            this.handleInput(event);
        });

        this.video.on('keyup', (event) => {
            this.handleInput(event);
        });

        // Set initial values

        // The simulator has never been loaded
        this._loaded = false;

        // When the simulator has properly loaded and run until the start of
        // the application, this will be 'true'
        this._ready = false;

        // Look for external register changes (from UI / debugger)
        this.registers.on('change', (info) => {
            // Send register data to simulator
            console.log("register change");
            this.writeRegisters();
        });

        // Load the simulator into the environment
        // Afterward, we will have a `window.VisualBoyAdvance`
        await Util.loadScript(this.basepath + Z80GBSimulator.VM_URL);

        let files = [];

        // Load the default kernel if none was provided instead
        if (!this.bootBinary) {
            // We don't use one
        }

        // Load the run binary, if it was provided
        //this.runBinary = null;
        if (!this.runBinary) {
            throw new Error("run binary is required");
        }

        files.push(this.runBinary);

        // The initial Module namespace
        this._module = this._initialModule = {};

        let args = [];
        args.push("--no-opengl");
        args.push(files[0].name);
        this._module.arguments = args;

        // Pass the canvas along to the module
        this._module.canvas = this.video.canvas;
        this._module.canvas.setAttribute('id', 'visualboy-canvas');

        // Ensure that the WASM loader can find the WASM file
        this._module.locateFile = (url) => {
            return this.basepath + 'js/targets/z80-gb/visualboyadvance/' + url;
        };

        // Handles any signals from the simulation
        this._module.signal = (message) => {
            console.log("signal!", message);
        };

        // Action to happen on quit.
        this._module.quit = () => {
            //console.log("hello");
            //this._quit();
        };
        this._module.exit = (status, ex) => {
            //this._quit();
        };

        // Callback when the simulator hits a breakpoint
        this._module.onBreakpoint = () => {
            this._breakpoint();
        };

        // The callback that fires before TinyEmu has even initialized
        this._module.preRun = (blah) => {
            // Upload the files to Visual Boy's local file-system
            let mounts = [];

            if (this.runBinary) {
                mounts.push({
                    type: "MEMFS",
                    file: this.runBinary
                });
            }

            if (this._dpmi) {
                mounts.push({
                    type: "MEMFS",
                    file: this._dpmi
                });
            }

            if (files) {
                for (let file of files) {
                    mounts.push({
                        type: "MEMFS",
                        file: file
                    });
                }
            }

            this._initializeFS(mounts);

            // Start simulation
            this._start();
        };

        // The callback when the runtime for Visual Boy Advance has initialized
        this._module.onRuntimeInitialized = () => {
            if (this._started) {
                return;
            }

            //this.reset();
        };

        // Ignore the warnings/errors from emscripten's boilerplate
        //this._module.printErr = (msg) => {
        //};

        // Load the web worker for the assembler
        return new Promise( async (resolve, reject) => {
            this.worker = new Worker(`${this.basepath}${Z80GBSimulator.VM_URL}`);

            // Create the worker and pass along messages
            this.worker.onmessage = async (e) => {
                var msg = e.data;

                switch(msg.type) {
                    case "ready":
                        this.worker.postMessage({
                            type: "run",
                            MEMFS: files
                        });
                        break;

                    case "running":
                        break;

                    case "stdout":
                        this.console.writeln(this._process, msg.data);
                        break;

                    case "stderr":
                        this.console.writeln(this._process, msg.data);
                        break;

                    case "exit":
                        break;

                    case "frame":
                        // Get image data and render it to our canvas
                        let imageData = new ImageData(
                            new Uint8ClampedArray(msg.buffer),
                            msg.width,
                            msg.height
                        );
                        this.video.active = true;
                        this.video.blit(imageData, 0, 0, 0, 0, msg.width, msg.height, msg.width, msg.height);

                        // Reattach
                        msg.type = 'canvas';
                        this.worker.postMessage(msg, [msg.buffer]);
                        break;

                    case "audio":
                        // Get audio frame and truncate and play it
                        this.appendAudioBuffer(msg.channels, msg.samples);
                        break;

                    case "api":
                        if (this._responseResolve[msg.name]) {
                            this._responseResolve[msg.name](msg.result, msg.args);
                        }
                        break;

                    case "done":
                        this._loaded = true;
                        await this.reset();
                        await this.waitFor('js_init', []);
                        await this.waitFor('load_game', [files[0].name]);
                        await this.attachCanvas();
                        await this.initializeControllers();
                        await this.initializeAudio();

                        console.log("libretro api version:", await this.retroApiVersion());
                        resolve();
                        break;

                    default:
                        break;
                }
            };
        });
    }

    async resume() {
        if (this.state !== Simulator.STATE_PAUSED) {
            return;
        }

        // Set state
        this.state = Simulator.STATE_RUNNING;

        // Run
        this.runLoop();
    }

    /**
     * Pauses simulator.
     */
    async pause() {
        if (this.state !== Simulator.STATE_RUNNING) {
            return;
        }

        // Set state
        this.state = Simulator.STATE_PAUSED;
    }

    /**
     * Internal function that represents the run loop.
     */
    runLoop(elapsed) {
        this.waitFor('retro_run', []).then( () => {
            if (this.state === Simulator.STATE_RUNNING) {
                window.requestAnimationFrame(this.runLoop.bind(this));
            }
        });
    }

    /**
     * Start or resume the simulation.
     */
    async run() {
        console.log("starting run", this.state);
        if (this.state === Simulator.STATE_DONE) {
            await this.reset();
        }

        console.log("starting run b", this.state);
        if (this.state === Simulator.STATE_READY) {
            this._start();
            this._module = {...this._initialModule};
            this.state = Simulator.STATE_RUNNING;

            // Run
            this.runLoop();
        }
        else {
            await this.resume();
        }
    }

    /**
     * Halts the simulation.
     */
    async stop() {
        if (this.state === Simulator.STATE_UNINITIALIZED) {
            return;
        }

        // TODO: stop visual boy
        console.log("STOPPED");
    }

    /**
     * Resets the simulation.
     *
     * Returns on error or when the simulator state is `STATE_READY`.
     */
    async reset() {
        console.log("stopping");
        await this.stop();

        // Initialize Visual Boy's Module namespace
        console.log("going to ready");
        this.state = Simulator.STATE_READY;
    }
}

/**
 * The URL of the simulator's JavaScript compiled code to use.
 */
Z80GBSimulator.VM_URL = "js/targets/z80-gb/visualboyadvance/vbam_libretro-worker.js";

Z80GBSimulator.CONTROLLER_INFO = [
    { name: "None", items: []},
    { name: "Joypad", items: ["B", "Y", "SELECT", "START", "UP", "DOWN", "LEFT", "RIGHT", "A", "X", "L", "R", "L2", "R2", "L3", "R3"], mask: 256},
    { name: "Mouse", items: ["X", "Y", "LEFT", "RIGHT", "WHEELUP", "WHEELDOWN", "MIDDLE", "HORIZ_WHEELUP", "HORIZ_WHEELDOWN", "BUTTON_4", "BUTTON_5"]},
    { name: "Keyboard", items: []},
    { name: "Lightgun", items: ["X", "Y", "TRIGGER", "AUX_A", "AUX_B", "PAUSE", "START", "SELECT", "AUX_C", "DPAD_UP", "DPAD_DOWN", "DPAD_LEFT", "DPAD_RIGHT", "SCREEN_X", "SCREEN_Y", "IS_OFFSCREEN", "RELOAD"]},
    { name: "Analog", items: []},
    { name: "Pointer", items: ["X", "Y", "PRESSED", "COUNT"]}
];

Z80GBSimulator.DEFAULT_INPUT_MAPPING = {
    "KeyX": [ ["Joypad", "A"] ],
    "KeyZ": [ ["Joypad", "B"] ],
    "KeyS": [ ["Joypad", "X"] ],
    "KeyA": [ ["Joypad", "Y"] ],
    "Enter": [ ["Joypad", "START" ] ],
    "Backspace": [ ["Joypad", "SELECT" ] ],
    "ArrowUp": [ ["Joypad", "UP" ] ],
    "ArrowDown": [ ["Joypad", "DOWN" ] ],
    "ArrowLeft": [ ["Joypad", "LEFT" ] ],
    "ArrowRight": [ ["Joypad", "RIGHT" ] ],
};
