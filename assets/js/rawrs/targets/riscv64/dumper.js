// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Dumper } from '../../processes/dumper';

export class RISCV64Dumper extends Dumper {
    /**
     * Retrieves the name of this target.
     */
    get target() {
        return "riscv64";
    }

    perform(resolve, reject, options) {
        let mode = options.mode;
        let section = options.section;
        let binary = this.working[0];
        var worker = new Worker(this.basepath + "js/targets/riscv64/binutils/riscv64-unknown-elf-readelf.js");

        var files = [binary];

        var lastRow = null;

        worker.onmessage = (e) => {
            var msg = e.data;

            switch(msg.type) {
                case "ready":
                    worker.postMessage({
                        type: "run",
                        MEMFS: files,
                        arguments: [mode + section, "-W", "-T", binary.name]
                    });
                    break;
                case "stdout":
                    switch(mode) {
                        // Returns memory in .data section
                        case "-x":
                            var matches = RISCV64Dumper.HEX_LINE_REGEX.exec(msg.data);
                            if (matches) {
                                // Convert the hexdump to a byte array
                                // Each segment in the hexdump is a 4 byte little-endian word
                                let dataString = matches.slice(2).join('');
                                let data = new Uint8Array(dataString.match(/.{1,2}/g).map( (byte) => parseInt(byte, 16)));

                                if (lastRow) {
                                    lastRow.data.set(data, lastRow.length)
                                    lastRow.length = lastRow.data.byteLength;
                                    this.trigger('update', lastRow);
                                    lastRow = null;
                                }
                                else {
                                    let row = new Uint8Array(data.length * 2);
                                    row.set(data, 0);
                                    lastRow = {
                                        address: matches[1],
                                        length:  data.byteLength,
                                        data:    row
                                    };
                                }
                            }
                            break;
                        // Returns labels
                        case "-s":
                            // If this line can be parsed as a symbol table
                            // entry, then parse it as such
                            var parts = RISCV64Dumper.LABEL_REGEX.exec(msg.data);
                            if (parts) {
                                // Determine the fields we care about
                                let address = parts[1];
                                let bind = parts[2].toLowerCase();
                                let section = ["unknown", "text", "data"][parseInt(parts[3])] || "unknown";
                                let label = parts[4];

                                // Pack the information
                                let symbol = {
                                    section: section,
                                    bind: bind,
                                    address: address,
                                    label: label
                                };

                                // Pass it up the chain
                                this.trigger('update', symbol);
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case "stderr":
                    // Ignore some strange errors emscripten reports sometimes
                    if (msg.data.indexOf("warning: unsupported syscall:") >= 0) {
                        break;
                    }

                    break;
                case "exit":
                    break;
                case "done":
                    if (lastRow) {
                        this.trigger('update', lastRow);
                    }
                    this.trigger('done');
                    resolve();
                    worker.terminate();
                    break;
                default:
                    break;
            }
        };
    }
}

RISCV64Dumper.HEX_LINE_REGEX = /^\s+(?:0x)?([0-9a-f]+)\s+([0-9a-f]+)\s(?:([0-9a-f]+)\s)?(?:([0-9a-f]+)\s)?(?:([0-9a-f]+)\s)?\s*.+$/;
RISCV64Dumper.LABEL_REGEX = /^\s+\d+:\s+([0-9a-f]+)\s+\d+\s+\S+\s+(\S+)\s+\S+\s+(\d+)\s+(\w+)/;
