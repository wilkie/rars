// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Compiler } from '../../processes/compiler';
import { GCC } from './gcc.js';

export class RISCV64CCompiler extends Compiler {
    /**
     * Retrieves the host triple for this target.
     */
    get targetTriple() {
        return "riscv64-unknown-linux-gnu";
    }

    static get options() {
        return {
            // cache the .o files
            cache: (name) => {
                return name.substring(0, name.length - 2) + ".o"
            },

            // give us ALL .h and .c files
            upload: /\.h|\.c/
        };
    }

    /**
     * Performs the compilation routine.
     *
     * @override
     */
    perform(resolve, reject) {
        if (!this._gcc) {
            this._gcc = new GCC(this);
        }

        try {
            (new Promise( async (innerResolve, innerReject) => {
                let ret = await this._gcc._perform();
                innerResolve(ret);
            })).then( (binary) => {
                resolve(binary);
            });
        }
        catch (e) {
            reject(e);
        }
    }
}
