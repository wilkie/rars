// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Disassembler } from '../../processes/disassembler.js';

export class X86MSDOSDisassembler extends Disassembler {
    /**
     * Retrieves the host triple for this target.
     */
    get targetTriple() {
        return "i586-pc-msdosdjgpp";
    }

    get options() {
        return {
            // This disassembler lists code from .s files
            listing: /\.s$/
        };
    }

    perform(resolve, reject) {
        let binary = this.working[0];
        var worker = new Worker(`${this.basepath}js/targets/${this.target}/binutils/${this.targetTriple}-objdump.js`);

        // This will hold information about what source line the next parsed
        // instruction belongs to.
        this._currentSource = {
            row: 0,
            file: '.none'
        };

        var files = [binary];

        worker.onmessage = (e) => {
            var msg = e.data;

            switch(msg.type) {
                case "ready":
                    worker.postMessage({
                        type: "run",
                        MEMFS: files,
                        arguments: [binary.name, "-d", "-l"]
                    });
                    break;
                case "stdout":
                    var matches = X86MSDOSDisassembler.SOURCE_LINE_REGEX.exec(msg.data);
                    if (matches) {
                        this._currentSource = {
                            row: parseInt(matches[2]),
                            file: matches[1]
                        };
                    }

                    matches = X86MSDOSDisassembler.INSTRUCTION_REGEX.exec(msg.data);
                    if (matches) {
                        this.trigger('instruction', {
                            address: matches[1],
                            machineCode: matches[2],
                            code: matches[3],
                            file: this._currentSource.file,
                            row: this._currentSource.row
                        });
                    }
                    break;
                case "stderr":
                    // Ignore some strange errors emscripten reports sometimes
                    if (msg.data.indexOf("warning: unsupported syscall:") >= 0) {
                        break;
                    }

                    this.terminal.writeln(msg.data);
                    break;
                case "exit":
                    break;
                case "done":
                    this.trigger('done');
                    resolve();
                    worker.terminate();
                    break;
                default:
                    break;
            }
        };
    }
}

/**
 * The regular expression that, when matched, denotes that the disassembly has
 * found a hint to the original line.
 */
X86MSDOSDisassembler.SOURCE_LINE_REGEX = /^(\/[^:]+):(\d+)$/;

/**
 * This regular expression parses a disassembled line.
 */
X86MSDOSDisassembler.INSTRUCTION_REGEX = /^\s+([0-9a-f]+):\s+([0-9a-f]+)\s+(.+)$/;
