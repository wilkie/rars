// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Simulator } from '../../simulator.js';
import { Util } from '../../util.js';

// Gather panels
import { Video } from '../../panels/video.js';

/**
 * Represents a DOSBox session.
 */
export class X86MSDOSSimulator extends Simulator {
    /**
     * Returns information about the capabilities of the simulator.
     */
    static get options() {
        return {
            // Grant us 32MiB of memory by default
            memory: {
                size: 32 * 1024 * 1024,
                unit: Simulator.B
            },

            outputs: [
                {
                    panel: Video,
                    name: 'run.panels.video',
                    width: 640,
                    height: 480
                }
            ],

            // give us the root directory files
            upload: /^root\/.+$/
        };
    }

    /**
     * Returns the video panel.
     */
    get video() {
        return this._video;
    }

    /**
     * Returns the list of current breakpoints.
     */
    get breakpoints() {
        return this._breakpoints;
    }

    /**
     * Returns the value in the PC register.
     *
     * @returns {bigint} The value of the PC register as an integer.
     */
    get pc() {
        return this.registers.general.eip;
    }

    /**
     * Called internally to set up the filesystem.
     */
    _initializeFS(mounts) {
        var FS = this._module.FS;

        // Borrowed a lot from Kagami, here. They are a veritable superhero.
        mounts.forEach( (mount) => {
            if (mount.type === "MEMFS") {
                return;
            }

            var fs = FS.filesystems[mount.type];
            if (!fs) {
                throw new Error("Bad mount type");
            }

            var mountpoint = mount.mountpoint;
            // NOTE(Kagami): Subdirs are not allowed in the paths to simplify
            // things and avoid ".." escapes.
            if (!mountpoint.match(/^\/[^\/]+$/) ||
                  mountpoint === "/." ||
                  mountpoint === "/.." ||
                  mountpoint === "/tmp" ||
                  mountpoint === "/home" ||
                  mountpoint === "/dev" ||
                  mountpoint === "/work") {
                throw new Error("Bad mount point");
            }

            FS.mkdir(mountpoint);
            FS.mount(fs, mount.opts, mountpoint);
        });

        FS.mkdir("/work");
        FS.chdir("/work");

        mounts.forEach( (mount) => {
            if (mount.type === "MEMFS") {
                // Create each subdirectory, if possible
                var subdirs = ("/work/" + mount.file.name).substring(1).split('/');
                var path = "";

                for (let i = 0; i < subdirs.length - 1; i++) {
                    path = path + "/" + subdirs[i];

                    if (!FS.analyzePath(path, true).exists) {
                        FS.mkdir(path);
                    }
                }

                var fd = FS.open(mount.file.name, "w+");
                var data = Util.toU8(mount.file.data);
                FS.write(fd, data, 0, data.length);
                FS.close(fd);
            }
        });
    }

    _start() {
        // Turn on the video signal
        this.video.active = true;
    }

    _quit() {
        // Set state to 'done'
        this.state = Simulator.STATE_DONE;

        if (this._waitPromise) {
            this._waitResolve();
        }
    }

    /**
     * Initializes the simulation to prepare to run.
     */
    async load() {
        // Do not call twice.
        if (this._initialized) {
            return;
        }
        this._initialized = true;

        // Retain output panels
        this._video = this.settings.outputs[0];

        // Set initial values

        // The simulator has never been loaded
        this._loaded = false;

        // When the simulator has properly loaded and run until the start of
        // the application, this will be 'true'
        this._ready = false;

        // Look for external register changes (from UI / debugger)
        this.registers.on('change', (info) => {
            // Send register data to simulator
        });

        // Load the simulator into the environment
        // Afterward, we will have a `window.DOSBox`
        await Util.loadScript(this.basepath + X86MSDOSSimulator.VM_URL);

        // Load the default kernel if none was provided instead
        if (!this.bootBinary) {
            // We don't use one
        }

        // Load the run binary, if it was provided
        if (!this.runBinary) {
            //throw new Error("run binary is required");
        }

        // Load the DPMI server
        if (!this._dpmi) {
            let response = await fetch(`${this.basepath}static/${this.target}/CWSDPMI.EXE`, { credentials: 'include' });
            let data = await response.arrayBuffer();

            this._dpmi = {
                name: "CWSDPMI.EXE",
                data: data
            };
        }

        // The initial Module namespace
        this._module = this._initialModule = {};

        let args = [];
        args = args.concat(['-c', 'MOUNT C /work']);
        args = args.concat(['-c', 'C:']);
        args = args.concat(['-c', this.runBinary.name]);
        this._module.arguments = args;

        // Pass the canvas along to the module
        this._module.canvas = this.video.canvas;
        this._module.canvas.setAttribute('id', 'dosbox-canvas');

        // Ensure that the WASM loader can find the WASM file
        this._module.locateFile = (url) => {
            return `${this.basepath}js/targets/${this.target}/dosbox/${url}`;
        };

        // Handles any signals from the simulation
        this._module.signal = (message) => {
            console.log("signal!", message);
        };

        // Action to happen on quit.
        this._module.quit = () => {
            this._quit();
        };

        this._module.exit = (status, ex) => {
            //this._quit();
        };

        // Callback when the simulator hits a breakpoint
        this._module.onBreakpoint = () => {
            this._breakpoint();
        };

        let files = [];
        for (let file of this.files) {
            let mountedFile = {};
            mountedFile.data = await file.data.arrayBuffer();
            mountedFile.name = file.name;
            if (file.name.startsWith("root/")) {
                mountedFile.name = file.name.substring(5);
            }
            files.push(mountedFile);
        }

        // The callback that fires before TinyEmu has even initialized
        this._module.preRun = (blah) => {
            // Upload the files to DOSBox's local file-system
            let mounts = [];

            if (this.runBinary) {
                mounts.push({
                    type: "MEMFS",
                    file: this.runBinary
                });
            }

            if (this._dpmi) {
                mounts.push({
                    type: "MEMFS",
                    file: this._dpmi
                });
            }

            if (files) {
                for (let file of files) {
                    mounts.push({
                        type: "MEMFS",
                        file: file
                    });
                }
            }

            this._initializeFS(mounts);

            // Start simulation
            this._start();
        };

        // The callback when the runtime for DOSBox has initialized
        this._module.onRuntimeInitialized = () => {
            if (this._started) {
                return;
            }

            //this.reset();
        };

        // Ignore the warnings/errors from emscripten's boilerplate
        this._module.printErr = (msg) => {
        };

        this._loaded = true;
        await this.reset();
    }

    async resume() {
        if (this.state !== Simulator.STATE_PAUSED) {
            return;
        }

        // Send ALT+PAUSE to DOSBox
        let e = new KeyboardEvent("keydown", {
            type: "keydown",
            key: "Pause",
            code: "Pause",
            keyCode: 19,
            which: 19,
            altKey: true,
            composed: true,
            bubbles: true,
            charCode: 0,
            location: 0,
            returnValue: true,
            srcElement: this._module.canvas,
            originalTarget: this._module.canvas,
            explicitOriginalTarget: this._module.canvas,
            target: this._module.canvas,
            view: window
        });
        this._module.canvas.dispatchEvent(e);

        // Set state
        this.state = Simulator.STATE_RUNNING;
    }

    /**
     * Pauses simulator.
     */
    async pause() {
        if (this.state !== Simulator.STATE_RUNNING) {
            return;
        }

        // Send ALT+PAUSE to DOSBox
        let e = new KeyboardEvent("keydown", {
            type: "keydown",
            key: "Pause",
            code: "Pause",
            keyCode: 19,
            which: 19,
            altKey: true,
            composed: true,
            bubbles: true,
            charCode: 0,
            location: 0,
            returnValue: true,
            srcElement: this._module.canvas,
            originalTarget: this._module.canvas,
            explicitOriginalTarget: this._module.canvas,
            target: this._module.canvas,
            view: window
        });
        this._module.canvas.dispatchEvent(e);

        // Set state
        this.state = Simulator.STATE_PAUSED;
    }

    /**
     * Start or resume the simulation.
     */
    async run() {
        if (this.state === Simulator.STATE_DONE) {
            await this.reset();
        }

        // Remove the static from the video panel
        this.video.active = true;

        console.log("starting run b", this.state);
        if (this.state === Simulator.STATE_READY) {
            this._start();
            this._module = {...this._initialModule};
            console.log("attempting to start dosbox", this._module);
            this._module = await window.DOSBox(this._module);
            console.log("started", this._module);
            this.state = Simulator.STATE_RUNNING;
        }
        else {
            await this.resume();
        }
    }

    /**
     * Halts the simulation.
     */
    async stop() {
        if (this.state === Simulator.STATE_UNINITIALIZED) {
            return;
        }

        this._waitResolve = null;
        this._waitPromise = new Promise( (resolve, reject) => { this._waitResolve = resolve; } );
        let timer = window.setInterval( () => {
            if (this._waitResolve) {
                window.clearInterval(timer);
            }
        });

        // Send CTRL+F9 to DOSBox
        let e = new KeyboardEvent("keydown", {
            type: "keydown",
            key: "Control",
            code: "Control",
            keyCode: 17,
            which: 17,
            ctrlKey: true,
            composed: true,
            bubbles: true,
            charCode: 0,
            location: 1,
            returnValue: true,
            srcElement: this._module.canvas,
            originalTarget: this._module.canvas,
            explicitOriginalTarget: this._module.canvas,
            target: this._module.canvas,
            view: window
        });
        this._module.canvas.dispatchEvent(e);

        e = new KeyboardEvent("keydown", {
            type: "keydown",
            key: "F9",
            code: "F9",
            keyCode: 120,
            which: 120,
            ctrlKey: true,
            composed: true,
            bubbles: true,
            charCode: 0,
            location: 1,
            returnValue: true,
            srcElement: this._module.canvas,
            originalTarget: this._module.canvas,
            explicitOriginalTarget: this._module.canvas,
            target: this._module.canvas,
            view: window
        });
        this._module.canvas.dispatchEvent(e);

        e = new KeyboardEvent("keyup", {
            type: "keyup",
            key: "F9",
            code: "F9",
            keyCode: 120,
            which: 120,
            ctrlKey: true,
            composed: true,
            bubbles: true,
            charCode: 0,
            location: 1,
            returnValue: true,
            srcElement: this._module.canvas,
            originalTarget: this._module.canvas,
            explicitOriginalTarget: this._module.canvas,
            target: this._module.canvas,
            view: window
        });
        this._module.canvas.dispatchEvent(e);
        await this._waitPromise;
    }

    /**
     * Resets the simulation.
     *
     * Returns on error or when the simulator state is `STATE_READY`.
     */
    async reset() {
        await this.stop();

        // Initialize DOSBox's Module namespace
        this.state = Simulator.STATE_READY;
    }
}

/**
 * The URL of the simulator's JavaScript compiled code to use.
 */
X86MSDOSSimulator.VM_URL = "js/targets/x86-msdos/dosbox/dosbox.js";
