// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Target } from '../target.js';

// Game Boy Assembler (sdasgb)
import { Z80GBAssembler } from './z80-gb/assembler.js';

// Game Boy Assembler (sdld)
import { Z80GBLinker } from './z80-gb/linker.js';

// Game Boy Assembler (sdcc)
import { Z80GBCCompiler } from './z80-gb/c_compiler.js';

// Visual Boy Advance Simulator
import { Z80GBSimulator } from './z80-gb/simulator.js';

// Needed for register types
import { Register } from '../register.js';

export class Z80GBTarget extends Target {
    constructor() {
        super();

        // Register .s files with the assembler
        this.registerHandler(/(?:\.s|\.S)$/, Z80GBAssembler);

        // Register .c files with the C compiler
        this.registerHandler(/\.c$/, Z80GBCCompiler);

        // Register .cc/.cpp files with the C++ compiler
        //this.registerHandler(/(?:\.cc|\.cpp)$/, MSDOSCPPCompiler);

        // Register .o files with the linker
        this.registerHandler(/\.o$/, Z80GBLinker);

        // Register .gb files with a rom reader, when we can.
        this.registerType(/\.gb$/, 'z80-gb/rom');

        // TODO: sprite editors?

        // Register .s files with a TextEditor.
        this.registerType(/\.s$/, 'assembly_z80_gb/text');

        // Register .c[c|pp]/.h files with a TextEditor.
        this.registerType(/\.c(c|pp)?$/, 'c_cpp/text');
        this.registerType(/\.h(h|pp)?$/, 'c_cpp/text');
    }

    /**
     * Retrieves the name of this target.
     */
    get name() {
        return "z80-gb";
    }

    get simulator() {
        return Z80GBSimulator;
    }

    /**
     * Retrieves the human-legible name of this target.
     */
    get title() {
        return "Game Boy";
    }

    get registers() {
        return {
            general: {
                af: Register.INT16,
                bc: Register.INT16,
                de: Register.INT16,
                hl: Register.INT16,
                sp: Register.INT16,
                pc: Register.INT16,
            }
        };
    }
}

if (window.RAWRSRegisterTarget) {
    window.RAWRSRegisterTarget(Z80GBTarget);
}
