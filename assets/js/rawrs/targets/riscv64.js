// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Target } from '../target.js';

// Gather processes
import { RISCV64Assembler } from './riscv64/assembler.js';
import { RISCV64Linker } from './riscv64/linker.js';
import { RISCV64CCompiler } from './riscv64/c_compiler.js';
import { RISCV64Disassembler } from './riscv64/disassembler.js';
import { RISCV64MemoryDumper } from './riscv64/memory_dumper.js';
import { RISCV64LabelDumper } from './riscv64/label_dumper.js';

// Our simulator
import { RISCV64Simulator } from './riscv64/simulator.js';

// Out debugger
import { RISCV64Debugger } from './riscv64/debugger.js';

// Needed for register types
import { Register } from '../register.js';

/**
 * Provides a RISC-V 64-bit target.
 */
export class RISCV64Target extends Target {
    constructor() {
        super();

        // Register .s files with the assembler
        this.registerHandler(/\.s$/, RISCV64Assembler);

        // Register .c files with the C compiler
        this.registerHandler(/\.c$/, RISCV64CCompiler);

        // Register .o files with the linker
        this.registerHandler(/\.o$/, RISCV64Linker);

        // Register .o files with the elf reader.
        this.registerType(/\.o$/, 'riscv64/elf64');

        // Register .s files with a TextEditor and assign overrides.
        this.registerType(/\.s$/, 'assembly_riscv/text', {
            /**
             * Reads all the labels in the file, currently.
             */
            onLoad: (text, filename) => {
                // We want to re-populate with the known labels/globals in the given text
                this._labelList = RISCV64Target.getLabelsFromText(text);

                // Get the ball rolling on loading the instruction table
                RISCV64Target.getDefinitionFromTable("");
            },

            /**
             * Reads the labels on the new position.
             *
             * So, when we navigate to a line, we want to just assume the label will be
             * editor or removed. So remove all the labels for the line we are *now* on.
             *
             * When we navigate *away* from a line, re-add the labels.
             *
             * This works because we never reasonably reference the label on the line
             * that introduces the label.
             */
            onPosition: (editor, row, column, line, word, kind, last) => {
                // If we switched lines
                if (last.row != row) {
                    // Remove the labels on the current line
                    this.removeLabelsFromString(line);

                    // Insert the labels on this preceded line (if any)
                    this.insertLabelsFromString(last.line);
                }
            },

            /**
             * Handles popovers for assembly terms.
             */
            onMouseover: (editor, line, word, kind, x, y) => {
                // If the word is an instruction (and not within a comment) then
                // popover the help text.
                if (kind.startsWith("keyword")) {
                    RISCV64Target.getDefinitionFromTable(word).then( (docHTML) => {
                        if (docHTML) {
                            editor.showTooltip(docHTML, x, y);
                        }
                        else {
                            editor.hideTooltip();
                        }
                    });
                }
                else {
                    editor.hideTooltip();
                }
            },

            /**
             * Handles populating the autocompletion list.
             */
            onCompletion: (editor, line, prefix, word, kind, row, column) => {
                // Determine if we are typing a jump or branch and autocomplete labels

                // Based on the context, we should either show the instructions
                // listing or the label listing.

                // Get the first word on the line and assume it is an instruction
                let instruction = prefix.split(" ")[0];

                let commaCount = prefix.split(",").length - 1;

                // Get the last character on the line before the current word
                let lastCharacter = prefix[prefix.length - 1];

                // These hold the specific instructions that use labels
                // Each are defined by where in the argument listing the label appears
                let instructionsFirstArgument = ["j", "jal"];
                let instructionsSecondArgument = ["la", "bgez", "beqz", "bltz", "blez", "bgtz", "bnez", "jal"];
                let instructionsThirdArgument = ["beq", "bge", "bgeu", "bgt", "bgtu", "ble", "bleu", "blt", "bltu", "bne"];

                // isLabel will be true when the cursor is typing at a place
                // where a label would be expected
                let isLabel = instructionsFirstArgument.indexOf(instruction) >= 0;
                isLabel = isLabel || (instructionsSecondArgument.indexOf(instruction) >= 0 && lastCharacter == ",");
                isLabel = isLabel || (instructionsThirdArgument.indexOf(instruction) >= 0 && commaCount > 1 && lastCharacter == ",");

                if (isLabel) {
                    // Return a list of relevant labels
                    return (this._labelList || []).map( (label) => {
                        return {
                            name: label,
                            value: label,
                            score: 100,
                            meta: 'label'
                        };
                    });
                }
                else if (!instruction) {
                    // Return a list of relevant instructions
                    return RISCV64Target._instructionsList;
                }
            }
        });

        // Register .c[c|pp]/.h files with a TextEditor.
        this.registerType(/\.c(c|pp)?$/, 'c_cpp/text');
        this.registerType(/\.h(h|pp)?$/, 'c_cpp/text');

        // New project structure
        this.registerProject('bare assembly', [{
            name: 'main.s',
            data: '# main.s\n\n',
            type: 'assembly_riscv/text'
        }]);
    }

    /**
     * Retrieves the name of this target.
     */
    get name() {
        return "riscv64";
    }

    get disassembler() {
        return RISCV64Disassembler;
    }

    get memoryDumper() {
        return RISCV64MemoryDumper;
    }

    get labelDumper() {
        return RISCV64LabelDumper;
    }

    get simulator() {
        return RISCV64Simulator;
    }

    get debugger() {
        return RISCV64Debugger;
    }

    get registers() {
        return {
            general: {
                pc:  Register.INT64,
                ra:  Register.INT64,
                sp:  Register.INT64,
                gp:  Register.INT64,
                tp:  Register.INT64,
                t0:  Register.INT64,
                t1:  Register.INT64,
                t2:  Register.INT64,
                s0:  Register.INT64,
                s1:  Register.INT64,
                a0:  Register.INT64,
                a1:  Register.INT64,
                a2:  Register.INT64,
                a3:  Register.INT64,
                a4:  Register.INT64,
                a5:  Register.INT64,
                a6:  Register.INT64,
                a7:  Register.INT64,
                s2:  Register.INT64,
                s3:  Register.INT64,
                s4:  Register.INT64,
                s5:  Register.INT64,
                s6:  Register.INT64,
                s7:  Register.INT64,
                s8:  Register.INT64,
                s9:  Register.INT64,
                s10: Register.INT64,
                s11: Register.INT64,
                t3:  Register.INT64,
                t4:  Register.INT64,
                t5:  Register.INT64,
                t6:  Register.INT64
            },
            fpu: {
                ft0:  Register.DOUBLEIEEE754,
                ft1:  Register.DOUBLEIEEE754,
                ft2:  Register.DOUBLEIEEE754,
                ft3:  Register.DOUBLEIEEE754,
                ft4:  Register.DOUBLEIEEE754,
                ft5:  Register.DOUBLEIEEE754,
                ft6:  Register.DOUBLEIEEE754,
                ft7:  Register.DOUBLEIEEE754,
                fs0:  Register.DOUBLEIEEE754,
                fs1:  Register.DOUBLEIEEE754,
                fa0:  Register.DOUBLEIEEE754,
                fa1:  Register.DOUBLEIEEE754,
                fa2:  Register.DOUBLEIEEE754,
                fa3:  Register.DOUBLEIEEE754,
                fa4:  Register.DOUBLEIEEE754,
                fa5:  Register.DOUBLEIEEE754,
                fa6:  Register.DOUBLEIEEE754,
                fa7:  Register.DOUBLEIEEE754,
                fs2:  Register.DOUBLEIEEE754,
                fs3:  Register.DOUBLEIEEE754,
                fs4:  Register.DOUBLEIEEE754,
                fs5:  Register.DOUBLEIEEE754,
                fs6:  Register.DOUBLEIEEE754,
                fs7:  Register.DOUBLEIEEE754,
                fs8:  Register.DOUBLEIEEE754,
                fs9:  Register.DOUBLEIEEE754,
                fs10: Register.DOUBLEIEEE754,
                fs11: Register.DOUBLEIEEE754,
                ft8:  Register.DOUBLEIEEE754,
                ft9:  Register.DOUBLEIEEE754,
                ft10: Register.DOUBLEIEEE754,
                ft11: Register.DOUBLEIEEE754
            },
            system: {
                sscratch: Register.INT64,
                sepc:     Register.INT64,
                scause:   Register.INT64,
                stval:    Register.INT64
            }
        };
    }

    /**
     * Gets the definition of an instruction and instruction name.
     */
    static async getDefinitionFromTable(instruction) {
        if (!RISCV64Target._instructionTable) {
            // We want to populate these with the instruction documentation
            RISCV64Target._instructionsList = [];
            RISCV64Target._instructionsLookup = {};

            // Ensure the tab exists and is loaded
            await RISCV64Target.loadInstructionsTab();

            // Get the instruction listing
            RISCV64Target._instructionTable = document.getElementById("instruction-table");

            for (const row of RISCV64Target._instructionTable.rows) {
                let dict = {};
                dict.name = row.cells[0].innerText;
                dict.value = row.cells[0].innerText;
                dict.score = 100;
                dict.meta = 'instruction';

                let instruction = dict.name;

                let summary = RISCV64Target._instructionTable.querySelector('td.summary[data-instruction="' + instruction + '"]');

                if (summary) {
                    let name = RISCV64Target._instructionTable.querySelector('td.name[data-instruction="' + instruction + '"]');

                    if (name) {
                        dict.docHTML = name.innerText + "<br>" + summary.innerText;
                    }
                }
                RISCV64Target._instructionsList.push(dict);
                RISCV64Target._instructionsLookup[dict.name] = dict;
            };
        }

        if (RISCV64Target._instructionsLookup[instruction]) {
            return RISCV64Target._instructionsLookup[instruction].docHTML;
        }

        return null;
    }

    /**
     * Ensures the instructions tab is loaded.
     *
     * This tab contains the documentation for each instruction. This is parsed
     * and used for auto-complete and popover hints on the editor.
     *
     * Acts as a promise that resolves when the tab is loaded.
     */
    static loadInstructionsTab() {
        return new Promise( (resolve, reject) => {
            document.querySelectorAll(".tabs").forEach( (tabStrip) => {
                tabStrip.querySelectorAll(".tab > a, .tab > button").forEach( (tabButton) => {
                    // TODO: move to target plugin somehow
                    var instructionsTabPanel = document.querySelector(".tab-panel#" + tabButton.getAttribute('aria-controls'));
                    if (instructionsTabPanel !== null && ((instructionsTabPanel.getAttribute("data-pjax") || "").indexOf("instructions") > 0)) {
                        // Check if the instructionsTab is PJAX loaded
                        if (!instructionsTabPanel.classList.contains("pjax-loaded")) {
                            var pjaxURL = instructionsTabPanel.getAttribute('data-pjax');
                            if (tabButton.parentNode.querySelector("a.ajax")) {
                                pjaxURL = tabButton.parentNode.querySelector("a.ajax").getAttribute('href');
                            }
                            if (pjaxURL) {
                                // Fetch HTML page and get content at "body.documentation"
                                instructionsTabPanel.classList.add("pjax-loaded");
                                fetch(pjaxURL, {
                                    credentials: 'include'
                                }).then(function(response) {
                                    return response.text();
                                }).then(function(text) {
                                    // Push text to dummy node
                                    let rootpath = document.body.getAttribute('data-rootpath');
                                    let basepath = document.body.getAttribute('data-basepath');
                                    text = text.replaceAll("\"../", "\"" + rootpath + basepath);

                                    var dummy = document.createElement("div");
                                    dummy.setAttribute('hidden', '');
                                    dummy.innerHTML = text;
                                    document.body.appendChild(dummy);
                                    var innerElement = dummy.querySelector(".content.documentation");
                                    instructionsTabPanel.innerHTML = "";
                                    instructionsTabPanel.appendChild(innerElement);
                                    dummy.remove();

                                    resolve();
                                });
                            }
                        }
                    }
                });
            });
        });
    }

    // Returns a list of all labels found in textInput
    static getLabelsFromText(textInput) {
        // Gets an array of all labels in the text 
        // TODO: Check to see whether or not something is in the data section
        // (maybe split the string that getValue() returns by ".data")
        let labelNames = textInput.match(/^\s*([a-zA-Z\d_]+):/gm);

        if (labelNames == null) {  // Prevents an error when labelNames is null
            labelNames = [];
        }

        let labelList = [];  // Contains the list of all labels with the ':' removed

        for (var i = 0; i < labelNames.length; i++) {
            let currentName = labelNames[i].replace(":", "").trim();  // Removes the colon in the string

            labelList.push(currentName);
        }

        return labelList;
    }

    // Takes a string, parses it to create a list of labels, then inserts them
    insertLabelsFromString(textInput) {
        if (!this._labelList) {
            this._labelList = [];
        }

        let newLabels = RISCV64Target.getLabelsFromText(textInput);

        for (let i = 0; i < newLabels.length; i++) {
            // Checks for duplicates, and if it is new, adds it to the global list
            if (!this._labelList.includes(newLabels[i])) {
                this._labelList.push(newLabels[i]);
            }
        }
    }

    removeLabelsFromString(textInput) {
        if (!this._labelList) {
            this._labelList = [];
        }

        let labels = RISCV64Target.getLabelsFromText(textInput);

        for (let i = 0; i < labels.length; i++) {
            if (this._labelList.includes(labels[i])) {
                // Removes the label from the global list
                this._labelList.splice(this._labelList.indexOf(labels[i]), 1);
            }
        }
    }
}

if (window.RAWRSRegisterTarget) {
    window.RAWRSRegisterTarget(RISCV64Target);
}
