// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import bigInt from 'big-integer';
import 'element-qsa-scope';

// Use big-integer to polyfill our use of BigInt
if (!window.BigInt) {
    window.BigInt = bigInt;
}

// Polyfill the simple usecase of BigUint64Array
// We really only use it as a container... hmm.
if (!window.BigUint64Array) {
    window.BigUint64Array = function(values) {
        this.values = values;
    };

    window.BigUint64Array.prototype.forEach = function(callback) {
        return this.values.forEach(callback);
    };
}

// Electron titlebar needs to be last in the DOM hierarchy.
let titlebar = document.querySelector("body > .titlebar");
if (titlebar) {
    document.body.appendChild(titlebar);
}

import { RAWRS } from './rawrs/rawrs';

window.rawrs = new RAWRS();

/* Target Registry */
window.__RAWRSTargets = {};
window.RAWRSRegisterTarget = (target) => {
    let instance = new target();
    window.__RAWRSTargets[instance.name] = instance;
};
window.RAWRSTargetFor = (name) => {
    return window.__RAWRSTargets[name];
};

export default RAWRS;
