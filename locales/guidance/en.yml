en:
  guidance:
    about:
      common:
        licensed: "licensed"
        modifications: "modifications"
        various: "various"

      content: |
        **RAWRS** is a learning tool designed to help teach architecture concepts through the writing of assembly language.
        The language used is RISC-V, an up-and-coming open RISC architecture based on MIPS. RAWRS itself is loosely
        modeled after its own namesake, [MARS](http://courses.missouristate.edu/kenvollmar/mars/), which is an
        equivalent learning solution for MIPS assembly.

        RAWRS is built off of entirely free and open software and is designed to be easy to deploy to any modern
        environment as a static website. It uses *emscripten* to compile the free and open toolchain that RAWRS uses
        to assemble and inspect application binaries all within the web browser's native JavaScript engine.

        The simulator (or emulator if you must) that runs the RISC-V application and kernel is TinyEmu by Fabrice
        Bellard, and is itself permissively licensed and open. Without this, the modifications required to make it
        work in an educational environment would not have been possible. And in the same spirit, RAWRS is offered,
        along with the educational RISC-V kernel written alongside it, completely openly and free.
      related-work:
        header: "Related Work"
        content: |
          This project is not to be confused with RARS by Benjamin Landers (another RISC-V project derived more
          directly by MARS) which can be found at <https://github.com/TheThirdOne/rars>. There is some effort to keep
          RAWRS in line with conventions found in this Java-based RARS.

      source-code:
        header: "Source Code"
        content: |
          You can find the source code for RAWRS on [GitLab](https://gitlab.com/wilkie/rawrs).

      license:
        header: "License"
        content: |
          The main web application and program content is licensed under the terms of the [GNU AGPLv3.0 license](https://www.gnu.org/licenses/agpl-3.0.en.html).

      development:
        header: "Development"
        content: |
          These are the good folks responsible for adding code to the project and making it the robust, educational experience it is.
        items:
          rawrs: "RAWRS Programming and Design"

      localization:
        header: "Localization"
        content: |
          These are people who dedicated time and energy to translate the application to another language. Without them, accessibility of education and fun would not be possible.

      artwork:
        header: "Artwork"
        content: |
          The beautiful artwork found in this application makes it unique and approachable. These are the folks that helped make that happen.
        items:
          dinosaurs: "Dinosaurs"
          icon: "Icon"

      documentation:
        header: "Documentation"
        items:
          general: "General"
          tutorials: "Tutorials"

      open-source:
        header: "Open Source Software"
        content: |
          This program would not be possible if not for the existing and ongoing effort provided by the community at large within these independently developed and maintained projects.

    usage:
      content: |
        This page gives you a rundown of the various features and helps explore the interface of RAWRS. Refer to this page when you want to know general information about the application or where certain things exist.

      edit-tab:
        header: "Edit Tab"
        content: |
          The "Edit" tab is the place where files are modified. This view
          contains a file listing that allows you to manage files for each
          project. This typically also has example files. There will also be a
          region for opening editors for different types of files.

        targets:
          header: "Target Selector"
          content: |
            The target selector dropdown allows you to pick a different target.
            A target reflects the hardware you want to write a program against.
            This dictates many things such as the capabilities and the tools
            that will be used.

        file-listing:
          header: "File Listing"
          directory: "directory"
          examples: "examples"
          content: |
            The file listing shows the available files for the chosen target.
            Each target maintains its own listing of files. These include
            read-only files that usually serve as examples, and the writable
            files that make up your own projects. Clicking on a file in the
            listing will open the file in the appropriate editor. The button
            next to the file or directory will bring up a menu of actions that
            can be performed on that file.

        new-project:
          header: "Creating New Projects"

        copy-from-examples:
          header: "Copying From Examples"

        file-tabs:
          header: "File Tabs"

        toolbar:
          header: "Toolbar"

        console:
          header: "Console"

      run-tab:
        header: "Run Tab"
        content: |
          The "Run" tab is where the action happens. Here, we have panels for
          different views of our running program. One is usually a simulation of
          the program itself. The other views might be different displays or
          debugging consoles.

        machine-code-listing:
          header: "Machine Code Listing"

        memory-listing:
          header: "Memory Listing"

        label-listing:
          header: "Label Listing"

        register-listing:
          header: "Register Listing"

        simulator:
          header: "Simulator"

      guidance-tab:
        header: "Guidance Tab"
        content: |
          This is the guidance tab! It is the place where all helpful documentation
          will be found. When you switch targets, this tab will be populated with
          documentation that is specific to that platform alongside the general
          documentation for RAWRS, such as this page.

        about:
          header: "About Tab"
          content: |
            This acknowledges the number of people and projects that make up the
            application.

        usage:
          header: "Usage Tab"
          content: |
            This page is, well, this page! It tells you how the application
            works.
