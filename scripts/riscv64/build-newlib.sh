#!/bin/bash

ROOTDIR=$PWD

# The host target we are building
TARGET="riscv64-unknown-elf"

if [ ! ${QUIET} ]; then
  echo ""
  if [ ${RAWRS_COUNTER} ]; then
    echo "${RAWRS_COUNTER}. Building RISC-V newlib"
    echo "-------------------------"
  else
    echo "Building RISC-V newlib"
    echo "======================"
  fi
  echo ""
fi

if [ ! -f ${ROOTDIR}/scripts/riscv64/versions.sh ]; then
  echo " - ERROR: need to run this script from the rawrs project directory"
  exit 1
fi

source $PWD/scripts/riscv64/versions.sh

PATCHDIR=${ROOTDIR}/patches/riscv64/newlib
UTILSDIR=utils/riscv64

SRCDIR=$PWD/packages/riscv64/riscv-newlib
BUILDDIR=packages/riscv64/riscv-newlib-build

INSTALLDIR=assets/static/riscv64

if [ ! ${QUIET} ]; then
  echo " - installing/updating RISC-V packages"
  ./scripts/riscv64/install.sh &> ${ROOTDIR}/packages/riscv64/install-during-binutils.log
fi

# Create build path for building native risc-v binutils
if [ ! -d ${ROOTDIR}/${BUILDDIR} ]; then
  echo " - creating ${BUILDDIR} directory"
  mkdir -p ${ROOTDIR}/${BUILDDIR}
else
  echo " - warning: using existing build directory at ${BUILDDIR}"
fi

# Compile native risc-v newlib
if [ ! -f ${ROOTDIR}/${BUILDDIR}/${TARGET}/newlib/libc/libc.a ]; then
  cd ${ROOTDIR}/${BUILDDIR}

  echo " - configuring a RISC-V newlib"
  PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH ${SRCDIR}/configure \
    --target=${TARGET} \
    --prefix=/usr \
		--enable-newlib-io-long-double \
		--enable-newlib-io-long-long \
		--enable-newlib-io-c99-formats \
		--enable-newlib-register-fini \
		CFLAGS_FOR_TARGET="-O2 -D_POSIX_MODE $(CFLAGS_FOR_TARGET)" \
		CXXFLAGS_FOR_TARGET="-O2 -D_POSIX_MODE $(CXXFLAGS_FOR_TARGET)" \
    &> ${ROOTDIR}/${BUILDDIR}/0-configure.log

  echo " - building a RISC-V newlib"
  PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH make &> ${ROOTDIR}/${BUILDDIR}/1-make.log
else
  echo " - using existing build of RISC-V newlib in ${BUILDDIR}"
fi

if [ ! -f ${ROOTDIR}/${UTILSDIR}/usr/${TARGET}/lib/crt0.o ]; then
  echo " - installing build of RISC-V newlib to ${UTILSDIR}"
  cd ${ROOTDIR}/${BUILDDIR}
  PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH make install DESTDIR=${ROOTDIR}/${UTILSDIR} &> ${ROOTDIR}/${BUILDDIR}/2-make-install.log

  if [ ! -f ${ROOTDIR}/${UTILSDIR}/${TARGET}/lib/crt0.o ]; then
    echo " - ERROR: could not install RISC-V newlib"
    exit 1
  fi
else
  echo " - using existing installed RISC-V newlib"
fi

echo " - RISC-V newlib installed to ${UTILSDIR}"

if [ ! -f ${ROOTDIR}/${INSTALLDIR}/usr/${TARGET}/lib/crt0.o ]; then
  echo " - installing build of RISC-V newlib to ${INSTALLDIR}"
  cd ${ROOTDIR}/${BUILDDIR}
  PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH make install DESTDIR=${ROOTDIR}/${INSTALLDIR} &> ${ROOTDIR}/${BUILDDIR}/3-make-install-assets.log

  if [ ! -f ${ROOTDIR}/${INSTALLDIR}/usr/${TARGET}/lib/crt0.o ]; then
    echo " - ERROR: could not install RISC-V newlib"
    exit 1
  fi
else
  echo " - using existing installed RISC-V newlib"
fi

echo " - RISC-V newlib installed to ${INSTALLDIR}"

# Return to the base path
cd ${ROOTDIR}

# Done.
if [ ! ${QUIET} ]; then
  echo " - done!"
fi
