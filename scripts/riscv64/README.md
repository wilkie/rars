# RISC-V Toolchain

These scripts build a native and a JavaScript (emscripten) version of a complete
RISC-V toolchain. This consists of binutils (an assembler and linker via `as` and
`ld` respectively), a C runtime and standard library via newlib, and a C compiler
via `gcc` (and its subcomponents `cc1`, `collect2`, and `cpp` for its various
stages). Also included is a debugger via `gdb`.

Each of the scripts and components is compiled in a very careful manner to ensure
that the bootstrapping shenanigans of, say, `gcc` are handled correctly. That is,
`gcc` will compile a version of itself to compile itself and that just will not
work when that bootstrapped version is JavaScript for some very obvious reasons
when using it on a native system to produce a JavaScript compiler.

So, it bootstraps the bootstrap by compiling a native version and transferring
the bootstrap compiler over and artificially making it seem to exist in the
future so the `gcc` Makefiles do not recreate it.

All of the GNU projects seem to enjoy building bootstrap generators in some
fashion. Many of the projects will produce header files or even C source files
using supplementary projects that just will not work when those generators are
in JavaScript form trying to generate source files to be used by a native
compiler. Therefore, those generators are also carefully copied into the build
paths for the JavaScript compilation from the native builds.

## Scripts

* `build-binutils.sh` - Builds binutils. Creates `as`, `ld`, `readelf`, and `objdump` JavaScript workers.
* `build-gdb.sh` - Builds `gdb` along with its JavaScript worker.
* `build-gcc.sh` - Builds `gcc`, `cc1`, `cpp`, `gcc-ar`, `gcc-nm` and `collect2` along with JavaScript workers.
* `build-newlib.sh` - Builds `libc` for a (close to) bare metal environment. `gcc` needs at least a minimal `libc`.
* `build-kernel.sh` - Builds a small full assembly RISC-V kernel that runs single applications in user mode.
* `build-tinyemu.sh` - Builds the full RISC-V emulator for JavaScript.

The `gcc` and `g++` that is built has forking disabled and announces the commands it wants to run.
In this form, the underlying system is responsible for delegating the commands to the workers built in other
steps such as `as`, `ld,` and `cc1` in the case of a C project compile. Generally, the output files
left by the final step provided by `gcc` is the resulting output file and every other file is a temporary
intermediate that may or may not be kept.
