#!/bin/bash

ROOTDIR=$PWD

echo ""
if [ ${RAWRS_COUNTER} ]; then
  echo "${RAWRS_COUNTER}. Building GPUTILS"
  echo "-------------------"
else
  echo "Building GPUTILS"
  echo "================"
fi
echo ""

if [ ! -f ${ROOTDIR}/scripts/z80-gb/versions.sh ]; then
  echo " - ERROR: need to run this script from the rawrs project directory"
  exit 1
fi

source $PWD/scripts/z80-gb/versions.sh

PATCHDIR=${ROOTDIR}/patches/z80-gb/gputils
UTILSDIR=utils/z80-gb

SRCDIR=$PWD/packages/common/gputils-${GPUTILS_VERSION}
BUILDDIR=packages/common/gputils-${GPUTILS_VERSION}-build
BUILDJSDIR=packages/common/gputils-${GPUTILS_VERSION}-js

INSTALLDIR=assets/js/targets/common/gputils

# Compile options

PROFILING_OPTS="-O2"

# Uncomment to gain stack traces
#PROFILING_OPTS="${PROFILING_OPTS} -g --profiling-funcs"

CFLAGS="-DHAVE_PSIGNAL -m32 ${PROFILING_OPTS}"

echo " - installing/updating GB packages"
mkdir -p ${ROOTDIR}/packages/z80-gb
./scripts/z80-gb/install.sh &> ${ROOTDIR}/packages/z80-gb/install-during-sdcc.log

if [ ! -d ${ROOTDIR}/${BUILDDIR} ]; then
  echo " - creating ${BUILDDIR} directory"
  mkdir -p ${ROOTDIR}/${BUILDDIR}
else
  echo " - warning: using existing build directory at ${BUILDDIR}"
fi

if [ ! -f ${ROOTDIR}/${UTILSDIR}/usr/bin/${TARGET}-as ]; then
  echo " - configuring a native GPUTILS"
  cd ${ROOTDIR}/${BUILDDIR}
  ${SRCDIR}/configure --prefix=/usr &> ${ROOTDIR}/${BUILDDIR}/0-configure.log

  echo " - building a native GPUTILS"
  make &> ${ROOTDIR}/${BUILDDIR}/1-make.log

  # Install
  if [ ! -d ${ROOTDIR}/${UTILSDIR} ]; then
    echo " - creating ${UTILSDIR} to install native GPUTILS"
    mkdir -p ${ROOTDIR}/${UTILSDIR}
  fi

  echo " - installing native GPUTILS"
  make DESTDIR=${ROOTDIR}/${UTILSDIR} install &> ${ROOTDIR}/${BUILDDIR}/2-make-install.log
else
  echo " - using existing installed native GPUTILS"
fi

echo " - native GPUTILS installed to ${UTILSDIR}"

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
