#!/bin/bash

HEADER="Building Visual Boy Advance"
TARGET=z80-gb
PACKAGE=visualboyadvance

source $PWD/scripts/common/include.sh

visualboy=$(basename ${VISUALBOY_URL})

PATCHDIR=${ROOTDIR}/patches/z80-gb/visualboyadvance
UTILSDIR=utils/z80-gb

SRCDIR=$PWD/packages/z80-gb/${visualboy}
BUILDDIR=packages/z80-gb/${visualboy}-build
BUILDJSDIR=packages/z80-gb/${visualboy}-js
BUILDLIBRETRODIR=packages/z80-gb/${visualboy}-libretro

INSTALLDIR=assets/js/targets/z80-gb/visualboyadvance

echo " - installing/updating GB packages"
mkdir -p ${ROOTDIR}/packages/z80-gb
./scripts/z80-gb/install.sh &> ${ROOTDIR}/packages/z80-gb/install-during-gbdk.log

# Ensure build path exists
if [ ! -d ${ROOTDIR}/${BUILDDIR} ]; then
  echo " - creating the ${BUILDDIR} directory"
  mkdir -p ${ROOTDIR}/${BUILDDIR}
else
  echo " - warning: using existing build directory at ${BUILDDIR}"
fi

# Build a native SDL build using cmake, etc
if [ ! -f ${ROOTDIR}/${BUILDDIR}/vbam ]; then
  cd ${ROOTDIR}/${BUILDDIR}
  echo " - creating makefiles with cmake"
  cmake ${SRCDIR} -DENABLE_FFMPEG=0 -DENABLE_WX=0 -DENABLE_LINK=0 -DENABLE_SDL=1 -DENABLE_ASM_CORE=0 &> ${ROOTDIR}/${BUILDDIR}/0-cmake.log

  echo " - building a native Visual Boy Advance"
  run "make" "${ROOTDIR}/${BUILDDIR}/1-make.log"
else
  echo " - using existing built native Visual Boy Advance"
fi

if [ ! -f ${ROOTDIR}/${BUILDDIR}/vbam ]; then
  echo " - ERROR: could not make a native visual boy"
  exit 1
fi

if [ ${BUILDJSVISUALBOY} ]; then
  # Ensure JavaScript build path exists
  if [ ! -d ${ROOTDIR}/${BUILDJSDIR} ]; then
    echo " - creating the ${BUILDJSDIR} directory"
    mkdir -p ${ROOTDIR}/${BUILDJSDIR}
  else
    echo " - warning: using existing build directory at ${BUILDJSDIR}"
  fi

  if [ ! ${EMSDK} ]; then
    echo " - initializing emcripten"
    source "$ROOTDIR/packages/emsdk/emsdk_env.sh" &> ${ROOTDIR}/${BUILDJSDIR}/0-emscripten-initialize.log
  else
    echo " - using existing activated emcripten at ${EMSDK}"
  fi

  # Compile options

  PROFILING_OPTS="-O2"

  # Uncomment to gain stack traces
  #PROFILING_OPTS="${PROFILING_OPTS} -g --profiling-funcs"

  EMSCRIPTEN_WASM_OPTS="-s WASM=1 -s NO_EXIT_RUNTIME=0 -lworkerfs.js -lfs.js -s FORCE_FILESYSTEM=1 -s ASSERTIONS=1 ${PROFILING_OPTS}"

  # Interpret the native build's log to compile the project with emscripten
  if [ ! -f ${ROOTDIR}/${BUILDJSDIR}/vbam.js ]; then
    echo " - copying gcc wrap C files from native build"
    cp ${ROOTDIR}/${BUILDDIR}/*.c ${ROOTDIR}/${BUILDJSDIR}/.

    echo " - building a JavaScript Visual Boy Advance"

    log=${ROOTDIR}/${BUILDJSDIR}/1-make.log
    rm -f ${log}
    touch ${log}

    cd ${ROOTDIR}/${BUILDJSDIR}
    input=${ROOTDIR}/${BUILDDIR}/1-make.log
    objects=
    linktext=
    installdir=
    finaltarget="vbam.js"
    finalflags="-s ASYNCIFY=1 -s ASYNCIFY_ONLY=@${PATCHDIR}/sync.txt -s FORCE_FILESYSTEM=1 -s WASM=1 -lfs.js -s MODULARIZE=1 -s EXPORT_NAME=VisualBoyAdvance -s EXTRA_EXPORTED_RUNTIME_METHODS=['FS','UTF8ToString','stringToUTF8'] -s TOTAL_MEMORY=33554432 -s ALLOW_MEMORY_GROWTH=0"
    #finalflags="-s ASYNCIFY=1 -s FORCE_FILESYSTEM=1 -s WASM=1 -lfs.js -s MODULARIZE=1 -s EXPORT_NAME=VisualBoyAdvance -s EXTRA_EXPORTED_RUNTIME_METHODS=['FS','UTF8ToString','stringToUTF8'] -s TOTAL_MEMORY=33554432 -s ALLOW_MEMORY_GROWTH=0"
    finalworkerflags="${EMSCRIPTEN_WASM_OPTS}"

    while IFS= read line; do
      cmd=
      link=
      target=
      cmakeflags=`echo "${line}" | grep -o "[^ ]*CMakeFiles/[^/]\+"`/flags.make

      # if c file exists locally, build it
      if [[ `echo "${line}" | grep "Building C"` ]]; then
        target=`echo "${line}" | grep -o "[^ ]\+\.o$" | sed 's;CMakeFiles/[^/]\+/;;' | sed 's;.o$;;'`
        linktext=`echo "${line}" | grep -o "[^ ]*CMakeFiles/[^/]\+"`/link.txt
        installdir=`echo "${line}" | grep -o " [^ ]\+CMakeFiles" | sed "s;CMakeFiles;;"`

        if [[ `echo "${line}" | grep "Building C "` ]]; then
          cmd=emcc
        elif [[ `echo "${line}" | grep "Building CXX "` ]]; then
          cmd=em++
        fi
      fi

      if [[ `echo "${line}" | grep "Linking C"` ]]; then
        if [[ `echo "${line}" | grep "Linking C "` ]]; then
          cmd=emcc
        elif [[ `echo "${line}" | grep "Linking CXX "` ]]; then
          cmd=em++
        fi

        if [[ `echo "${line}" | grep "static library"` ]]; then
          # Creating .a
          link=`echo "${line}" | grep -o "[^ ]\+$"`
          linkext=
          linkflags="-r ${installdir}"
          cmd=emar
        elif [[ `echo "${line}" | grep "executable"` ]]; then
          # Creating executable
          link=`echo "${line}" | grep -o "[^ ]\+$"`
          linkext=.js
          linkflags="-L${ROOTDIR}/packages/system/lib -s LEGACY_GL_EMULATION=1 -s USE_SDL=2 -o ${installdir}"

          # Look for other link flags via link.txt
          if [ ! -f ${subpath}/linktext.sh ]; then
            if [ -f ${ROOTDIR}/${BUILDDIR}/${linktext} ]; then
              cat ${ROOTDIR}/${BUILDDIR}/${linktext} | grep -o " -o .*$" | sed "s; -o [^ ]\+;;" | sed "s;^;ldflags=\";" | sed "s;$;\";" > ${subpath}/linktext.sh
              sed 's; -flto=.;;' -i ${subpath}/linktext.sh
              sed 's; -Wl,[^ "]\+;;g' -i ${subpath}/linktext.sh
              sed 's; -ffat-lto-objects;;' -i ${subpath}/linktext.sh
              sed 's; -mfpmath[^ ]*;;' -i ${subpath}/linktext.sh
              sed 's; -msse[^ ]*;;' -i ${subpath}/linktext.sh
              sed 's; -march[^ ]*;;' -i ${subpath}/linktext.sh
              sed 's; -mtune[^ ]*;;' -i ${subpath}/linktext.sh
              sed 's; -pthread;;' -i ${subpath}/linktext.sh
              sed 's; -lpthread;;' -i ${subpath}/linktext.sh
              sed 's; -lgl;;' -i ${subpath}/linktext.sh
              sed 's; -lSDL2main;;' -i ${subpath}/linktext.sh
              sed 's; -lGLX;;' -i ${subpath}/linktext.sh
              sed 's; -lGLU;;' -i ${subpath}/linktext.sh
              sed 's; -lOpenGL;;' -i ${subpath}/linktext.sh
              echo "# from ${linktext}" >> ${subpath}/linktext.sh
            fi
          fi

          if [ -f ${subpath}/linktext.sh ]; then
            # add link flags
            ldflags=
            source ${subpath}/linktext.sh &>> ${log}
            linkflags="${ldflags} ${linkflags}"
          fi
        fi
      fi

      if [[ "${link}" ]]; then
        fullcmd="${cmd} ${linkflags}${link}${linkext} ${objects}"
        echo "[LINK]: ${cmd} ${link}${linkext}" &>> ${log}
        if [[ "${link}${linkext}" == ${finaltarget} ]]; then
          fullcmd="${cmd} ${linkflags}${link}-module${linkext} ${objects} ${finalflags}"
        fi
        ${fullcmd} &>> ${log}

        if [[ "${link}${linkext}" == ${finaltarget} ]]; then
          # Also build a worker
          fullcmd="${cmd} ${linkflags}${link}-bare.js ${objects} ${finalworkerflags}"
          echo "[LINK]: ${cmd} ${link}-bare.js" &>> ${log}
          ${fullcmd} &>> ${log}
          cat ${ROOTDIR}/lib/pre-worker.js ${ROOTDIR}/${BUILDJSDIR}/${link}-bare.js ${ROOTDIR}/lib/post-worker.js > ${ROOTDIR}/${BUILDJSDIR}/${link}.js
        fi
        objects=
      fi

      if [[ "${target}" ]]; then
        subpath=$(dirname ${target})
        mkdir -p ${subpath}
        targetpath=${SRCDIR}/${target}
        if [ -f $(basename ${target}) ]; then
          targetpath=${target}
        fi
        fullcmd="${cmd} ${targetpath} -c -o ${target}.o"

        # Add to target
        objects="${objects} ${target}.o"

        # Generate a bash import for the cmakeflags
        if [ ! -f ${subpath}/flags.sh ]; then
          cp ${ROOTDIR}/${BUILDDIR}/${cmakeflags} ${subpath}/flags.sh
          sed 's; = \(.*\);="\1";' -i ${subpath}/flags.sh
          sed 's; -flto=.;;' -i ${subpath}/flags.sh
          sed 's; -ffat-lto-objects;;' -i ${subpath}/flags.sh
          sed 's; -mfpmath[^ ]*;;' -i ${subpath}/flags.sh
          sed 's; -msse[^ ]*;;' -i ${subpath}/flags.sh
          sed 's; -march[^ ]*;;' -i ${subpath}/flags.sh
          sed 's; -pthread;;' -i ${subpath}/flags.sh
          sed 's; -lpthread;;' -i ${subpath}/flags.sh
          sed 's; -mtune[^ ]*;;' -i ${subpath}/flags.sh
          echo "# from ${cmakeflags}" >> ${subpath}/flags.sh
        fi

        # -s LEGACY_GL_EMULATION=1 - Links in an emulation layer for glBegin/glEnd, etc
        # -s USE_SDL=2 - Links in an emscripten port of libSDL2
        CFLAGS="-I${ROOTDIR}/packages/system/include -s LEGACY_GL_EMULATION=1 -s USE_SDL=2"
        CXXFLAGS="${CFLAGS}"
        if [ -f ${subpath}/flags.sh ]; then
          source ${subpath}/flags.sh &>> ${log}

          CFLAGS="${C_FLAGS} ${C_DEFINES} ${C_INCLUDES} ${CFLAGS}"
          CXXFLAGS="${CXX_FLAGS} ${CXX_DEFINES} ${CXX_INCLUDES} ${CXXFLAGS}"
        fi

        if [[ "${cmd}" == "emcc" ]]; then
          FLAGS="${CFLAGS}"
          echo "[EMCC]: ${fullcmd}" &>> ${log}
        else
          FLAGS="${CXXFLAGS}"
          echo "[EM++]: ${fullcmd}" &>> ${log}
        fi

        if [ ! ${target}.o -nt ${targetpath} ]; then
          ${fullcmd} ${FLAGS} &>> ${log}
        fi
      fi
    done < "${input}"

    # Patches findEventTarget so it never attaches an event to the window / <html> tag.
    sed "s/function findEventTarget(target){/function findEventTarget(target){if(target>=2){return document.body.querySelector('#visualboy-canvas');}/" -i ${ROOTDIR}/${BUILDJSDIR}/vbam.js
    sed "s/function findEventTarget(target){/function findEventTarget(target){if(target>=2){return document.body.querySelector('#visualboy-canvas');}/" -i ${ROOTDIR}/${BUILDJSDIR}/vbam-module.js
  else
    echo " - using existing built JavaScript Visual Boy Advance"
  fi

  if [ ! -f ${ROOTDIR}/${BUILDJSDIR}/vbam.js ]; then
    echo " - ERROR: could not make a JavaScript Visual Boy Advance"
    exit 1
  fi
fi

# Create a libretro core
if [ ! -f ${ROOTDIR}/${BUILDLIBRETRODIR}/src/libretro/vbam_libretro.wasm ]; then
  cd ${ROOTDIR}/${BUILDLIBRETRODIR}

  echo " - patching libretro core"
  if [ ! -f ${ROOTDIR}/${BUILDLIBRETRODIR}/patching.log ]; then
    # build the lto plugin
    patch -s -Np1 < ${PATCHDIR}/libretro.patch &> ${ROOTDIR}/${BUILDLIBRETRODIR}/patching.log

    # copy the files into the path
    cp ${PATCHDIR}/prelim.* ${ROOTDIR}/${BUILDLIBRETRODIR}/src/libretro/.
    cp ${PATCHDIR}/rawrs.* ${ROOTDIR}/${BUILDLIBRETRODIR}/src/libretro/.
  else
    echo " - patches already believed to be applied (remove patching.log to reapply)"
  fi

  cd ${ROOTDIR}/${BUILDLIBRETRODIR}/src/libretro

  if [ ! ${EMSDK} ]; then
    echo " - initializing emcripten"
    source "$ROOTDIR/packages/emsdk/emsdk_env.sh" &> ${ROOTDIR}/${BUILDLIBRETRODIR}/0-libretro-emscripten-initialize.log
  else
    echo " - using existing activated emcripten at ${EMSDK}"
  fi

  echo " - building a JavaScript Visual Boy Advance libretro core"
  run "emmake make" "${ROOTDIR}/${BUILDLIBRETRODIR}/1-libretro-make.log"
else
  echo " - using existing built JavaScript Visual Boy Advance libretro core"
fi

if [ ! -f ${ROOTDIR}/${BUILDLIBRETRODIR}/src/libretro/vbam_libretro.wasm ]; then
  echo " - ERROR: could not make a JavaScript Visual Boy Advance libretro core"
  exit 1
fi

# Create the worker from the libretro core
if [ ! -f ${ROOTDIR}/${BUILDLIBRETRODIR}/src/libretro/vbam_libretro-worker.js ]; then
  echo " - building a JavaScript Visual Boy Advance libretro core worker"

  cd ${ROOTDIR}/${BUILDLIBRETRODIR}/src/libretro

  cat ${PATCHDIR}/prelim.js ${ROOTDIR}/lib/pre-worker.js ${ROOTDIR}/${BUILDLIBRETRODIR}/src/libretro/vbam_libretro.js ${ROOTDIR}/lib/post-worker.js > ${ROOTDIR}/${BUILDLIBRETRODIR}/src/libretro/vbam_libretro-worker.js
else
  echo " - using existing built JavaScript Visual Boy Advance libretro core worker"
fi

if [ ! -f ${ROOTDIR}/${BUILDLIBRETRODIR}/src/libretro/vbam_libretro-worker.js ]; then
  echo " - ERROR: could not make a JavaScript Visual Boy Advance libretro core worker"
  exit 1
fi

cd ${ROOTDIR}

if [ ! -d ${INSTALLDIR} ]; then
  echo " - creating ${INSTALLDIR} for JavaScript files"
  mkdir -p ${INSTALLDIR}
else
  echo " - installing to existing ${INSTALLDIR} for JavaScript files"
fi

mkdir -p ${ROOTDIR}/${INSTALLDIR}

# Install vbam.js/vbam.wasm
cd ${ROOTDIR}/${BUILDJSDIR}

if [ ! -f ${ROOTDIR}/${INSTALLDIR}/vbam-module.js ]; then
  echo " - creating ${INSTALLDIR}/vbam-module.js"
  cp vbam-module.js ${ROOTDIR}/${INSTALLDIR}/vbam-module.js
else
  echo " - creating ${INSTALLDIR}/vbam-module.js (exists already)"
fi

if [ ! -f ${ROOTDIR}/${INSTALLDIR}/vbam-module.wasm ]; then
  echo " - creating ${INSTALLDIR}/vbam-module.wasm"
  cp vbam-module.wasm ${ROOTDIR}/${INSTALLDIR}/vbam-module.wasm
else
  echo " - creating ${INSTALLDIR}/vbam-module.wasm (exists already)"
fi

if [ ! -f ${ROOTDIR}/${INSTALLDIR}/vbam.js ]; then
  echo " - creating ${INSTALLDIR}/vbam.js"
  cp vbam.js ${ROOTDIR}/${INSTALLDIR}/vbam.js
else
  echo " - creating ${INSTALLDIR}/vbam.js (exists already)"
fi

if [ ! -f ${ROOTDIR}/${INSTALLDIR}/vbam-bare.wasm ]; then
  echo " - creating ${INSTALLDIR}/vbam-bare.wasm"
  cp vbam-bare.wasm ${ROOTDIR}/${INSTALLDIR}/vbam-bare.wasm
else
  echo " - creating ${INSTALLDIR}/vbam-bare.wasm (exists already)"
fi

cd ${ROOTDIR}/${BUILDLIBRETRODIR}/src/libretro

if [ ! -f ${ROOTDIR}/${INSTALLDIR}/vbam_libretro-worker.js ]; then
  echo " - creating ${INSTALLDIR}/vbam_libretro-worker.js"
  cp vbam_libretro-worker.js ${ROOTDIR}/${INSTALLDIR}/vbam_libretro-worker.js
else
  echo " - creating ${INSTALLDIR}/vbam_libretro-worker.js (exists already)"
fi

if [ ! -f ${ROOTDIR}/${INSTALLDIR}/vbam_libretro.wasm ]; then
  echo " - creating ${INSTALLDIR}/vbam_libretro.wasm"
  cp vbam_libretro.wasm ${ROOTDIR}/${INSTALLDIR}/vbam_libretro.wasm
else
  echo " - creating ${INSTALLDIR}/vbam_libretro.wasm (exists already)"
fi

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
