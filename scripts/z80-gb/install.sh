#!/bin/bash

echo ""
echo "Installing Z80 GameBoy Target"
echo "============================="
echo ""

ROOTDIR=$PWD
COMMONDIR=packages/common
INSTALLDIR=packages/z80-gb

if [ ! -f ${ROOTDIR}/scripts/z80-gb/versions.sh ]; then
  echo " - ERROR: need to run this script from the rawrs project directory"
  echo ""
  echo "Failed."
  exit 1
fi

echo " - checking prerequisites"

# TODO: I think we need libSDL2 to build the native visual boy.
CHECK="texi2pdf bison flex gcc make cmake"
good=1

for binary in ${CHECK}
do
  if ! hash ${binary} 2>/dev/null; then
    echo " - ERROR: must install ${binary}"
    good=0
  fi
done

if [ ${good} -eq 0 ]; then
  echo " - ERROR: prerequisites needed"
  echo ""
  echo "Failed."
  exit 1
fi

echo " - all prerequisites found"
echo ""

source $PWD/scripts/z80-gb/versions.sh

echo "1. GPUTILS"
echo "----------"
echo ""

echo " - using version ${GPUTILS_VERSION}"

if [ ! -d ${ROOTDIR}/${INSTALLDIR} ]; then
  echo " - creating ${INSTALLDIR}"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
fi

if [ ! -d ${ROOTDIR}/${COMMONDIR}/gputils-${GPUTILS_VERSION} ]; then
  echo " - downloading GPUTILS ${GPUTILS_VERSION}"
  wget -q -nc ${GPUTILS_URL} -O ${ROOTDIR}/${COMMONDIR}/gputils-${GPUTILS_VERSION}.tar.bz2
  cd ${ROOTDIR}/${COMMONDIR}
  echo " - unpacking gputils-${GPUTILS_VERSION}.tar.bz2"
  tar xf ${ROOTDIR}/${COMMONDIR}/gputils-${GPUTILS_VERSION}.tar.bz2
  cd ../..
else
  echo " - GPUTILS already found with version ${GPUTILS_VERSION}"
fi

echo ""
echo "2. Boost"
echo "--------"
echo ""

echo " - using version ${BOOST_VERSION}"

_boostver=${BOOST_VERSION//./_}

if [ ! -d ${ROOTDIR}/${COMMONDIR}/boost_${_boostver} ]; then
  echo " - downloading boost ${BOOST_VERSION}"
  wget -q -nc ${BOOST_URL} -O ${ROOTDIR}/${COMMONDIR}/boost-${BOOST_VERSION}.tar.bz2
  cd ${ROOTDIR}/${COMMONDIR}
  echo " - unpacking boost-${BOOST_VERSION}.tar.bz2"
  tar xf ${ROOTDIR}/${COMMONDIR}/boost-${BOOST_VERSION}.tar.bz2
  cd ../..
else
  echo " - boost already found with version ${BOOST_VERSION}"
fi

echo ""
echo "3. SDCC"
echo "-------"
echo ""

echo " - using version ${SDCC_VERSION}"

if [ ! -d ${ROOTDIR}/${INSTALLDIR}/sdcc-${SDCC_VERSION} ]; then
  echo " - downloading SDCC ${SDCC_VERSION}"
  wget -q -nc ${SDCC_URL} -O ${ROOTDIR}/${INSTALLDIR}/sdcc-${SDCC_VERSION}.tar.bz2
  cd ${ROOTDIR}/${INSTALLDIR}
  echo " - unpacking sdcc-${SDCC_VERSION}.tar.bz2"
  tar xf ${ROOTDIR}/${INSTALLDIR}/sdcc-${SDCC_VERSION}.tar.bz2
  mv ${ROOTDIR}/${INSTALLDIR}/sdcc ${ROOTDIR}/${INSTALLDIR}/sdcc-${SDCC_VERSION}
  cd ../..
else
  echo " - SDCC already found with version ${SDCC_VERSION}"
fi

echo ""
echo "4. GameBoy Development Kit (GBDK)"
echo "---------------------------------"
echo ""

echo " - using version ${GBDK_VERSION}"

if [ ! -d ${ROOTDIR}/${INSTALLDIR}/gbdk-2020 ]; then
  echo " - downloading gbdk-2020 from git sources"
  cd ${ROOTDIR}/${INSTALLDIR}
  git clone ${GBDK_URL}
  cd ${ROOTDIR}
else
  echo " - gbdk source repository already exists"
fi

cd ${ROOTDIR}/${INSTALLDIR}/gbdk-2020
if ! git cat-file -e ${GBDK_VERSION}; then
  echo " - fetching updates to gbdk-2020"
  git pull
else
  echo " - revision ${GBDK_VERSION} found"
fi

echo ""
echo "5. Visual Boy Advance (Emulator)"
echo "--------------------------------"
echo ""

echo " - using version ${VISUALBOY_VERSION}"

visualboy=$(basename ${VISUALBOY_URL})
if [ ! -d ${ROOTDIR}/${INSTALLDIR}/${visualboy} ]; then
  echo " - downloading ${visualboy} from git sources"
  cd ${ROOTDIR}/${INSTALLDIR}
  git clone ${VISUALBOY_URL}
  cd ${ROOTDIR}
else
  echo " - ${visualboy} source repository already exists"
fi

cd ${ROOTDIR}/${INSTALLDIR}/${visualboy}
if ! git cat-file -e ${VISUALBOY_VERSION}; then
  echo " - fetching updates to ${visualboy}"
  git pull
else
  echo " - revision ${VISUALBOY_VERSION} found"
fi

visualboy=$(basename ${VISUALBOY_URL})
if [ ! -d ${ROOTDIR}/${INSTALLDIR}/${visualboy}-libretro ]; then
  echo " - downloading ${visualboy} from git sources (libretro)"
  cd ${ROOTDIR}/${INSTALLDIR}
  git clone ${VISUALBOY_URL} ${ROOTDIR}/${INSTALLDIR}/${visualboy}-libretro
  cd ${ROOTDIR}
else
  echo " - ${visualboy}-libretro source repository already exists"
fi

cd ${ROOTDIR}/${INSTALLDIR}/${visualboy}-libretro
if ! git cat-file -e ${VISUALBOY_VERSION}; then
  echo " - fetching updates to ${visualboy}-libretro"
  git pull
else
  echo " - revision ${VISUALBOY_VERSION} found for libretro repository"
fi

echo ""
echo "Done."
