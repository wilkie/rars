#!/bin/bash

HEADER="Building SDCC"
TARGET=z80-gb
PACKAGE=sdcc

source $PWD/scripts/common/include.sh

SRCDIR=$PWD/packages/z80-gb/sdcc-${SDCC_VERSION}
BUILDDIR=packages/z80-gb/sdcc-${SDCC_VERSION}-build
BUILDJSDIR=packages/z80-gb/sdcc-${SDCC_VERSION}-js

INSTALLDIR=assets/js/targets/z80-gb/sdcc
LIBINSTALLDIR=assets/static/z80-gb

# Compile options

PROFILING_OPTS="-O2"

# Uncomment to gain stack traces
#PROFILING_OPTS="${PROFILING_OPTS} -g --profiling-funcs"

EMSCRIPTEN_WASM_OPTS="-s WASM=1 -s NO_EXIT_RUNTIME=0 -lworkerfs.js -lfs.js -s FORCE_FILESYSTEM=1 -s ASSERTIONS=1 ${PROFILING_OPTS}"

CFLAGS="-I${ROOTDIR}/packages/system/include -L${ROOTDIR}/packages/system/lib -DHAVE_PSIGNAL -DHAVE_STRSIGNAL=1 -DHAVE_DECL_SBRK=1 -DHAVE_DECL_STRSIGNAL=1 -DHAVE_SYS_RESOURCE_H=1 -DHAVE_SYS_TIMES_H=1 -DHAVE_STDLIB_H=1 -DHAVE_UNISTD_H=1 -DHAVE_STRING_H=1 -DHAVE_SYS_STAT_H=1 -DHAVE_FCNTL_H=1 -m32 ${PROFILING_OPTS} -s USE_PTHREADS=0 -include limits.h"

echo " - installing/updating GB packages"
mkdir -p ${ROOTDIR}/packages/z80-gb
./scripts/z80-gb/install.sh &> ${ROOTDIR}/packages/z80-gb/install-during-sdcc.log

echo " - patching sdcc"

cd ${SRCDIR}

if [ ! -f ${SRCDIR}/patching.log ]; then
  run "patch -s -Np1 -i ${PATCHDIR}/makebin-nogogo-v2-bool.patch" "${SRCDIR}/patching.log"
  run "patch -s -Np1 -i ${PATCHDIR}/sdas_macro_80_char_overflow.patch" "${SRCDIR}/patching.log" 1
  run "patch -s -Np1 -i ${PATCHDIR}/sdcc_z80_boN_baN.patch" "${SRCDIR}/patching.log" 1
  run "patch -s -Np1 -i ${PATCHDIR}/sdcc_z80_enable_incbin.patch" "${SRCDIR}/patching.log" 1
  run "patch -s -Np1 -i ${PATCHDIR}/sdldz80-sms-virtual-address.patch" "${SRCDIR}/patching.log" 1
  touch ${SRCDIR}/patching.log
else
  echo " - patches already believed to be applied (remove patching.log to reapply)"
fi

if [ ! -d ${ROOTDIR}/${BUILDDIR} ]; then
  echo " - copying to the ${BUILDDIR} directory"
  cp -r ${SRCDIR} ${ROOTDIR}/${BUILDDIR}
else
  echo " - warning: using existing build directory at ${BUILDDIR}"
fi

_boostver=${BOOST_VERSION//./_}

if [ ! -f ${ROOTDIR}/${UTILSDIR}/usr/bin/sdcc ]; then
  echo " - configuring a native sdcc for GameBoy targets"
  cd ${ROOTDIR}/${BUILDDIR}
  PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH CXXFLAGS="-I${ROOTDIR}/packages/common/boost_${_boostver}" ${SRCDIR}/configure --prefix=/usr &> ${ROOTDIR}/${BUILDDIR}/0-configure.log

  echo " - building a native sdcc for GameBoy targets"
  PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH run "make CXXFLAGS='-I${ROOTDIR}/packages/common/boost_${_boostver}'" "${ROOTDIR}/${BUILDDIR}/1-make.log"

  # Install
  if [ ! -d ${ROOTDIR}/${UTILSDIR} ]; then
    echo " - creating ${UTILSDIR} to install native sdcc"
    mkdir -p ${ROOTDIR}/${UTILSDIR}
  fi

  echo " - installing native sdcc for GameBoy targets"
  PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH make CXXFLAGS="-I${ROOTDIR}/packages/common/boost_${_boostver}" DESTDIR=${ROOTDIR}/${UTILSDIR} install &> ${ROOTDIR}/${BUILDDIR}/2-make-install.log
else
  echo " - using existing installed native sdcc for GameBoy targets"
fi

echo " - native sdcc installed to ${UTILSDIR}"

if [ ! -d ${ROOTDIR}/${BUILDJSDIR} ]; then
  echo " - copying to the ${BUILDJSDIR} directory"
  cp -r ${SRCDIR} ${ROOTDIR}/${BUILDJSDIR}

  echo " - patching sdcc"
  run "patch -s -Np0 -i ${PATCHDIR}/sdcc-dispatch.patch" "${ROOTDIR}/${BUILDJSDIR}/patching.log"
else
  echo " - warning: using existing build directory at ${BUILDJSDIR}"
fi

if [ ! ${EMSDK} ]; then
  echo " - initializing emcripten"
  source "$ROOTDIR/packages/emsdk/emsdk_env.sh" &> ${ROOTDIR}/${BUILDJSDIR}/0-emscripten-initialize.log
else
  echo " - using existing activated emcripten at ${EMSDK}"
fi

cd ${ROOTDIR}/${BUILDJSDIR}

if [ ! -f ${ROOTDIR}/${BUILDJSDIR}/support/sdbinutils/bfd/.libs/libbfd.a ]; then
  echo " - configuring a JavaScript sdcc for GameBoy targets"
  cd ${ROOTDIR}/${BUILDJSDIR}
  PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH CFLAGS="${CFLAGS}" CXXFLAGS="${CFLAGS} -I${ROOTDIR}/packages/common/boost_${_boostver}" emconfigure ${SRCDIR}/configure &> ${ROOTDIR}/${BUILDJSDIR}/1-configure.log

  echo " - configuring a JavaScript binutils for sdcc for GameBoy targets"
  cd ${ROOTDIR}/${BUILDJSDIR}/support/sdbinutils
  PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH CFLAGS="${CFLAGS}" CXXFLAGS="${CFLAGS} -I${ROOTDIR}/packages/common/boost_${_boostver}" emconfigure ${SRCDIR}/support/sdbinutils/configure \
    --disable-option-checking \
    --disable-lto --disable-nls \
    --disable-install-libiberty \
    --disable-multilib --disable-nls \
    --disable-werror &> ${ROOTDIR}/${BUILDJSDIR}/2-configure-sdbinutils.log

  echo " - copying over native generators needed during compilation"

  COPIES="support/sdbinutils/bfd/doc/chew support/sdbinutils/bfd/doc/chew.stamp support/sdbinutils/binutils/sysinfo"

  for fixup in ${COPIES}
  do
    rm -f ${ROOTDIR}/${BUILDJSDIR}/${fixup}
    mkdir -p ${ROOTDIR}/${BUILDJSDIR}/$(dirname "${fixup}")
    cp --preserve=mode ${ROOTDIR}/${BUILDDIR}/${fixup} ${ROOTDIR}/${BUILDJSDIR}/${fixup}
    chmod +x ${ROOTDIR}/${BUILDJSDIR}/${fixup}
    touch -d "+24 hour" ${ROOTDIR}/${BUILDJSDIR}/${fixup}
  done

  echo " - building a JavaScript sdcc for GameBoy targets"
  PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH run "emmake make CFLAGS=\"${CFLAGS}\" CPPFLAGS=\"${CFLAGS} -I${ROOTDIR}/packages/common/boost_${_boostver}\" CXXFLAGS=\"${CFLAGS} -I${ROOTDIR}/packages/common/boost_${_boostver}\"" "${ROOTDIR}/${BUILDJSDIR}/3-make.log"

  echo " - doing a make install for the JavaScript sdcc for GameBoy targets"
  PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH emmake make install DESTDIR=${ROOTDIR}/${BUILDJSDIR}/wasm CFLAGS="${CFLAGS}" CPPFLAGS="${CFLAGS} -I${ROOTDIR}/packages/common/boost_${_boostver}" CXXFLAGS="${CFLAGS} -I${ROOTDIR}/packages/common/boost_${_boostver}" &> ${ROOTDIR}/${BUILDJSDIR}/4-make-install.log
else
  echo " - using existing built JavaScript sdcc for GameBoy targets"
fi

cd ${ROOTDIR}

if [ ! -d ${ROOTDIR}/${INSTALLDIR} ]; then
  echo " - creating ${INSTALLDIR} for JavaScript workers"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
else
  echo " - installing to existing ${INSTALLDIR} for JavaScript workers"
fi

# We need JavaScript versions of several applications:
BINARIES="support/makebin/makebin support/packihx/packihx sim/ucsim/s51.src/s51 sim/ucsim/avr.src/savr support/sdbinutils/binutils/sdar sdas/as8xcxxx/sdas390 sdas/as6808/sdas6808 sdas/as8051/sdas8051 sdas/asgb/sdasgb sdas/aspdk13/sdaspdk13 sdas/aspdk14/sdaspdk14 sdas/aspdk15/sdaspdk15 sdas/asrab/sdasrab sdas/asstm8/sdasstm8 sdas/astlcs90/sdastlcs90 sdas/asz80/sdasz80 src/sdcc debugger/mcs51/sdcdb support/cpp/sdcpp sdas/linksrc/sdld support/sdbinutils/binutils/sdnm support/sdbinutils/binutils/sdobjcopy support/sdbinutils/binutils/sdranlib sim/ucsim/hc08.src/shc08 sim/ucsim/m6809.src/sm6809 sim/ucsim/mcs6502.src/smcs6502 sim/ucsim/p1516.src/sp1516 sim/ucsim/pdk.src/spdk sim/ucsim/rxk.src/srxk sim/ucsim/st7.src/sst7 sim/ucsim/stm8.src/sstm8 sim/ucsim/tlcs.src/stlcs sim/ucsim/xa.src/sxa sim/ucsim/z80.src/sz80"

for binary in ${BINARIES}
do
  dest=$(basename ${binary})
  subpath=$(dirname ${binary})
  existingpath=
  maketarget=${dest}
  workingpath=${subpath}
  buildpath=${subpath}
  cflags="${CFLAGS} -I${PWD}"
  cppflags="${CFLAGS} -I${PWD} -I${ROOTDIR}/packages/common/boost_${_boostver}"
  ldflags=
  libtool=1
  if [[ ${binary} == debugger* ]]; then
    # Generic debugger/* binaries (sdcdb)
    maketarget="-C ${subpath}"
    existingpath=${ROOTDIR}/${BUILDJSDIR}/bin/
    workingpath=.
    cflags=
    cppflags=
    libtool=
  fi
  if [[ ${binary} == support* ]]; then
    # Generic support/* binaries like packihx
    maketarget="-C ${subpath}"
    existingpath=${ROOTDIR}/${BUILDJSDIR}/bin/
    workingpath=.
    cflags=
    cppflags=
    libtool=
  fi
  if [[ ${binary} == support/sdbinutils/binutils* ]]; then
    # Ar packager
    maketarget="-C ${subpath}"
    existingpath=${subpath}/
    workingpath=.
    cflags=
    cppflags=
    libtool=1
  fi
  if [[ ${binary} == sdas* ]]; then
    # Generic sdas/* assemblers
    maketarget="-C ${subpath}"
    existingpath=${ROOTDIR}/${BUILDJSDIR}/bin/
    workingpath=.
    cflags=
    cppflags=
    libtool=
  fi
  if [[ ${binary} == *linksrc* ]]; then
    # Linkers (just sdld)
    maketarget="sdcc-ld"
    existingpath=${ROOTDIR}/${BUILDJSDIR}/bin/
    cflags=
    cppflags=
    libtool=
  fi
  if [[ ${binary} == *ucsim* ]]; then
    # The architecture binaries
    existingpath=${ROOTDIR}/${BUILDJSDIR}/${subpath}/
    subsubpath=$(basename ${subpath})
    subpath=$(dirname ${subpath})
    workingpath=${subpath}
    maketarget="-C ${subsubpath} ${dest}"
    cflags=
    cppflags=
    libtool=
  fi
  if [[ ${binary} == src* ]]; then
    # Compilers
    maketarget="../bin/${dest}"
    existingpath=${ROOTDIR}/${BUILDJSDIR}/bin/
    cflags=
    cppflags=
    libtool=
  fi
  if [[ ${binary} == *sdcc ]]; then
    ldflags="${cflags} -s INITIAL_MEMORY=67108864"
  fi
  binary=$(basename ${binary})

  if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${dest}.js ]; then
    echo " - creating ${INSTALLDIR}/${dest}.js"
    cd ${ROOTDIR}/${BUILDJSDIR}/${workingpath}
    rm -f ${existingpath}${dest}
    rm -f ${existingpath}${dest}.wasm
    if [ ! -z "${cflags}" ]; then
      PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH CPLUS_INCLUDE_PATH="${ROOTDIR}/packages/common/boost_${_boostver}" emmake make ${maketarget} CFLAGS="${cflags}" CXXFLAGS="${cppflags}" CPPFLAGS="${cppflags}" &> ${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log
    else
      PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH CPLUS_INCLUDE_PATH="${ROOTDIR}/packages/common/boost_${_boostver}" emmake make ${maketarget} &> ${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log
    fi
    context=0
    if [ -z "${libtool}" ]; then
      while [[ `cat ${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log | grep -e "-o [^ ]*${dest} " -e "-o [^ ]*${dest}$" -A${context}` == *\\ ]]; do
        context=$[$context + 1]
      done
    else
      while [[ `cat ${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log | grep -e "libtool: link:.\+-o ${binary} " -A${context}` == *\\ ]]; do
        context=$[$context + 1]
      done
    fi

    # Creates *-bare.js
    if [ -z "${libtool}" ]; then
      COMMAND=`cat ${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log | grep -e "-o [^ ]*${dest} " -e "-o [^ ]*${dest}$" -A${context} | sed "s;-o [^ ]*${dest};${EMSCRIPTEN_WASM_OPTS} ${ldflags} -o ${ROOTDIR}/${INSTALLDIR}/${dest}-bare.js ;"`
    else
      COMMAND=`cat ${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log | grep -e "libtool: link:.\+-o ${binary} " -A${context} | sed "s;libtool: link: ;;" | sed "s;-o [^ ]*${dest};${EMSCRIPTEN_WASM_OPTS} ${ldflags} -o ${ROOTDIR}/${INSTALLDIR}/${dest}-bare.js ;"`
    fi
    echo ${COMMAND} > ${ROOTDIR}/${BUILDJSDIR}/build-${binary}.sh
    chmod +x ${ROOTDIR}/${BUILDJSDIR}/build-${binary}.sh
    cd ${ROOTDIR}/${BUILDJSDIR}/${buildpath}
    source ${ROOTDIR}/${BUILDJSDIR}/build-${binary}.sh

    if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${dest}-bare.js ]; then
      echo " - ERROR: cannot make JavaScript ${dest}"
      exit 1
    fi

    # Creates the worker itself
    cat ${ROOTDIR}/assets/js/browserfs.min.js ${ROOTDIR}/lib/pre-worker.js ${ROOTDIR}/${INSTALLDIR}/${dest}-bare.js ${ROOTDIR}/lib/post-worker.js > ${ROOTDIR}/${INSTALLDIR}/${dest}.js
  else
    echo " - creating ${INSTALLDIR}/${dest}.js (exists already)"
  fi
done

# Copy sdld to sdld* applications
SDLD_BINARIES="sdld6808 sdldgb sdldpdk sdldstm8 sdldz80"
for binary in ${SDLD_BINARIES}
do
  if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${binary}.js ]; then
    echo " - symlinking to sdld for ${INSTALLDIR}/${binary}.js"
    ln -sf sdld.js ${ROOTDIR}/${INSTALLDIR}/${binary}.js
  else
    echo " - symlinking to sdld for ${INSTALLDIR}/${binary}.js (exists already)"
  fi
done

# Install system root (${UTILSDIR}/usr => assets/static/z80-gb/usr)

if [ ! -d ${ROOTDIR}/${LIBINSTALLDIR}/usr ]; then
  echo " - installing native sdcc for GameBoy targets to system root"
  cd ${ROOTDIR}/${BUILDDIR}
  PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH make CXXFLAGS="-I${ROOTDIR}/packages/common/boost_${_boostver}" DESTDIR=${ROOTDIR}/${LIBINSTALLDIR} install &> ${ROOTDIR}/${BUILDDIR}/3-make-install-to-assets-static.log
fi

# Purge binaries from system root
if [ -d ${ROOTDIR}/${LIBINSTALLDIR}/usr/bin ]; then
  echo " - removing /usr/bin from system root"
  rm -r ${ROOTDIR}/${LIBINSTALLDIR}/usr/bin
fi

# Purge doc from system root
if [ -d ${ROOTDIR}/${LIBINSTALLDIR}/usr/share/doc ]; then
  echo " - removing /usr/share/doc from system root"
  rm -r ${ROOTDIR}/${LIBINSTALLDIR}/usr/share/doc
fi

# Purge man from system root
if [ -d ${ROOTDIR}/${LIBINSTALLDIR}/usr/share/man ]; then
  echo " - removing /usr/share/man from system root"
  rm -r ${ROOTDIR}/${LIBINSTALLDIR}/usr/share/man
fi

# Compress system root /usr
if [ ! -f ${ROOTDIR}/${LIBINSTALLDIR}/sdcc.zip ]; then
  echo " - creating ${LIBINSTALLDIR}/sdcc.zip"
  cd ${ROOTDIR}/${LIBINSTALLDIR}/usr
  zip ../sdcc.zip -qr *
else
  echo " - creating ${LIBINSTALLDIR}/sdcc.zip (already exists)"
fi

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
