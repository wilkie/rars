#!/bin/bash

GPUTILS_VERSION=1.5.2
GPUTILS_URL=https://sourceforge.net/projects/gputils/files/gputils/1.5.0/gputils-${GPUTILS_VERSION}.tar.bz2/download

BOOST_VERSION=1.68.0
_boostver=${BOOST_VERSION//./_}
BOOST_URL=https://boostorg.jfrog.io/artifactory/main/release/${BOOST_VERSION}/source/boost_${_boostver}.tar.bz2

SDCC_URL=https://github.com/gbdk-2020/gbdk-2020-sdcc/releases/download/sdcc-12539-compiler-source-unpatched/sdcc-code-src-20210711-12539.tar.bz2
SDCC_VERSION=12539

GBDK_URL=https://github.com/gbdk-2020/gbdk-2020
GBDK_VERSION=e64fa36573ba45fd6eabcac38bfb11e6e2890970

VISUALBOY_URL=https://github.com/visualboyadvance-m/visualboyadvance-m
VISUALBOY_VERSION=cdeb1027cc91a4b8036a763992336b8a2ba8f724
