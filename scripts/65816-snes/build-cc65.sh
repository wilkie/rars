#!/bin/bash

HEADER="Building CC65"
TARGET=65816-snes
PACKAGE=cc65

source $PWD/scripts/common/include.sh

SRCDIR=$PWD/packages/${TARGET}/cc65
BUILDDIR=packages/${TARGET}/cc65-build
BUILDJSDIR=packages/${TARGET}/cc65-js

INSTALLDIR=assets/js/targets/${TARGET}/cc65

# Compile options

PROFILING_OPTS="-O2"

# Uncomment to gain stack traces
#PROFILING_OPTS="${PROFILING_OPTS} -g --profiling-funcs"

EMSCRIPTEN_WASM_OPTS="-s WASM=1 -s NO_EXIT_RUNTIME=0 -lworkerfs.js -lfs.js -s FORCE_FILESYSTEM=1 -s ASSERTIONS=1 ${PROFILING_OPTS}"

echo " - installing/updating packages"
mkdir -p ${ROOTDIR}/packages/${TARGET}
./scripts/${TARGET}/install.sh &> ${ROOTDIR}/packages/${TARGET}/install-during-${PACKAGE}.log

if [ ! -d ${ROOTDIR}/${BUILDDIR} ]; then
  echo " - copying to the ${BUILDDIR} directory"
  cp -r ${SRCDIR} ${ROOTDIR}/${BUILDDIR}
else
  echo " - warning: using existing build directory at ${BUILDDIR}"
fi

if [ ! -f ${ROOTDIR}/${UTILSDIR}/usr/bin/cc65 ]; then
  echo " - building a native cc65 for Super Nintendo targets"
  cd ${ROOTDIR}/${BUILDDIR}
  run "make" "${ROOTDIR}/${BUILDDIR}/0-make.log"

  # Install
  if [ ! -d ${ROOTDIR}/${UTILSDIR} ]; then
    echo " - creating ${UTILSDIR} to install native cc65"
    mkdir -p ${ROOTDIR}/${UTILSDIR}
  fi

  echo " - installing native cc65 for Super Nintendo targets"
  make PREFIX=${ROOTDIR}/${UTILSDIR}/usr install &> ${ROOTDIR}/${BUILDDIR}/1-make-install.log
else
  echo " - using existing installed native cc65 for Super Nintendo targets"
fi

echo " - native cc65 installed to ${UTILSDIR}"

if [ ! -d ${ROOTDIR}/${BUILDJSDIR} ]; then
  echo " - copying to the ${BUILDJSDIR} directory"
  cp -r ${SRCDIR} ${ROOTDIR}/${BUILDJSDIR}

  echo " - patching cc65"
  #cd ${ROOTDIR}/${BUILDJSDIR}
  #patch -s -Np1 -i ${PATCHDIR}/gbdk-lcc.patch &> ${ROOTDIR}/${BUILDJSDIR}/patching.log
else
  echo " - warning: using existing build directory at ${BUILDJSDIR}"
fi

if [ ! ${EMSDK} ]; then
  echo " - initializing emcripten"
  source "$ROOTDIR/packages/emsdk/emsdk_env.sh" &> ${ROOTDIR}/${BUILDJSDIR}/0-emscripten-initialize.log
else
  echo " - using existing activated emcripten at ${EMSDK}"
fi

cd ${ROOTDIR}/${BUILDJSDIR}

if [ ! -f ${ROOTDIR}/${BUILDJSDIR}/wrk/cc65/main.o ]; then
  echo " - building a JavaScript cc65 for Super Nintendo targets"
  cd ${ROOTDIR}/${BUILDJSDIR}
  run "emmake make CC=emcc CXX=em++" "${ROOTDIR}/${BUILDJSDIR}/1-make.log"
else
  echo " - using existing built JavaScript cc65 for Super Nintendo targets"
fi

BINARIES="ar65 ca65 cc65 chrcvt65 cl65 co65 da65 grc65 ld65 od65 sim65 sp65"

for binary in ${BINARIES}
do
  dest=$(basename ${binary})
  binary=$(basename ${binary})

  if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${dest}.js ]; then
    echo " - creating ${INSTALLDIR}/${dest}.js"
    cd ${ROOTDIR}/${BUILDJSDIR}
    rm -f bin/${binary}.exe
    rm -f bin/${binary}.wasm
    run "emmake make CC=emcc CXX=em++ ${binary}" "${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log"

    # Creates *-bare.js
    COMMAND=`cat ${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log | grep -e "\-o ../bin/${binary}.exe" | sed "s;-o ../bin/${binary}.exe;${EMSCRIPTEN_WASM_OPTS} -o ${ROOTDIR}/${INSTALLDIR}/${dest}-bare.js;"`
    echo ${COMMAND} > ${ROOTDIR}/${BUILDJSDIR}/build-${binary}.sh
    chmod +x ${ROOTDIR}/${BUILDJSDIR}/build-${binary}.sh
    mkdir -p ${ROOTDIR}/${INSTALLDIR}
    cd ${ROOTDIR}/${BUILDJSDIR}/src
    source ${ROOTDIR}/${BUILDJSDIR}/build-${binary}.sh

    if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${dest}-bare.js ]; then
      echo " - ERROR: cannot make JavaScript ${dest}"
      exit 1
    fi

    # Creates the worker itself
    cat ${ROOTDIR}/assets/js/browserfs.min.js ${ROOTDIR}/lib/pre-worker.js ${ROOTDIR}/${INSTALLDIR}/${dest}-bare.js ${ROOTDIR}/lib/post-worker.js > ${ROOTDIR}/${INSTALLDIR}/${dest}.js
  else
    echo " - creating ${INSTALLDIR}/${dest}.js (exists already)"
  fi
done

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
exit
