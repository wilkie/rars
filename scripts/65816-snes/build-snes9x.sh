#!/bin/bash

HEADER="Building SNES9x"
TARGET=65816-snes
PACKAGE=snes9x

source $PWD/scripts/common/include.sh

PATCHDIR=${ROOTDIR}/patches/${TARGET}/${PACKAGE}
UTILSDIR=utils/${TARGET}

SRCDIR=${ROOTDIR}/packages/${TARGET}/${PACKAGE}
BUILDLIBRETRODIR=packages/${TARGET}/${PACKAGE}-libretro

INSTALLDIR=assets/js/targets/${TARGET}/${PACKAGE}

echo " - installing/updating packages"
mkdir -p ${ROOTDIR}/packages/${TARGET}
./scripts/${TARGET}/install.sh &> ${ROOTDIR}/packages/${TARGET}/install-during-${PACKAGE}.log

# Ensure build path exists
if [ ! -d ${ROOTDIR}/${BUILDLIBRETRODIR} ]; then
  echo " - copying the ${BUILDLIBRETRODIR} directory"
  cp -r ${SRCDIR} ${ROOTDIR}/${BUILDLIBRETRODIR}
else
  echo " - warning: using existing build directory at ${BUILDLIBRETRODIR}"
fi

# Create a libretro core
if [ ! -f ${ROOTDIR}/${BUILDLIBRETRODIR}/libretro/snes9x_libretro.wasm ]; then
  cd ${ROOTDIR}/${BUILDLIBRETRODIR}

  echo " - patching libretro core"
  if [ ! -f ${ROOTDIR}/${BUILDLIBRETRODIR}/patching.log ]; then
    # build the lto plugin
    patch -s -Np1 < ${PATCHDIR}/libretro.patch &> ${ROOTDIR}/${BUILDLIBRETRODIR}/patching.log

    # copy the files into the path
    cp ${PATCHDIR}/prelim.* ${ROOTDIR}/${BUILDLIBRETRODIR}/libretro/.
    cp ${PATCHDIR}/rawrs.* ${ROOTDIR}/${BUILDLIBRETRODIR}/libretro/.
  else
    echo " - patches already believed to be applied (remove patching.log to reapply)"
  fi

  cd ${ROOTDIR}/${BUILDLIBRETRODIR}/libretro

  if [ ! ${EMSDK} ]; then
    echo " - initializing emcripten"
    source "$ROOTDIR/packages/emsdk/emsdk_env.sh" &> ${ROOTDIR}/${BUILDLIBRETRODIR}/0-libretro-emscripten-initialize.log
  else
    echo " - using existing activated emcripten at ${EMSDK}"
  fi

  echo " - building a JavaScript SNES9x libretro core"
  run "emmake make" "${ROOTDIR}/${BUILDLIBRETRODIR}/1-libretro-make.log"
else
  echo " - using existing built JavaScript SNES9x libretro core"
fi

# Create the worker from the libretro core
if [ ! -f ${ROOTDIR}/${BUILDLIBRETRODIR}/libretro/snes9x_libretro-worker.js ]; then
  echo " - building a JavaScript SNES9x libretro core worker"

  cd ${ROOTDIR}/${BUILDLIBRETRODIR}/libretro

  cat ${PATCHDIR}/prelim.js ${ROOTDIR}/lib/pre-worker.js ${ROOTDIR}/${BUILDLIBRETRODIR}/libretro/snes9x_libretro.js ${ROOTDIR}/lib/post-worker.js > ${ROOTDIR}/${BUILDLIBRETRODIR}/libretro/snes9x_libretro-worker.js
else
  echo " - using existing built JavaScript SNES9x libretro core worker"
fi

if [ ! -f ${ROOTDIR}/${BUILDLIBRETRODIR}/libretro/snes9x_libretro-worker.js ]; then
  echo " - ERROR: could not make a JavaScript SNES9x libretro core worker"
  exit 1
fi

cd ${ROOTDIR}/${BUILDLIBRETRODIR}/libretro

if [ ! -d ${ROOTDIR}/${INSTALLDIR} ]; then
  echo " - creating ${INSTALLDIR} for snes9x workers"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
fi

if [ ! -f ${ROOTDIR}/${INSTALLDIR}/snes9x_libretro-worker.js ]; then
  echo " - creating ${INSTALLDIR}/snes9x_libretro-worker.js"
  cp snes9x_libretro-worker.js ${ROOTDIR}/${INSTALLDIR}/snes9x_libretro-worker.js
else
  echo " - creating ${INSTALLDIR}/snes9x_libretro-worker.js (exists already)"
fi

if [ ! -f ${ROOTDIR}/${INSTALLDIR}/snes9x_libretro.wasm ]; then
  echo " - creating ${INSTALLDIR}/snes9x_libretro.wasm"
  cp snes9x_libretro.wasm ${ROOTDIR}/${INSTALLDIR}/snes9x_libretro.wasm
else
  echo " - creating ${INSTALLDIR}/snes9x_libretro.wasm (exists already)"
fi

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
