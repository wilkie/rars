#!/bin/bash

echo ""
echo "Installing Super Nintendo Target"
echo "================================"
echo ""

ROOTDIR=$PWD
COMMONDIR=packages/common
INSTALLDIR=packages/65816-snes

if [ ! -f ${ROOTDIR}/scripts/65816-snes/versions.sh ]; then
  echo " - ERROR: need to run this script from the rawrs project directory"
  echo ""
  echo "Failed."
  exit 1
fi

echo " - checking prerequisites"

CHECK="texi2pdf bison flex gcc make"
good=1

for binary in ${CHECK}
do
  if ! hash ${binary} 2>/dev/null; then
    echo " - ERROR: must install ${binary}"
    good=0
  fi
done

if [ ${good} -eq 0 ]; then
  echo " - ERROR: prerequisites needed"
  echo ""
  echo "Failed."
  exit 1
fi

echo " - all prerequisites found"
echo ""

source $PWD/scripts/65816-snes/versions.sh

echo "1. CC65"
echo "-------"
echo ""

if [ ! -d ${ROOTDIR}/${INSTALLDIR}/cc65 ]; then
  echo " - downloading cc65 from git sources"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
  cd ${ROOTDIR}/${INSTALLDIR}
  git clone ${CC65_URL}
  cd ../..
else
  echo " - cc65 source repository already exists"
fi

cd ${ROOTDIR}/${INSTALLDIR}/cc65
if ! git cat-file -e ${CC65_VERSION}; then
  echo " - fetching updates to cc65"
  git pull
else
  echo " - revision ${CC65_VERSION} found"
fi

echo ""
echo "2. SNES SDK"
echo "-----------"
echo ""

if [ ! -d ${ROOTDIR}/${INSTALLDIR}/snes-sdk ]; then
  echo " - downloading snes-sdk from git sources"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
  cd ${ROOTDIR}/${INSTALLDIR}
  git clone ${SNES_SDK_URL}
  cd ../..
else
  echo " - snes-sdk source repository already exists"
fi

cd ${ROOTDIR}/${INSTALLDIR}/snes-sdk
if ! git cat-file -e ${SNES_SDK_VERSION}; then
  echo " - fetching updates to snes-sdk"
  git pull
else
  echo " - revision ${SNES_SDK_VERSION} found"
fi

echo ""
echo "3. SNES9x"
echo "---------"
echo ""

if [ ! -d ${ROOTDIR}/${INSTALLDIR}/snes9x ]; then
  echo " - downloading snes9x from git sources"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
  cd ${ROOTDIR}/${INSTALLDIR}
  git clone ${SNES9X_URL}
  cd ../..
else
  echo " - snes9x source repository already exists"
fi

cd ${ROOTDIR}/${INSTALLDIR}/snes9x
if ! git cat-file -e ${SNES9X_VERSION}; then
  echo " - fetching updates to snes9x"
  git pull
else
  echo " - revision ${SNES9X_VERSION} found"
fi

echo ""
echo "4. Classic Kong"
echo "---------------"
echo ""

if [ ! -d ${ROOTDIR}/${INSTALLDIR}/classickong ]; then
  echo " - downloading classickong from git sources"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
  cd ${ROOTDIR}/${INSTALLDIR}
  git clone ${CLASSIC_KONG_URL}
  cd ../..
else
  echo " - snes-sdk source repository already exists"
fi

cd ${ROOTDIR}/${INSTALLDIR}/classickong
if ! git cat-file -e ${CLASSIC_KONG_VERSION}; then
  echo " - fetching updates to classickong"
  git pull
else
  echo " - revision ${CLASSIC_KONG_VERSION} found"
fi

echo ""
echo "Done."
