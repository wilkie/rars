#!/bin/bash

if [ ! ${ISL_VERSION} ]; then
  ISL_VERSION=0.18
fi
ISL_URL=https://gcc.gnu.org/pub/gcc/infrastructure
ISL_FILENAME=isl-${ISL_VERSION}.tar.bz2

# Use INSTALLDIR to pick the destination path to install isl
if [ ! ${INSTALLDIR} ]; then
  INSTALLDIR=packages/system
fi

ROOTDIR=$PWD

if [ ! ${QUIET} ]; then
  echo ""
  if [ ${RAWRS_COUNTER} ]; then
    echo "${RAWRS_COUNTER}. Building ISL library"
    echo "-----------------------"
  else
    echo "Building ISL library"
    echo "===================="
  fi
  echo ""
fi

if [ ! -f ${ROOTDIR}/scripts/common/build-isl.sh ]; then
  echo " - ERROR: need to run this script from the rawrs project directory"
  exit 1
fi

PACKAGEDIR=packages/common
BUILDJSDIR=packages/common/isl-${ISL_VERSION}-js
SRCDIR=${ROOTDIR}/${PACKAGEDIR}/isl-${ISL_VERSION}

if [ ! -d ${ROOTDIR}/${PACKAGEDIR} ]; then
  echo " - creating ${PACKAGEDIR}"
  mkdir -p ${ROOTDIR}/${PACKAGEDIR}
fi

# Install
if [ ! -d ${ROOTDIR}/${PACKAGEDIR}/isl-${ISL_VERSION} ]; then
  echo " - downloading isl ${ISL_VERSION}"
  wget -q -nc ${ISL_URL}/${ISL_FILENAME} -O ${ROOTDIR}/${PACKAGEDIR}/${ISL_FILENAME}
  cd ${ROOTDIR}/${PACKAGEDIR}
  tar xf ${ISL_FILENAME}
  cd ${ROOTDIR}
fi

# Do not build if it exists
if [ ! -f ${ROOTDIR}/${BUILDJSDIR}/.libs/libisl.a ]; then
  if [ ! -d ${ROOTDIR}/${BUILDJSDIR} ]; then
    echo " - creating ${BUILDJSDIR} directory"
    mkdir -p ${ROOTDIR}/${BUILDJSDIR}
  else
    echo " - warning: using existing build directory at ${BUILDJSDIR}"
  fi

  # Activate our emscripten environment
  if [ ! ${EMSDK} ]; then
    echo " - initializing emcripten"
    source "$ROOTDIR/emsdk/emsdk_env.sh" &> ${ROOTDIR}/${BUILDJSDIR}/0-emscripten-initialize.log
  else
    echo " - using existing activated emcripten at ${EMSDK}"
  fi
  
  cd ${ROOTDIR}/${BUILDJSDIR}

  echo " - configuring JavaScript isl ${ISL_VERSION}"
  # We need to set _GNU_SOURCE so that emscripten's strings.h defines 'ffs' for configuration
  CFLAGS="-D_GNU_SOURCE=1" emconfigure ${SRCDIR}/configure --prefix=/ \
    --disable-shared \
    --enable-static \
    --host none \
    --with-gmp-prefix=${ROOTDIR}/${INSTALLDIR} &> ${ROOTDIR}/${BUILDJSDIR}/1-configure.log

  # The different flags we need to compile
  CFLAGS="-DHAVE_PSIGNAL -m32 -s USE_PTHREADS=0" 
  CPPFLAGS="-DHAVE_PSIGNAL -m32 -s USE_PTHREADS=0" 
  LDFLAGS="-Wl,--no-check-features -s ALLOW_MEMORY_GROWTH=1 -s USE_PTHREADS=0"

  echo " - building JavaScript isl ${ISL_VERSION}"
  emmake make CXX=emcc CFLAGS="${CFLAGS}" CPPFLAGS="${CPPFLAGS}" LDFLAGS="${LDFLAGS}" PTHREAD_CFLAGS="" &> ${ROOTDIR}/${BUILDJSDIR}/2-make.log
else
  echo " - using existing built JavaScript ISL library"
fi

if [ ! -d ${ROOTDIR}/${INSTALLDIR} ]; then
  echo " - creating ${INSTALLDIR}"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
fi

if [ ! -f ${ROOTDIR}/${INSTALLDIR}/lib/libisl.a ]; then
  emmake make install DESTDIR=${ROOTDIR}/${INSTALLDIR} &> ${ROOTDIR}/${BUILDJSDIR}/3-make-install.log
else
  echo " - using existing installed JavaScript ISL library"
fi

echo " - installed to ${INSTALLDIR}"

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
