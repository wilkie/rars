#!/bin/bash

HEADER="Building MS-DOS binutils"
TARGET=x86-msdos
PACKAGE=binutils

# The host target we are building
HOST="i586-pc-msdosdjgpp"

source $PWD/scripts/common/include.sh

SRCDIR=$PWD/packages/x86-msdos/binutils-${BINUTILS_VERSION}
BUILDDIR=packages/x86-msdos/binutils-${BINUTILS_VERSION}-build
BUILDJSDIR=packages/x86-msdos/binutils-${BINUTILS_VERSION}-js

INSTALLDIR=assets/js/targets/x86-msdos/binutils

# Compile options

PROFILING_OPTS="-O2"

# Uncomment to gain stack traces
#PROFILING_OPTS="${PROFILING_OPTS} -g --profiling-funcs"

CFLAGS="-DHAVE_PSIGNAL -m32 ${PROFILING_OPTS}"

echo " - installing/updating MS-DOS packages"
mkdir -p ${ROOTDIR}/packages/x86-msdos
./scripts/x86-msdos/install.sh &> ${ROOTDIR}/packages/x86-msdos/install-during-binutils.log

echo " - patching binutils"

cd ${SRCDIR}

# Apply DJGPP related patches from
# ftp://ftp.delorie.com/pub/djgpp/current/v2gnu/bnu234s.zip
if [ ! -f ${SRCDIR}/patching.log ]; then
  patch -s -Np1 < ${PATCHDIR}/binutils-djgpp.patch &> ${SRCDIR}/patching.log
  patch -s -Np1 < ${PATCHDIR}/binutils-bfd-djgpp.patch &>> ${SRCDIR}/patching.log
  patch -s -Np2 < ${PATCHDIR}/lto-discard.patch &>> ${SRCDIR}/patching.log
else
  echo " - patches already believed to be applied (remove patching.log to reapply)"
fi

if [ ! -d ${ROOTDIR}/${BUILDDIR} ]; then
  echo " - creating ${BUILDDIR} directory"
  mkdir -p ${ROOTDIR}/${BUILDDIR}
else
  echo " - warning: using existing build directory at ${BUILDDIR}"
fi

if [ ! -f ${ROOTDIR}/${UTILSDIR}/usr/bin/${HOST}-as ]; then
  echo " - configuring a native binutils for MS-DOS targets"
  cd ${ROOTDIR}/${BUILDDIR}
  ${SRCDIR}/configure --prefix=/usr \
    --target=$HOST \
    --infodir="/usr/share/info/$HOST" \
    --datadir="/usr/$HOST/share" \
    --enable-lto --disable-nls \
    --disable-install-libiberty \
    --disable-multilib --disable-nls \
    --disable-werror &> ${ROOTDIR}/${BUILDDIR}/native-configure.log

  echo " - building a native binutils for MS-DOS targets"
  run "make" "${ROOTDIR}/${BUILDDIR}/native-make.log"

  # Install
  if [ ! -d ${ROOTDIR}/${UTILSDIR} ]; then
    echo " - creating ${UTILSDIR} to install native binutils"
    mkdir -p ${ROOTDIR}/$UTILSDIR
  fi

  echo " - installing native binutils for MS-DOS targets"
  make -C ${ROOTDIR}/${BUILDDIR} DESTDIR=${ROOTDIR}/${UTILSDIR} install &> ${ROOTDIR}/${BUILDDIR}/native-make-install.log
else
  echo " - using existing installed native binutils for MS-DOS targets"
fi

echo " - native binutils installed to ${UTILSDIR}"

if [ ! -d ${ROOTDIR}/${BUILDJSDIR} ]; then
  echo " - creating ${BUILDJSDIR} directory"
  mkdir -p ${ROOTDIR}/${BUILDJSDIR}
else
  echo " - warning: using existing build directory at ${BUILDJSDIR}"
fi

if [ ! ${EMSDK} ]; then
  echo " - initializing emcripten"
  source "$ROOTDIR/packages/emsdk/emsdk_env.sh" &> ${ROOTDIR}/${BUILDJSDIR}/0-emscripten-initialize.log
else
  echo " - using existing activated emcripten at ${EMSDK}"
fi

cd ${ROOTDIR}/${BUILDJSDIR}

if [ ! -f ${ROOTDIR}/${BUILDJSDIR}/gas/as.o ]; then
  echo " - configuring a JavaScript binutils for MS-DOS targets"
  # TODO: we can re-enable lto if we can support loading it via dlopen by
  #       compiling it with certain options or compiling it into the binary
  #       somehow.
  emconfigure ${SRCDIR}/configure --prefix=/usr \
    --target=$HOST \
    --infodir="/usr/share/info/$HOST" \
    --datadir="/usr/$HOST/share" \
    --disable-lto --disable-nls \
    --disable-install-libiberty \
    --disable-multilib --disable-nls \
    --disable-werror &> ${ROOTDIR}/${BUILDJSDIR}/1-configure.log

  echo " - copying over native generators needed during compilation"

  COPIES="bfd/doc/chew bfd/doc/chew.stamp binutils/sysinfo"

  for fixup in ${COPIES}
  do
    rm -f ${ROOTDIR}/${BUILDJSDIR}/${fixup}
    mkdir -p ${ROOTDIR}/${BUILDJSDIR}/$(dirname "${fixup}")
    cp --preserve=mode ${ROOTDIR}/${BUILDDIR}/${fixup} ${ROOTDIR}/${BUILDJSDIR}/${fixup}
    chmod +x ${ROOTDIR}/${BUILDJSDIR}/${fixup}
    touch -d "+24 hour" ${ROOTDIR}/${BUILDJSDIR}/${fixup}
  done

  echo " - building a JavaScript binutils for MS-DOS targets"
  run "emmake make all-ld CFLAGS=\"${CFLAGS}\"" "${ROOTDIR}/${BUILDJSDIR}/2-make.log"
else
  echo " - using existing built JavaScript binutils for MS-DOS targets"
fi

cd $ROOTDIR

if [ ! -d ${ROOTDIR}/${INSTALLDIR} ]; then
  echo " - creating ${INSTALLDIR} for JavaScript workers"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
else
  echo " - installing to existing ${INSTALLDIR} for JavaScript workers"
fi

EMSCRIPTEN_WASM_OPTS="-s WASM=1 -s NO_EXIT_RUNTIME=0 -lworkerfs.js -lfs.js -s FORCE_FILESYSTEM=1 -s ASSERTIONS=1 ${PROFILING_OPTS}"

BINARIES="gas/as-new ld/ld-new binutils/objdump binutils/readelf"

for binary in ${BINARIES}
do
  dest=$(basename ${binary})
  if [[ ${dest} == *-new ]]; then
    dest=`echo ${dest} | cut -c1-2`
  fi

  subpath=$(dirname ${binary})
  binary=$(basename ${binary})

  if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${HOST}-${dest}.js ]; then
    echo " - creating ${INSTALLDIR}/${HOST}-${dest}.js"
    cd ${ROOTDIR}/${BUILDJSDIR}/${subpath}
    rm -f ${binary}
    rm -f ${binary}.wasm
    run "emmake make ${binary}" "${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log"
    context=0
    while [[ `cat ${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log | grep "libtool: link:.\+-o ${binary} " -A${context}` == *\\ ]]; do
      context=$[$context + 1]
    done

    # Creates *-bare.js
    COMMAND=`cat ${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log | grep "libtool: link:.\+-o ${binary} " -A${context} | sed "s;libtool: link: ;;" | sed "s;-o ${binary};${EMSCRIPTEN_WASM_OPTS} -o ${ROOTDIR}/${INSTALLDIR}/${HOST}-${dest}-bare.js;"`
    echo ${COMMAND} > ${ROOTDIR}/${BUILDJSDIR}/build-${binary}.sh
    chmod +x ${ROOTDIR}/${BUILDJSDIR}/build-${binary}.sh
    source ${ROOTDIR}/${BUILDJSDIR}/build-${binary}.sh

    if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${HOST}-${dest}-bare.js ]; then
      echo " - ERROR: cannot make JavaScript ${dest}"
      exit 1
    fi

    # Creates the worker itself
    cat ${ROOTDIR}/assets/js/browserfs.min.js ${ROOTDIR}/lib/pre-worker.js ${ROOTDIR}/${INSTALLDIR}/${HOST}-${dest}-bare.js ${ROOTDIR}/lib/post-worker.js > ${ROOTDIR}/${INSTALLDIR}/${HOST}-${dest}.js
  else
    echo " - creating ${INSTALLDIR}/${HOST}-${dest}.js (exists already)"
  fi
done

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
