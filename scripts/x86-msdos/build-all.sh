#!/bin/bash

ROOTDIR=$PWD

echo ""
echo "Building MS-DOS Target"
echo "======================"

if [ ! -f ${ROOTDIR}/scripts/x86-msdos/versions.sh ]; then
  echo ""
  echo " - ERROR: need to run this script from the rawrs project directory"
  exit 1
fi

RAWRS_COUNTER=1 ./scripts/x86-msdos/build-binutils.sh
RAWRS_COUNTER=2 ./scripts/x86-msdos/build-gcc.sh
RAWRS_COUNTER=3 ./scripts/x86-msdos/build-djgpp-djcrx.sh
RAWRS_COUNTER=4 ./scripts/x86-msdos/build-dosbox.sh
RAWRS_COUNTER=5 ./scripts/x86-msdos/build-examples.sh

echo ""
