#!/bin/bash

HEADER="Building RISC-V gcc for Linux"
TARGET=riscv64-linux
PACKAGE=gcc

# The host target we are building
HOST="riscv64-unknown-linux-gnu"

source $PWD/scripts/common/include.sh

PATCHBAREDIR=$PWD/patches/riscv64/gcc
SRCDIR=$PWD/packages/riscv64/riscv-gnu-toolchain/riscv-gcc
BUILDDIR=packages/riscv64-linux/riscv-gcc-build
BUILDSTAGE1DIR=packages/riscv64-linux/riscv-gcc-bootstrap-build
BUILDJSDIR=packages/riscv64-linux/riscv-gcc-js

INSTALLDIR=assets/js/targets/riscv64-linux/gcc
LIBINSTALLDIR=assets/static/riscv64-linux

echo " - installing/updating RISC-V Linux packages"
./scripts/riscv64-linux/install.sh &> ${ROOTDIR}/packages/riscv64-linux/install-during-gcc.log

# Ensure gmp/mpfr/mpc/isl/zlib are built
SYSTEMDIR=packages/riscv64/system
if [ ! -f ${ROOTDIR}/${SYSTEMDIR}/lib/libgmp.a ]; then
  INSTALLDIR=${SYSTEMDIR} QUIET=1 ./scripts/common/build-gmp.sh
fi
if [ ! -f ${ROOTDIR}/${SYSTEMDIR}/lib/libmpfr.a ]; then
  INSTALLDIR=${SYSTEMDIR} QUIET=1 ./scripts/common/build-mpfr.sh
fi
if [ ! -f ${ROOTDIR}/${SYSTEMDIR}/lib/libmpc.a ]; then
  INSTALLDIR=${SYSTEMDIR} QUIET=1 ./scripts/common/build-mpc.sh
fi
if [ ! -f ${ROOTDIR}/${SYSTEMDIR}/lib/libisl.a ]; then
  INSTALLDIR=${SYSTEMDIR} QUIET=1 ./scripts/common/build-isl.sh
fi
if [ ! -f ${ROOTDIR}/${SYSTEMDIR}/lib/libz.so ]; then
  INSTALLDIR=${SYSTEMDIR} QUIET=1 ./scripts/common/build-zlib.sh
fi

# Create build path for building native risc-v gcc (stage 1)
if [ ! -d ${ROOTDIR}/${BUILDSTAGE1DIR} ]; then
  echo " - creating ${BUILDSTAGE1DIR} directory"
  mkdir -p ${ROOTDIR}/${BUILDSTAGE1DIR}
else
  echo " - warning: using existing build directory at ${BUILDSTAGE1DIR}"
fi

# Compile native risc-v gcc (stage 1)
# If newlib is not built, we need a compiler to build it... so build a stage 1
if [ ! -f ${ROOTDIR}/${BUILDSTAGE1DIR}/gcc/gcc.o ]; then
  cd ${ROOTDIR}/${BUILDSTAGE1DIR}

  # Add binutils to the path for this build
  export PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH

  echo " - configuring a native gcc (stage 1) for RISC-V Linux targets"
  ${SRCDIR}/configure --prefix ${ROOTDIR}/${UTILSDIR}/gcc-linux-bootstrap --target=${HOST} \
    --with-newlib \
		--with-sysroot=${ROOTDIR}/${UTILSDIR} \
    --without-headers \
    --enable-languages=c \
    --disable-multilib \
    --disable-plugin \
    --disable-libsanitizer \
    --disable-shared \
    --enable-static \
    --enable-threads=no \
    --with-system-zlib \
    --enable-lto \
    --disable-libgomp \
    --enable-checking=release \
    --disable-libstdcxx-pch \
    --enable-libstdcxx-filesystem-ts \
    --disable-libatomic \
    --disable-libmudflap \
    --disable-libssp \
    --disable-libquadmath \
    --disable-libgomp \
    --disable-nls \
    --disable-bootstrap &> ${ROOTDIR}/${BUILDSTAGE1DIR}/0-configure-stage1.log

  echo " - building a native gcc (stage 1) for RISC-V Linux targets"
  run "make inhibit-libc=true all-gcc" "${ROOTDIR}/${BUILDSTAGE1DIR}/1-make-stage1.log"
else
  echo " - using existing build of RISC-V Linux gcc IN ${BUILDSTAGE1DIR}"
fi
  # Add binutils to the path for this build
  export PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH

  cd ${ROOTDIR}/${BUILDSTAGE1DIR}
  echo " - building a native libgcc (stage 1) for RISC-V Linux targets"
  run "make inhibit-libc=true all-target-libgcc" "${ROOTDIR}/${BUILDSTAGE1DIR}/2-make-stage1-libgcc.log"

if [ ! -f ${ROOTDIR}/${UTILSDIR}/gcc-linux-bootstrap/bin/${HOST}-gcc ]; then
  # Add binutils to the path for this build
  export PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH

  echo " - installing a native gcc (stage 1) for RISC-V Linux targets"
  cd ${ROOTDIR}/${BUILDSTAGE1DIR}
  run "make inhibit-libc=true install-gcc" "${ROOTDIR}/${BUILDSTAGE1DIR}/3-install-stage1.log"
else
  echo " - using existing installed native gcc (stage 1) for RISC-V Linux targets"
fi

if [ ! -f ${ROOTDIR}/${UTILSDIR}/gcc-linux-bootstrap/bin/${HOST}-gcc ]; then
  echo " - ERROR: did not build and/or install a bootstrap gcc for RISC-V Linux targets"
  exit 1
fi

# Add binutils to the path for this build
export PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH

if [ ! -f ${ROOTDIR}/${UTILSDIR}/gcc-linux-bootstrap/lib/gcc/${HOST}/10.2.0/libgcc.a ]; then
  echo " - installing a native libgcc (stage 1) for RISC-V Linux targets"
  cd ${ROOTDIR}/${BUILDSTAGE1DIR}
  run "make inhibit-libc=true install-target-libgcc" "${ROOTDIR}/${BUILDSTAGE1DIR}/4-install-stage1-libgcc.log"
else
  echo " - using existing installed native libgcc (stage 1) for RISC-V Linux targets"
fi

if [ ! -f ${ROOTDIR}/${UTILSDIR}/gcc-linux-bootstrap/lib/gcc/${HOST}/10.2.0/libgcc.a ]; then
  echo " - ERROR: did not build and/or install a bootstrap libgcc for RISC-V Linux targets"
  exit 1
fi

if [ ! -f ${ROOTDIR}/${UTILSDIR}/gcc-linux-bootstrap/${HOST}/bin/as ]; then
  echo " - creating a symlink to the built RISC-V Linux binutils"
  ln -fs ${ROOTDIR}/${UTILSDIR}/usr/${HOST} ${ROOTDIR}/${UTILSDIR}/gcc-linux-bootstrap/${HOST}
else
  echo " - using existing symlink to the built RISC-V Linux binutils"
fi

if [ ! -f ${ROOTDIR}/${UTILSDIR}/usr/include/asm/ioctl.h ]; then
  # With our stage 1 compiler (or whatever exists) build Linux headers
  cd ${ROOTDIR}
  QUIET=1 PATH=${ROOTDIR}/${UTILSDIR}/gcc-linux-bootstrap/bin:$PATH ./scripts/riscv64-linux/build-headers.sh
else
  echo " - using existing Linux headers found in ${UTILSDIR}/usr/include"
fi

if [ ! -f ${ROOTDIR}/${UTILSDIR}/usr/lib/crt1.o ]; then
  # With our stage 1 compiler (or whatever exists) build glibc
  cd ${ROOTDIR}
  QUIET=1 PATH=${ROOTDIR}/${UTILSDIR}/gcc-linux-bootstrap/bin:$PATH ./scripts/riscv64-linux/build-glibc.sh
fi

if [ ! -f ${ROOTDIR}/${UTILSDIR}/usr/lib/crt1.o ]; then
  echo "Cannot find a proper libc. Did glibc not build correctly?"
  exit 1
fi

# Create build path for building native risc-v gcc
if [ ! -d ${ROOTDIR}/${BUILDDIR} ]; then
  echo " - creating ${BUILDDIR} directory"
  mkdir -p ${ROOTDIR}/${BUILDDIR}
else
  echo " - warning: using existing build directory at ${BUILDDIR}"
fi

# Now we build our real compiler
if [ ! -f ${ROOTDIR}/${BUILDDIR}/gcc/gcc.o ]; then
  cd ${ROOTDIR}/${BUILDDIR}

  # Add binutils to the path for this build
  export PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH

  # We configure *with* glibc as the system root
  echo " - configuring a native gcc for RISC-V Linux targets"
  ${SRCDIR}/configure --prefix /usr --target=${HOST} \
		--with-sysroot=${ROOTDIR}/${UTILSDIR} \
    --enable-languages=c,c++ \
    --disable-multilib \
    --disable-plugin \
    --disable-libsanitizer \
    --enable-shared \
    --enable-static \
    --enable-threads=no \
    --with-system-zlib \
    --enable-lto \
    --disable-libgomp \
    --enable-checking=release \
    --disable-libstdcxx-pch \
    --enable-libstdcxx-filesystem-ts \
		--disable-libatomic \
		--disable-libmudflap \
		--disable-libssp \
		--disable-libquadmath \
		--disable-libgomp \
		--disable-nls \
		--disable-bootstrap &> ${ROOTDIR}/${BUILDDIR}/0-configure.log

  echo " - building a native gcc for RISC-V Linux targets"
  run "make" "${ROOTDIR}/${BUILDDIR}/1-make.log"
else
  echo " - using existing native build of RISC-V Linux gcc in ${BUILDDIR}"
fi

# Install native risc-v gcc
if [ ! -f ${ROOTDIR}/${UTILSDIR}/usr/bin/${HOST}-gcc ]; then
  echo " - installing native build of RISC-V Linux gcc to ${UTILSDIR}"

  # Add binutils to the path for this build
  export PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH

  cd ${ROOTDIR}/${BUILDDIR}
  run "make install DESTDIR=${ROOTDIR}/${UTILSDIR}" "${ROOTDIR}/${BUILDDIR}/2-make-install.log"
  cd ${ROOTDIR}
else
  echo " - using existing installed native gcc for RISC-V Linux targets"
fi

echo " - native gcc installed to ${UTILSDIR}"

# Install libgcc and friends
if [ ! -f ${ROOTDIR}/${LIBINSTALLDIR}/usr/lib/gcc/${HOST}/10.2.0/libgcc.a ]; then
  echo " - copying gcc runtime libraries to ${LIBINSTALLDIR}"
  rm -rf ${ROOTDIR}/${LIBINSTALLDIR}/usr/lib/gcc
  mkdir -p ${ROOTDIR}/${LIBINSTALLDIR}/usr/lib
  cp -r ${ROOTDIR}/${UTILSDIR}/usr/lib/gcc ${ROOTDIR}/${LIBINSTALLDIR}/usr/lib/gcc
  cp ${ROOTDIR}/${UTILSDIR}/usr/lib/gcc/riscv64-unknown-linux-gnu/10.2.0/include/* ${ROOTDIR}/${LIBINSTALLDIR}/usr/include/.

  if [ ! -f ${ROOTDIR}/${LIBINSTALLDIR}/usr/lib/gcc/${HOST}/10.2.0/libgcc.a ]; then
    echo " - ERROR: could not install gcc libraries to ${LIBINSTALLDIR}"
    exit 1
  fi
fi

rm -rf ${ROOTDIR}/${LIBINSTALLDIR}/usr/bin/*

echo " - RISC-V Linux libgcc installed to ${LIBINSTALLDIR}"

# Build our JavaScript compiler now

PROFILING_OPTS=
# Uncomment to gain stack traces
#PROFILING_OPTS=-g --profiling-funcs

# The different flags we need to compile
export CFLAGS="-DHAVE_PSIGNAL -DHAVE_STRSIGNAL=1 -DHAVE_DECL_SBRK=1 -DHAVE_DECL_STRSIGNAL=1 -DHAVE_SYS_RESOURCE_H=1 -DHAVE_SYS_TIMES_H=1 -DHAVE_STDLIB_H=1 -DHAVE_UNISTD_H=1 -DHAVE_STRING_H=1 -DHAVE_SYS_STAT_H=1 -DHAVE_FCNTL_H=1 -m32 ${PROFILING_OPTS} -s USE_PTHREADS=0"
export CPPFLAGS=$CFLAGS
export CXXFLAGS=$CFLAGS
export LDFLAGS="-Wl,--no-check-features -s ALLOW_MEMORY_GROWTH=1 -s USE_PTHREADS=0 -L${ROOTDIR}/${SYSTEMDIR}/lib ${ROOTDIR}/${SYSTEMDIR}/lib/libgmp.a ${ROOTDIR}/${SYSTEMDIR}/lib/libmpfr.a ${ROOTDIR}/${SYSTEMDIR}/lib/libmpc.a"
export CPLUS_INCLUDE_PATH="${ROOTDIR}/${SYSTEMDIR}/include"

# Create build path for building JavaScript risc-v gcc
if [ ! -d ${ROOTDIR}/${BUILDJSDIR} ]; then
  echo " - creating ${BUILDJSDIR} directory"
  mkdir -p ${ROOTDIR}/${BUILDJSDIR}
else
  echo " - warning: using existing build directory at ${BUILDJSDIR}"
fi

if [ ! -f ${ROOTDIR}/${BUILDJSDIR}/gcc/gcc.o ]; then
  # Initialize emscripten
  if [ ! ${EMSDK} ]; then
    echo " - initializing emcripten"
    source "$ROOTDIR/packages/emsdk/emsdk_env.sh" &> ${ROOTDIR}/${BUILDJSDIR}/0-emscripten-initialize.log
  else
    echo " - using existing activated emcripten at ${EMSDK}"
  fi

  # It is important to nuke ftw.h (and other things) so that gcc does not use it
  EMFIXES="include/ftw.h include/sys/prctl.h"

  cd ${ROOTDIR}/${BUILDJSDIR}
  for nukeheader in ${EMFIXES}; do
    if [ -f ${ROOTDIR}/packages/emsdk/upstream/emscripten/cache/sysroot/${nukeheader} ]; then
      echo " - nuking emscripten's system ${nukeheader} header file"
      cp ${ROOTDIR}/packages/emsdk/upstream/emscripten/cache/sysroot/${nukeheader} $(basename ${nukeheader})
      rm ${ROOTDIR}/packages/emsdk/upstream/emscripten/cache/sysroot/${nukeheader}
    fi
  done

  # Configure
  cd ${ROOTDIR}/${BUILDJSDIR}
  echo " - configuring a JavaScript gcc for RISC-V Linux targets"
  emconfigure ${SRCDIR}/configure --target=${HOST} \
      --prefix=/usr \
      --with-sysroot=${ROOTDIR}/${UTILSDIR} \
      --with-gmp=${ROOTDIR}/${SYSTEMDIR} \
      --with-mpfr=${ROOTDIR}/${SYSTEMDIR} \
      --with-mpc=${ROOTDIR}/${SYSTEMDIR} \
      --with-isl=${ROOTDIR}/${SYSTEMDIR} \
      --with-zlib=${ROOTDIR}/${SYSTEMDIR} \
      --without-headers \
      --enable-languages=c,c++ \
      --disable-multilib \
      --disable-plugin \
      --disable-libsanitizer \
      --disable-shared \
      --enable-static \
      --enable-threads=no \
      --disable-threads \
      --enable-lto \
      --disable-libstdcxx-pch \
      --disable-checking \
      --enable-libstdcxx-filesystem-ts \
      --disable-libatomic \
      --disable-libmudflap \
      --disable-libssp \
      --disable-libquadmath \
      --disable-libgomp \
      --disable-nls \
      --disable-bootstrap \
      --disable-install-libiberty &> ${ROOTDIR}/${BUILDJSDIR}/1-configure.log

  echo " - building (initial pass... will fail) a JavaScript gcc for risc-v targets"
  run "emmake make CXX=em++ CFLAGS=\"${CFLAGS}\" CPPFLAGS=\"${CPPFLAGS}\" LDFLAGS=\"${LDFLAGS}\" PTHREAD_CFLAGS=\"\"" "${ROOTDIR}/${BUILDJSDIR}/2-make.log"

  echo " - configuring JavaScript ./gcc path to prepare for the second pass"
  mkdir -p gcc
  cd gcc
  emconfigure ${SRCDIR}/gcc/configure --srcdir=${SRCDIR}/gcc --cache-file=./config.cache --prefix=/usr --with-sysroot=${ROOTDIR}/${UTILSDIR} --with-gmp=${ROOTDIR}/${SYSTEMDIR} --with-mpfr=${ROOTDIR}/${SYSTEMDIR} --with-mpc=${ROOTDIR}/${SYSTEMDIR} --with-isl=${ROOTDIR}/${SYSTEMDIR} --with-zlib=${ROOTDIR}/${SYSTEMDIR} --without-headers --disable-multilib --disable-plugin --disable-libsanitizer --disable-shared --enable-static --enable-threads=no --disable-threads --enable-lto --disable-libstdcxx-pch --disable-checking --enable-libstdcxx-filesystem-ts --disable-libatomic --disable-libmudflap --disable-libssp --disable-libquadmath --disable-libgomp --disable-nls --disable-bootstrap --disable-install-libiberty --enable-languages=c,c++,lto "--program-transform-name=s&^&riscv64-unknown-linux-gnu-&" --disable-option-checking --build=x86_64-pc-linux-gnu --host=x86_64-pc-linux-gnu --target=riscv64-unknown-linux-gnu &> ${ROOTDIR}/${BUILDJSDIR}/3-gcc-configure.log
  cd ..

  # Replace the emscripten headers
  cd ${ROOTDIR}/${BUILDJSDIR}
  for nukeheader in ${EMFIXES}; do
    if [ ! -f ${ROOTDIR}/packages/emsdk/upstream/emscripten/cache/sysroot/${nukeheader} ]; then
      echo " - restoring emscripten's system ${nukeheader} header file"
      cp $(basename ${nukeheader}) ${ROOTDIR}/packages/emsdk/upstream/emscripten/cache/sysroot/${nukeheader}
    fi
  done

  cd ${ROOTDIR}/${BUILDJSDIR}
  echo " - building (second pass... will fail) a JavaScript gcc for risc-v targets"
  run "emmake make CXX=em++ CFLAGS=\"${CFLAGS}\" CPPFLAGS=\"${CPPFLAGS}\" LDFLAGS=\"${LDFLAGS}\" PTHREAD_CFLAGS=\"\"" "${ROOTDIR}/${BUILDJSDIR}/4-make.log"

  echo " - copying over built ./gcc to JavaScript build path"
  mkdir -p ${ROOTDIR}/${BUILDJSDIR}/gcc/build
  find ${ROOTDIR}/${BUILDDIR}/gcc/build -type f ! -name "*.*" -exec cp --preserve=mode {} ${ROOTDIR}/${BUILDJSDIR}/gcc/build/. \;
  find ${ROOTDIR}/${BUILDJSDIR}/gcc/build -type f ! -name "*.*" -exec touch -d "+24 hour" {} \;

  echo " - copying over native generator and bootstrap binaries to JavaScript build path"

  COPIES="fixincludes/fixincl gcc/xgcc gcc/cc1 build-x86_64-pc-linux-gnu/fixincludes/fixincl gcc/as"

  for fixup in ${COPIES}
  do
    rm -f ${ROOTDIR}/${BUILDJSDIR}/${fixup}
    mkdir -p ${ROOTDIR}/${BUILDJSDIR}/$(dirname "${fixup}")
    echo " - copying ${fixup} over from the native build"
    cp --preserve=mode ${ROOTDIR}/${BUILDDIR}/${fixup} ${ROOTDIR}/${BUILDJSDIR}/${fixup}
    chmod +x ${ROOTDIR}/${BUILDJSDIR}/${fixup}
    touch -d "+24 hour" ${ROOTDIR}/${BUILDJSDIR}/${fixup}
  done

  echo " - building (third pass) a JavaScript gcc for risc-v targets"
  run "emmake make CXX=em++ CFLAGS=\"${CFLAGS}\" CPPFLAGS=\"${CPPFLAGS}\" LDFLAGS=\"${LDFLAGS}\" PTHREAD_CFLAGS=\"\"" "${ROOTDIR}/${BUILDJSDIR}/5-make.log"
  if [ ! -f ${ROOTDIR}/${BUILDJSDIR}/gcc/gcc.o ]; then
    echo " - ERROR: could not build the JavaScript gcc"
    exit 1
  fi
else
  echo " - using existing built JavaScript gcc for RISC-V Linux targets"
fi

cd ${ROOTDIR}

if [ ! -d ${ROOTDIR}/${INSTALLDIR} ]; then
  echo " - creating ${INSTALLDIR} for JavaScript workers"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
else
  echo " - installing to existing ${INSTALLDIR} for JavaScript workers"
fi

EMSCRIPTEN_WASM_OPTS="-s FORCE_FILESYSTEM=1 -s WASM=1 -s NO_EXIT_RUNTIME=0 -lworkerfs.js -lfs.js -s ASSERTIONS=1 ${PROFILING_OPTS}"

# Create the new gcc entry
if [ ! -f ${SRCDIR}/gcc/gcc-bare.c ]; then
  echo " - creating gcc-bare.c"
  cp ${SRCDIR}/gcc/gcc.c ${SRCDIR}/gcc/gcc-bare.c
  cd ${SRCDIR}
  patch -N -p1 < ${PATCHBAREDIR}/gcc-bare.patch &> ${SRCDIR}/patching.log
else
  echo " - creating gcc-bare.c (exists already)"
fi

if [ ! -f ${ROOTDIR}/${BUILDJSDIR}/gcc/gcc-bare.o ]; then
  echo " - creating gcc-bare.o"
  cd ${ROOTDIR}/${BUILDJSDIR}/gcc
  COMMAND=`cat ${ROOTDIR}/${BUILDJSDIR}/5-make.log | grep "\-o gcc.o" | sed "s/gcc\./gcc-bare./g"`
  echo ${COMMAND} > ${ROOTDIR}/${BUILDJSDIR}/gcc/build-gcc-bare.sh
  chmod +x ${ROOTDIR}/${BUILDJSDIR}/gcc/build-gcc-bare.sh
  source ${ROOTDIR}/${BUILDJSDIR}/gcc/build-gcc-bare.sh

  if [ ! -f ${ROOTDIR}/${BUILDJSDIR}/gcc/gcc-bare.o ]; then
    echo " - ERROR: could not build gcc-bare.o"
    exit 1
  fi
else
  echo " - creating gcc-bare.o (exists already)"
fi

# Link
BINARIES="cpp gcc-ar gcc-nm gcc-ranlib cc1 cc1plus xgcc xg++"

for binary in ${BINARIES}
do
  # Initialize emscripten
  if [ ! ${EMSDK} ]; then
    echo " - initializing emcripten"
    source "$ROOTDIR/packages/emsdk/emsdk_env.sh" &> ${ROOTDIR}/${BUILDJSDIR}/0-emscripten-initialize.log
  fi

  # xgcc and xg++ must be just "gcc" and "g++"
  dest=${binary}
  if [[ ${binary} == x* ]]; then
    dest=`echo ${binary} | cut -c2-`
  fi

  if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${HOST}-${dest}.js ]; then
    echo " - creating ${INSTALLDIR}/${HOST}-${dest}.js"
    cd ${ROOTDIR}/${BUILDJSDIR}/gcc
    rm -f ${binary}
    run "emmake make ${binary}" "${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log"
    context=0
    while [[ `cat ${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log | grep "\-o ${binary}" -A${context}` == *\\ ]]; do
      context=$[$context + 1]
    done

    # Creates *-bare.js
    COMMAND=`cat ${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log | grep "\-o ${binary}" -A${context} | sed "s; gcc\.o; gcc-bare.o;g" | sed "s;-o ${binary};${EMSCRIPTEN_WASM_OPTS} -o ${ROOTDIR}/${INSTALLDIR}/${HOST}-${dest}-bare.js;"`
    echo ${COMMAND} > ${ROOTDIR}/${BUILDJSDIR}/gcc/build-${binary}.sh
    chmod +x ${ROOTDIR}/${BUILDJSDIR}/gcc/build-${binary}.sh
    source ${ROOTDIR}/${BUILDJSDIR}/gcc/build-${binary}.sh

    if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${HOST}-${dest}-bare.js ]; then
      echo " - ERROR: cannot make JavaScript ${dest}"
      exit 1
    fi

    # Creates the worker itself
    cat ${ROOTDIR}/assets/js/browserfs.min.js ${ROOTDIR}/lib/pre-worker.js ${ROOTDIR}/${INSTALLDIR}/${HOST}-${dest}-bare.js ${ROOTDIR}/lib/post-worker.js > ${ROOTDIR}/${INSTALLDIR}/${HOST}-${dest}.js
  else
    echo " - creating ${INSTALLDIR}/${HOST}-${binary}.js (exists already)"
  fi
done

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
exit
