#!/bin/bash

HEADER="Building RISC-V Proxy Kernel"
TARGET=riscv64-linux
PACKAGE=riscv-pk

# The host target we are building
HOST="riscv64-unknown-elf"

source $PWD/scripts/common/include.sh

SRCDIR=$PWD/packages/riscv64-linux/riscv-pk
BUILDDIR=packages/riscv64-linux/riscv-pk-build
INSTALLDIR=assets/static/riscv64-linux

echo " - installing/updating RISC-V packages"
./scripts/riscv64-linux/install.sh &> ${ROOTDIR}/packages/riscv64-linux/install-during-riscvpk.log

# Create build path for building native risc-v binutils
if [ ! -d ${ROOTDIR}/${BUILDDIR} ]; then
  echo " - creating ${BUILDDIR} directory"
  mkdir -p ${ROOTDIR}/${BUILDDIR}
else
  echo " - warning: using existing build directory at ${BUILDDIR}"
fi

# Compile native riscv-pk
if [ ! -f ${ROOTDIR}/${BUILDDIR}/bbl.bin ]; then
  echo " - patching riscv-pk"

  cd ${SRCDIR}
  if [ ! -f ${SRCDIR}/patching.log ]; then
    run "patch -s -Np1 -i ${PATCHDIR}/tinyemu.patch" "${SRCDIR}/patching.log"
  else
    echo " - patches already believed to be applied (remove patching.log to reapply)"
  fi

  cd ${ROOTDIR}/${BUILDDIR}

  echo " - configuring a native riscv-pk for RISC-V targets"
  ${SRCDIR}/configure --prefix=${ROOTDIR}/${UTILSDIR}/riscv64-unknown-elf/ --host=riscv64-unknown-elf &> ${ROOTDIR}/${BUILDDIR}/0-configure.log

  echo " - building a native riscv-pk for RISC-V targets"
  run "make" "${ROOTDIR}/${BUILDDIR}/1-make.log"
  cd ${ROOTDIR}
else
  echo " - using native build of RISC-V riscv-pk in ${BUILDDIR}"
fi

# Install native riscv-pk
if [ ! -f ${ROOTDIR}/${INSTALLDIR}/boot/bbl.bin ]; then
  echo " - installing native build of RISC-V bootloader to ${INSTALLDIR}/boot"
  cd ${ROOTDIR}/${BUILDDIR}
  mkdir -p ${ROOTDIR}/${INSTALLDIR}/boot
  cp bbl.bin ${ROOTDIR}/${INSTALLDIR}/boot/bbl.bin
  cd ${ROOTDIR}
else
  echo " - using existing installed native riscv-pk for RISC-V targets"
fi

echo " - native bootloader (riscv-pk) installed to ${INSTALLDIR}"

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
