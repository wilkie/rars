#!/bin/bash

HEADER="Building RISC-V binutils for Linux"
TARGET=riscv64-linux
PACKAGE=binutils

# The host target we are building
HOST="riscv64-unknown-linux-gnu"

source $PWD/scripts/common/include.sh

SRCDIR=$PWD/packages/riscv64/riscv-gnu-toolchain/riscv-binutils
BUILDDIR=packages/riscv64-linux/riscv-binutils-build
BUILDJSDIR=packages/riscv64-linux/riscv-binutils-js

INSTALLDIR=assets/js/targets/riscv64-linux/binutils

echo " - installing/updating RISC-V for Linux packages"
mkdir -p ${ROOTDIR}/packages/riscv64-linux
./scripts/riscv64-linux/install.sh &> ${ROOTDIR}/packages/riscv64-linux/install-during-binutils.log

# Create build path for building native risc-v binutils
if [ ! -d ${ROOTDIR}/${BUILDDIR} ]; then
  echo " - creating ${BUILDDIR} directory"
  mkdir -p ${ROOTDIR}/${BUILDDIR}
else
  echo " - warning: using existing build directory at ${BUILDDIR}"
fi

# Compile native risc-v binutils
if [ ! -f ${ROOTDIR}/${BUILDDIR}/bfd/doc/chew ]; then
  cd ${ROOTDIR}/${BUILDDIR}

  echo " - configuring a native binutils for RISC-V Linux targets"
  ${SRCDIR}/configure --prefix /usr --target=${HOST} --disable-nls &> ${ROOTDIR}/${BUILDDIR}/0-configure.log

  echo " - building a native binutils for RISC-V Linux targets"
  run "make" "${ROOTDIR}/${BUILDDIR}/1-make.log"
  cd ${ROOTDIR}
else
  echo " - using native build of RISC-V Linux binutils in ${BUILDDIR}"
fi

# Install native risc-v binutils
if [ ! -f ${ROOTDIR}/${UTILSDIR}/usr/bin/${HOST}-as ]; then
  echo " - installing native build of RISC-V Linux binutils to ${UTILSDIR}"
  cd ${ROOTDIR}/${BUILDDIR}
  make install DESTDIR=${ROOTDIR}/${UTILSDIR} &> ${ROOTDIR}/${BUILDDIR}/2-make-install.log
  cd ${ROOTDIR}
else
  echo " - using existing installed native binutils for RISC-V Linux targets"
fi

echo " - native binutils for RISC-V Linux targets installed to ${UTILSDIR}"

PROFILING_OPTS=

# Uncomment to gain stack traces
#PROFILING_OPTS=-g --profiling-funcs

# Create build path for JavaScript build of risc-v binutils
if [ ! -d ${ROOTDIR}/${BUILDJSDIR} ]; then
  echo " - creating ${BUILDJSDIR} directory"
  mkdir -p ${ROOTDIR}/${BUILDJSDIR}
else
  echo " - warning: using existing build directory at ${BUILDJSDIR}"
fi

# Initialize emscripten
if [ ! ${EMSDK} ]; then
  echo " - initializing emcripten"
  source "$ROOTDIR/packages/emsdk/emsdk_env.sh" &> ${ROOTDIR}/${BUILDJSDIR}/0-emscripten-initialize.log
else
  echo " - using existing activated emcripten at ${EMSDK}"
fi

cd ${ROOTDIR}/${BUILDJSDIR}

CFLAGS="-DHAVE_PSIGNAL -m32 ${PROFILING_OPTS} -s ALLOW_MEMORY_GROWTH=1"

# Build the JavaScript binutils for risc-v
if [ ! -f ${ROOTDIR}/${BUILDJSDIR}/gas/as.o ]; then
  # Configure
  cd ${ROOTDIR}/${BUILDJSDIR}
  echo " - configuring a JavaScript binutils for RISC-V Linux targets"
  emconfigure ${SRCDIR}/configure CFLAGS="${CFLAGS}" --target=${HOST} --prefix /usr \
    --disable-nls \
    --enable-lto \
    --disable-install-libiberty \
    --disable-multilib &> ${ROOTDIR}/${BUILDJSDIR}/1-configure.log

  # Build first pass
  echo " - copying over native generators needed during compilation"

  COPIES="bfd/doc/chew bfd/doc/chew.stamp binutils/sysinfo"

  for fixup in ${COPIES}
  do
    rm -f ${ROOTDIR}/${BUILDJSDIR}/${fixup}
    mkdir -p ${ROOTDIR}/${BUILDJSDIR}/$(dirname "${fixup}")
    cp --preserve=mode ${ROOTDIR}/${BUILDDIR}/${fixup} ${ROOTDIR}/${BUILDJSDIR}/${fixup}
    chmod +x ${ROOTDIR}/${BUILDJSDIR}/${fixup}
    touch -d "+24 hour" ${ROOTDIR}/${BUILDJSDIR}/${fixup}
  done

  echo " - building a JavaScript binutils for RISC-V Linux targets"
  run "emmake make all-ld CFLAGS=\"${CFLAGS}\"" "${ROOTDIR}/${BUILDJSDIR}/2-make.log"
else
  echo " - using existing built JavaScript binutils for RISC-V Linux targets"
fi

cd ${ROOTDIR}

if [ ! -d ${ROOTDIR}/${INSTALLDIR} ]; then
  echo " - creating ${INSTALLDIR} for JavaScript workers"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
else
  echo " - installing to existing ${INSTALLDIR} for JavaScript workers"
fi

EMSCRIPTEN_WASM_OPTS="-s WASM=1 -s NO_EXIT_RUNTIME=0 -lworkerfs.js -lfs.js -s FORCE_FILESYSTEM=1 -s ASSERTIONS=1 ${PROFILING_OPTS}"

BINARIES="gas/as-new ld/ld-new binutils/objdump binutils/readelf"

for binary in ${BINARIES}
do
  dest=$(basename ${binary})
  if [[ ${dest} == *-new ]]; then
    dest=`echo ${dest} | cut -c1-2`
  fi

  subpath=$(dirname ${binary})
  binary=$(basename ${binary})

  if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${HOST}-${dest}.js ]; then
    echo " - creating ${INSTALLDIR}/${HOST}-${dest}.js"
    cd ${ROOTDIR}/${BUILDJSDIR}/${subpath}
    rm -f ${binary}
    rm -f ${binary}.wasm
    run "emmake make ${binary}" "${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log"
    context=0
    while [[ `cat ${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log | grep "libtool: link:.\+-o ${binary} " -A${context}` == *\\ ]]; do
      context=$[$context + 1]
    done

    # Creates *-bare.js
    COMMAND=`cat ${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log | grep "libtool: link:.\+-o ${binary} " -A${context} | sed "s;libtool: link: ;;" | sed "s;-o ${binary};${EMSCRIPTEN_WASM_OPTS} -o ${ROOTDIR}/${INSTALLDIR}/${HOST}-${dest}-bare.js;"`
    echo ${COMMAND} > ${ROOTDIR}/${BUILDJSDIR}/build-${binary}.sh
    chmod +x ${ROOTDIR}/${BUILDJSDIR}/build-${binary}.sh
    source ${ROOTDIR}/${BUILDJSDIR}/build-${binary}.sh

    if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${HOST}-${dest}-bare.js ]; then
      echo " - ERROR: cannot make JavaScript ${dest}"
      exit 1
    fi

    # Creates the worker itself
    cat ${ROOTDIR}/assets/js/browserfs.min.js ${ROOTDIR}/lib/pre-worker.js ${ROOTDIR}/${INSTALLDIR}/${HOST}-${dest}-bare.js ${ROOTDIR}/lib/post-worker.js > ${ROOTDIR}/${INSTALLDIR}/${HOST}-${dest}.js
  else
    echo " - creating ${INSTALLDIR}/${HOST}-${dest}.js (exists already)"
  fi
done

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
