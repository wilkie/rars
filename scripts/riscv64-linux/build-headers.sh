#!/bin/bash

HEADER="Building RISC-V Linux headers"
TARGET=riscv64-linux
PACKAGE=headers

# The host target we are building
HOST="riscv64-unknown-linux-gnu"

source $PWD/scripts/common/include.sh

SRCDIR=$PWD/packages/riscv64-linux/linux-${LINUX_VERSION}
BUILDDIR=packages/riscv64-linux/linux-${LINUX_VERSION}

INSTALLDIR=assets/static/riscv64-linux

if [ ! ${QUIET} ]; then
  echo " - installing/updating RISC-V Linux packages"
  ./scripts/riscv64-linux/install.sh &> ${ROOTDIR}/packages/riscv64-linux/install-during-glibc.log
fi

# Compile native risc-v Linux headers
if [ ! -f ${ROOTDIR}/${BUILDDIR}/usr/include/asm-generic/.mman.h.cmd ]; then
  cd ${ROOTDIR}/${BUILDDIR}

  echo " - building system headers for Linux ${LINUX_VERSION}"
  run "make headers ARCH=riscv CROSS_COMPILE=${HOST}- INSTALL_HDR_PATH=${ROOTDIR}/${UTILSDIR}/usr" "${ROOTDIR}/${BUILDDIR}/1-make-headers.log"

  cd ${ROOTDIR}
else
  echo " - using existing build of RISC-V Linux headers in ${BUILDDIR}"
fi

# Install native risc-v Linux headers
if [ ! -f ${ROOTDIR}/${UTILSDIR}/usr/include/asm/ioctl.h ]; then
  echo " - installing native RISC-V Linux headers to ${UTILSDIR}"
  cd ${ROOTDIR}/${BUILDDIR}
  run "make headers_install ARCH=riscv CROSS_COMPILE=${HOST}- INSTALL_HDR_PATH=${ROOTDIR}/${UTILSDIR}/usr" "${ROOTDIR}/${BUILDDIR}/2-make-headers-install.log"
  cd ${ROOTDIR}
else
  echo " - using existing installed native glibc for RISC-V Linux targets"
fi

echo " - native RISC-V Linux headers installed to ${UTILSDIR}"

if [ ! -f ${ROOTDIR}/${INSTALLDIR}/usr/include/asm/ioctl.h ]; then
  echo " - installing native RISC-V Linux headers to ${INSTALLDIR}"
  cd ${ROOTDIR}/${BUILDDIR}
  run "make headers_install ARCH=riscv CROSS_COMPILE=${HOST}- INSTALL_HDR_PATH=${ROOTDIR}/${INSTALLDIR}/usr" "${ROOTDIR}/${BUILDDIR}/3-make-headers-install-static.log"

  if [ ! -f ${ROOTDIR}/${INSTALLDIR}/usr/include/asm/ioctl.h ]; then
    echo " - ERROR: could not install RISC-V Linux headers"
    exit 1
  fi
else
  echo " - using existing installed RISC-V Linux headers"
fi

echo " - native RISC-V Linux headers also installed to ${INSTALLDIR}"

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
