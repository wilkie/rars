# RISC-V Linux Toolchain

These scripts build a native and a JavaScript (emscripten) version of a complete
RISC-V toolchain targetting the Linux kernel. This consists of binutils (an
assembler and linker via `as` and `ld` respectively), a C runtime and standard
library via `glibc`, and a C compiler via `gcc` (and its subcomponents `cc1`,
`collect2`, and `cpp` for its various stages). Also included is a debugger via
`gdb` (although it is currently not provided for Linux targets).

This build works very similarly to the non-Linux target found with the plain
`riscv64` target.

## Scripts

* `build-binutils.sh` - Builds binutils. Creates `as`, `ld`, `readelf`, and `objdump` JavaScript workers.
<!--* `build-gdb.sh` - Builds `gdb` along with its JavaScript worker.-->
* `build-gcc.sh` - Builds `gcc`, `cc1`, `cpp`, `gcc-ar`, `gcc-nm` and `collect2` along with JavaScript workers.
* `build-glibc.sh` - Builds `libc` for a Linux environment. `gcc` needs at least a minimal `libc`.
* `build-headers.sh` - Builds the Linux kernel system headers for RISC-V.
* `build-linux.sh` - Builds a Linux kernel for RISC-V that we can target.

The `gcc` and `g++` that is built has forking disabled and announces the commands it wants to run.
In this form, the underlying system is responsible for delegating the commands to the workers built in other
steps such as `as`, `ld,` and `cc1` in the case of a C project compile. Generally, the output files
left by the final step provided by `gcc` is the resulting output file and every other file is a temporary
intermediate that may or may not be kept.
