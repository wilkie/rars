#!/bin/bash

ROOTDIR=$PWD

echo ""
if [ ${RAWRS_COUNTER} ]; then
  echo "${RAWRS_COUNTER}. Building ARM64 binutils"
  echo "--------------------------"
else
  echo "Building ARM64 binutils"
  echo "======================="
fi
echo ""

if [ ! -f ${ROOTDIR}/scripts/arm64-linux/versions.sh ]; then
  echo " - ERROR: need to run this script from the rawrs project directory"
  exit 1
fi

source $PWD/scripts/arm64-linux/versions.sh

# The host target we are building
TARGET="aarch64-linux-gnu"

PATCHDIR=${ROOTDIR}/patches/arm64-linux/binutils
UTILSDIR=utils/arm64

SRCDIR=$PWD/packages/arm64-linux/binutils-${BINUTILS_VERSION}
BUILDDIR=packages/arm64-linux/binutils-${BINUTILS_VERSION}-build
BUILDJSDIR=packages/arm64-linux/binutils-${BINUTILS_VERSION}-js

INSTALLDIR=assets/js/targets/arm64/binutils

# Compile options

PROFILING_OPTS="-O2"

# Uncomment to gain stack traces
#PROFILING_OPTS="${PROFILING_OPTS} -g --profiling-funcs"

CFLAGS="-DHAVE_PSIGNAL -m32 ${PROFILING_OPTS}"

echo " - installing/updating ARM64 packages"
mkdir -p ${ROOTDIR}/packages/arm64-linux
./scripts/arm64-linux/install.sh &> ${ROOTDIR}/packages/arm64-linux/install-during-binutils.log

if [ ! -d ${ROOTDIR}/${BUILDDIR} ]; then
  echo " - creating ${BUILDDIR} directory"
  mkdir -p ${ROOTDIR}/${BUILDDIR}
else
  echo " - warning: using existing build directory at ${BUILDDIR}"
fi

if [ ! -f ${ROOTDIR}/${UTILSDIR}/usr/bin/${TARGET}-as ]; then
  echo " - configuring a native binutils for ARM64 targets"
  cd ${ROOTDIR}/${BUILDDIR}
  ${SRCDIR}/configure --prefix=/usr \
    --target=$TARGET \
    --infodir="/usr/share/info/$TARGET" \
    --datadir="/usr/$TARGET/share" \
    --enable-lto --disable-nls \
    --disable-install-libiberty \
    --disable-multilib --disable-nls \
    --disable-werror &> ${ROOTDIR}/${BUILDDIR}/native-configure.log

  echo " - building a native binutils for ARM64 targets"
  make &> ${ROOTDIR}/${BUILDDIR}/native-make.log

  # Install
  if [ ! -d ${ROOTDIR}/${UTILSDIR} ]; then
    echo " - creating ${UTILSDIR} to install native binutils"
    mkdir -p ${ROOTDIR}/$UTILSDIR
  fi

  echo " - installing native binutils for ARM64 targets"
  make -C ${ROOTDIR}/${BUILDDIR} DESTDIR=${ROOTDIR}/${UTILSDIR} install &> ${ROOTDIR}/${BUILDDIR}/native-make-install.log
else
  echo " - using existing installed native binutils for ARM64 targets"
fi

echo " - native binutils installed to ${UTILSDIR}"

if [ ! -d ${ROOTDIR}/${BUILDJSDIR} ]; then
  echo " - creating ${BUILDJSDIR} directory"
  mkdir -p ${ROOTDIR}/${BUILDJSDIR}
else
  echo " - warning: using existing build directory at ${BUILDJSDIR}"
fi

if [ ! ${EMSDK} ]; then
  echo " - initializing emcripten"
  source "$ROOTDIR/packages/emsdk/emsdk_env.sh" &> ${ROOTDIR}/${BUILDJSDIR}/0-emscripten-initialize.log
else
  echo " - using existing activated emcripten at ${EMSDK}"
fi

cd ${ROOTDIR}/${BUILDJSDIR}

if [ ! -f ${ROOTDIR}/${BUILDJSDIR}/gas/as.o ]; then
  echo " - configuring a JavaScript binutils for ARM64 targets"
  # TODO: we can re-enable lto if we can support loading it via dlopen by
  #       compiling it with certain options or compiling it into the binary
  #       somehow.
  emconfigure ${SRCDIR}/configure --prefix=/usr \
    --target=$TARGET \
    --infodir="/usr/share/info/$TARGET" \
    --datadir="/usr/$TARGET/share" \
    --disable-lto --disable-nls \
    --disable-install-libiberty \
    --disable-multilib --disable-nls \
    --disable-werror &> ${ROOTDIR}/${BUILDJSDIR}/1-configure.log

  echo " - copying over native generators needed during compilation"

  COPIES="bfd/doc/chew bfd/doc/chew.stamp binutils/sysinfo"

  for fixup in ${COPIES}
  do
    rm -f ${ROOTDIR}/${BUILDJSDIR}/${fixup}
    mkdir -p ${ROOTDIR}/${BUILDJSDIR}/$(dirname "${fixup}")
    cp --preserve=mode ${ROOTDIR}/${BUILDDIR}/${fixup} ${ROOTDIR}/${BUILDJSDIR}/${fixup}
    chmod +x ${ROOTDIR}/${BUILDJSDIR}/${fixup}
    touch -d "+24 hour" ${ROOTDIR}/${BUILDJSDIR}/${fixup}
  done

  echo " - building a JavaScript binutils for ARM64 targets"
  emmake make all-ld CFLAGS="${CFLAGS}" &> ${ROOTDIR}/${BUILDJSDIR}/2-make.log
else
  echo " - using existing built JavaScript binutils for ARM64 targets"
fi

cd $ROOTDIR

if [ ! -d ${ROOTDIR}/${INSTALLDIR} ]; then
  echo " - creating ${INSTALLDIR} for JavaScript workers"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
else
  echo " - installing to existing ${INSTALLDIR} for JavaScript workers"
fi

EMSCRIPTEN_WASM_OPTS="-s WASM=1 -s NO_EXIT_RUNTIME=0 -lworkerfs.js -lfs.js -s FORCE_FILESYSTEM=1 -s ASSERTIONS=1 ${PROFILING_OPTS}"

BINARIES="gas/as-new ld/ld-new binutils/objdump binutils/readelf"

for binary in ${BINARIES}
do
  dest=$(basename ${binary})
  if [[ ${dest} == *-new ]]; then
    dest=`echo ${dest} | cut -c1-2`
  fi

  subpath=$(dirname ${binary})
  binary=$(basename ${binary})

  if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${TARGET}-${dest}.js ]; then
    echo " - creating ${INSTALLDIR}/${TARGET}-${dest}.js"
    cd ${ROOTDIR}/${BUILDJSDIR}/${subpath}
    rm -f ${binary}
    rm -f ${binary}.wasm
    emmake make ${binary} &> ${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log
    context=0
    while [[ `cat ${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log | grep "libtool: link:.\+-o ${binary} " -A${context}` == *\\ ]]; do
      context=$[$context + 1]
    done

    # Creates *-bare.js
    COMMAND=`cat ${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log | grep "libtool: link:.\+-o ${binary} " -A${context} | sed "s;libtool: link: ;;" | sed "s;-o ${binary};${EMSCRIPTEN_WASM_OPTS} -o ${ROOTDIR}/${INSTALLDIR}/${TARGET}-${dest}-bare.js;"`
    echo ${COMMAND} > ${ROOTDIR}/${BUILDJSDIR}/build-${binary}.sh
    chmod +x ${ROOTDIR}/${BUILDJSDIR}/build-${binary}.sh
    source ${ROOTDIR}/${BUILDJSDIR}/build-${binary}.sh

    if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${TARGET}-${dest}-bare.js ]; then
      echo " - ERROR: cannot make JavaScript ${dest}"
      exit 1
    fi

    # Creates the worker itself
    cat ${ROOTDIR}/assets/js/browserfs.min.js ${ROOTDIR}/lib/pre-worker.js ${ROOTDIR}/${INSTALLDIR}/${TARGET}-${dest}-bare.js ${ROOTDIR}/lib/post-worker.js > ${ROOTDIR}/${INSTALLDIR}/${TARGET}-${dest}.js
  else
    echo " - creating ${INSTALLDIR}/${TARGET}-${dest}.js (exists already)"
  fi
done

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
