const SVGSpriter = require('svg-sprite');
const fs = require('fs');

var config = {
  mode: {
      symbol: true,
  },
  shape: {
    id: {
      generator: (name, file) => {
        let path = file.path;
        name = path.substring(0, path.length - 4).replace('/', '-');
        return name;
      }
    },
    transform: [
      {
        svgo: {
          plugins: [
            {
              removeAttrs: {
                attrs: "*:(stroke|fill):((?!^none$).)*"
              }
            }
          ]
        }
      }
    ]
  }
};

// Which places are we parsing into a sheet?
var sheets = ["ui", "dinosaurs", "toolbar"];

sheets.forEach( async (collection) => {
    // Create spriter instance
    let spriter = new SVGSpriter(config);

    // Add SVG source files
    let path = 'assets/images/icons/' + collection;

    await new Promise( (resolve, reject) => {
        fs.readdir(path, async (err, items) => {
            await new Promise( (resolve, reject) => {
                let count = items.length;
                items.forEach( async (item) => {
                    let filename = path + '/' + item;
                    await new Promise( (resolve, reject) => {
                        fs.stat(filename, async (err, stat) => {
                            if (stat && stat.isDirectory()) {
                                await new Promise( (resolve, reject) => {
                                    fs.readdir(filename, (err, subitems) => {
                                        let subcount = subitems.length;
                                        subitems.forEach( (subitem) => {
                                            console.log("Reading", "icons/" + collection + "/" + item + "/" + subitem);
                                            spriter.add("./" + item + "/" + subitem, null, fs.readFileSync(filename + "/" + subitem, {encoding: 'utf-8'}));

                                            subcount--;
                                            if (subcount == 0) {
                                                resolve();
                                            }
                                        });
                                    });
                                });
                            }
                            else {
                                console.log("Reading", "icons/" + collection + "/" + item);
                                spriter.add("./" + item, null, fs.readFileSync(filename, {encoding: 'utf-8'}));
                            }
                            resolve();
                        });
                    });

                    count--;
                    if (count == 0) {
                        resolve();
                    }
                });
            });

            resolve();
        });
    });

    console.log("Building", collection + ".svg");

    // Compile the sprite
    spriter.compile( (error, result) => {
        /* Write `result` files to disk (or do whatever with them ...) */
        for (var mode in result) {
            for (var resource in result[mode]) {
                if (!fs.existsSync("assets/images")) {
                    fs.mkdirSync("assets/images");
                }
                if (!fs.existsSync("assets/images/" + mode)) {
                    fs.mkdirSync("assets/images/" + mode);
                }
                fs.writeFileSync("assets/images/" + mode + "/" + collection + ".svg", result[mode][resource].contents);
            }
        }
    });
});
