#include <libretro.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <emscripten.h>
#include <deque>
#include <string>

// Retaining controller possibilities
std::deque<std::deque<unsigned int>> controller_ports;
std::deque<std::deque<std::string>> controller_ports_names;
std::deque<std::deque<std::deque<std::string>>> controller_ports_item_names;
std::deque<std::deque<std::deque<unsigned int>>> controller_ports_item_devices;
std::deque<std::deque<std::deque<unsigned int>>> controller_ports_item_ids;
std::deque<std::deque<std::deque<int16_t>>> controller_ports_item_state;

// Retain pixel format
enum retro_pixel_format _retro_format;

// Framebuffers
#define RGBA_BUFFERS 2
static uint32_t* _retro_bgra8888data[RGBA_BUFFERS] = {NULL, NULL};
static size_t _retro_bgra8888frame = 0;

#define AUDIO_BUFFERS 3
static float* _retro_audiodata[AUDIO_BUFFERS] = {NULL, NULL, NULL};
static size_t _retro_audiodatalength = 0;
static size_t _retro_audioframe = 0;

#define AUDIO_THRESHOLD 3072

/**
 * Actual callbacks.
 */

/**
 * The callback for setting options.
 */
extern "C" {
    EM_JS(void, push_frame, (const void *data, unsigned width, unsigned height, size_t pitch), {
        if (self.Module.__push_frame) {
            self.Module.__push_frame(data, width, height, pitch);
        }
    });

    EM_JS(void, push_samples, (const void *data, size_t frames), {
        if (self.Module.__push_samples) {
            self.Module.__push_samples(data, frames);
        }
    });

    // For video draws
    void _retro_video_callback(const void *data, unsigned width, unsigned height, size_t pitch) {
        // Allocate frame
        if (!_retro_bgra8888data[_retro_bgra8888frame]) {
            _retro_bgra8888data[_retro_bgra8888frame] = (uint32_t*)malloc(sizeof(uint32_t) * width * height);
        }

        // Determine this frame
        uint32_t* bgra8888data = _retro_bgra8888data[_retro_bgra8888frame];

        // Rewrite to BGRA8888 if format is RGB565
        if (_retro_format == RETRO_PIXEL_FORMAT_XRGB8888) {
            // Rewrite XRGB8888 -> BGRA8888
            uint32_t* xrgb8888data = (uint32_t*)data;
            uint32_t* ptr = bgra8888data;
            for (size_t i = 0; i < width * height; i++) {
                *ptr = ((*xrgb8888data) & 0xff00) |
                       ((*xrgb8888data & 0xff) << 16) |
                       ((*xrgb8888data >> 16) & 0xff) |
                       (0xff000000);

                xrgb8888data++;
                ptr++;
            }
        }
        else if (_retro_format == RETRO_PIXEL_FORMAT_RGB565) {
            // Rewrite RGB565 -> BGRA8888
            uint32_t* ptr = bgra8888data;
            for (size_t y = 0; y < height; y++) {
                uint16_t* rgb565data = (uint16_t*)(((uint8_t*)data) + (pitch * y));

                for (size_t x = 0; x < width; x++) {
                    uint16_t rgb565 = *rgb565data;
                    uint32_t r = (((rgb565 >> 11) & 0x1f) * 527 + 23) >> 6;
                    uint32_t g = (((rgb565 >> 5) & 0x3f) * 259 + 33) >> 6;
                    uint32_t b = ((rgb565 & 0x1f) * 527 + 23) >> 6;

                    *ptr = (b << 16) | (g << 8) | (r) | 0xff000000;

                    rgb565data++;
                    ptr++;
                }
            }
        }

        // Go to the next buffer
        _retro_bgra8888frame = (_retro_bgra8888frame + 1) % RGBA_BUFFERS;

        push_frame((const void*)bgra8888data, width, height, width * 4);
    }

    void _retro_input_poll_callback() {
    }

    int16_t _retro_input_state_callback(unsigned port, unsigned device, unsigned index, unsigned id) {
        // Check bounds
        if (controller_ports_item_state.size() <= port) {
            return 0;
        }

        if (controller_ports_item_state[port].size() <= index) {
            return 0;
        }

        int16_t combined = 0;

        // Check input state based on device/id
        for (size_t item_id = 0; item_id < controller_ports_item_state[port][index].size(); item_id++) {
            if (id == RETRO_DEVICE_ID_JOYPAD_MASK) {
                // Combine all possible items together
                if (controller_ports_item_devices[port][index][item_id] == device) {
                    int16_t subId = controller_ports_item_ids[port][index][item_id];

                    if (controller_ports_item_state[port][index][item_id]) {
                        combined |= (1 << subId);
                    }
                }
            }
            else {
                if ((controller_ports_item_devices[port][index][item_id] == device) &&
                    (controller_ports_item_ids[port][index][item_id] == id)) {
                    return controller_ports_item_state[port][index][item_id];
                }
            }
        }

        if (id == RETRO_DEVICE_ID_JOYPAD_MASK) {
            return combined;
        }

        return 0;
    }

    void _retro_audio_sample(int16_t left, int16_t right) {
    }

    size_t _retro_audio_sample_batch(const int16_t* data, size_t frames) {
        // Allocate frame
        if (!_retro_audiodata[_retro_audioframe]) {
            _retro_audiodata[_retro_audioframe] = (float*)malloc((sizeof(float) * AUDIO_THRESHOLD) + 128);
        }

        float* f32data = _retro_audiodata[_retro_audioframe];
        float* ptrL = f32data + _retro_audiodatalength;
        float* ptrR = (f32data + 512) + _retro_audiodatalength;

        // We pool samples until we get at least a certain number of frames
        // Convert 16-bit PCM interleaved data to 32-bit planar float

        for (size_t i = 0, j = 0; i < frames; i++, j+=2) {
            *ptrL = ((float)data[j]) / 32768.0f;
            *ptrR = ((float)data[j + 1]) / 32768.0f;
            ptrL++;
            ptrR++;
        }

        _retro_audiodatalength += frames;

        if (_retro_audiodatalength >= AUDIO_THRESHOLD) {
            // Go to the next buffer
            _retro_audioframe = (_retro_audioframe + 1) % AUDIO_BUFFERS;

            push_samples((void*)f32data, _retro_audiodatalength);

            _retro_audiodatalength = 0;
        }

        return frames;
    }

    void _retro_log_callback(enum retro_log_level level, const char* format, ...) {
        va_list args;
        va_start(args, format);
        vprintf(format, args);
        va_end(args);
    }

    // For options get/set
    bool _retro_environment_callback(unsigned cmd, void *data) {
        switch (cmd) {
            case RETRO_ENVIRONMENT_GET_CORE_OPTIONS_VERSION:
                // We are 1.0
                *((unsigned int *)(data)) = 1;
                return 1;

            case RETRO_ENVIRONMENT_GET_LOG_INTERFACE:
                {
                    struct retro_log_callback* log;
                    log = (struct retro_log_callback*)data;
                    log->log = _retro_log_callback;
                }
                return 1;

            case RETRO_ENVIRONMENT_GET_SYSTEM_DIRECTORY:
                *((const char **)(data)) = "/work";
                return 1;

            case RETRO_ENVIRONMENT_SET_MEMORY_MAPS:
                return 1;

            case RETRO_ENVIRONMENT_GET_LANGUAGE:
                // Report english, TODO: support other languages
                *((unsigned int *)(data)) = RETRO_LANGUAGE_ENGLISH;
                return 1;

            case RETRO_ENVIRONMENT_SET_PIXEL_FORMAT:
                {
                    enum retro_pixel_format format = *((enum retro_pixel_format*)data);
                    if (format == RETRO_PIXEL_FORMAT_XRGB8888) {
                        _retro_format = format;
                        return 1;
                    }
                    else if (format == RETRO_PIXEL_FORMAT_RGB565) {
                        _retro_format = format;
                        return 1;
                    }
                }
                return 0;

            case RETRO_ENVIRONMENT_GET_INPUT_BITMASKS:
                return 1;

            case RETRO_ENVIRONMENT_SET_VARIABLES:
                // v0 options query
                {
                }
                return 0;

            case RETRO_ENVIRONMENT_SET_CORE_OPTIONS_INTL:
                // v1 options query
                {
                    struct retro_core_options_intl* core_options_intl = (struct retro_core_options_intl*)data;

                    // Go through 'US' array
                    struct retro_core_option_definition* definition;

                    definition = core_options_intl->us;
                    _retro_environment_callback(RETRO_ENVIRONMENT_SET_CORE_OPTIONS, (void*)definition);

                    definition = core_options_intl->local;
                    _retro_environment_callback(RETRO_ENVIRONMENT_SET_CORE_OPTIONS, (void*)definition);
                }

                return 1;

            case RETRO_ENVIRONMENT_SET_CORE_OPTIONS:
                {
                    struct retro_core_option_definition* current = (struct retro_core_option_definition*)data;

                    // Go through each key
                    while (current && current->key) {
                        // Go to the next entry
                        current++;
                    }
                }

                return 1;

#ifdef RETRO_ENVIRONMENT_SET_CORE_OPTIONS_V2
            case RETRO_ENVIRONMENT_SET_CORE_OPTIONS_V2:
                // v2 options query
                return 0;
#endif

            case RETRO_ENVIRONMENT_SET_CONTROLLER_INFO:
                {
                    // Gather controller information
                    struct retro_controller_info* controller_info = (struct retro_controller_info*)data;

                    unsigned int port_id = 0;
                    while (controller_info->types != NULL) {
                        unsigned int num_types = controller_info->num_types;
                        const struct retro_controller_description* types = controller_info->types;

                        controller_ports.push_back(std::deque<unsigned int>());
                        controller_ports_names.push_back(std::deque<std::string>());

                        for (size_t i = 0; i < num_types; i++) {
                            const char* desc = types[i].desc;
                            unsigned int id = types[i].id;

                            controller_ports[port_id].push_back(id);
                            controller_ports_names[port_id].push_back(std::string(desc));
                        }

                        port_id++;
                        controller_info++;
                    }
                }
                return 1;

            case RETRO_ENVIRONMENT_SET_INPUT_DESCRIPTORS:
                {
                    struct retro_input_descriptor* item = (struct retro_input_descriptor*)data;

                    while (item->description != NULL) {
                        unsigned int portID = item->port;
                        unsigned int device = item->device;
                        unsigned int index = item->index;
                        unsigned int id = item->id;

                        std::string name = std::string(item->description);

                        if (controller_ports_item_devices.size() <= portID) {
                            controller_ports_item_devices.push_back(std::deque<std::deque<unsigned int>>());
                        }

                        if (controller_ports_item_devices[portID].size() <= index) {
                            controller_ports_item_devices[portID].push_back(std::deque<unsigned int>());
                        }

                        controller_ports_item_devices[portID][index].push_back(device);

                        if (controller_ports_item_ids.size() <= portID) {
                            controller_ports_item_ids.push_back(std::deque<std::deque<unsigned int>>());
                        }

                        if (controller_ports_item_ids[portID].size() <= index) {
                            controller_ports_item_ids[portID].push_back(std::deque<unsigned int>());
                        }

                        controller_ports_item_ids[portID][index].push_back(id);

                        if (controller_ports_item_state.size() <= portID) {
                            controller_ports_item_state.push_back(std::deque<std::deque<int16_t>>());
                        }

                        if (controller_ports_item_state[portID].size() <= index) {
                            controller_ports_item_state[portID].push_back(std::deque<int16_t>());
                        }

                        // Defaults to off
                        controller_ports_item_state[portID][index].push_back(0);

                        if (controller_ports_item_names.size() <= portID) {
                            controller_ports_item_names.push_back(std::deque<std::deque<std::string>>());
                        }

                        if (controller_ports_item_names[portID].size() <= index) {
                            controller_ports_item_names[portID].push_back(std::deque<std::string>());
                        }

                        controller_ports_item_names[portID][index].push_back(name);

                        item++;
                    }
                }
                return 1;

            default:
                break;
        }

        return 0;
    }

    /**
     * Initialize the library.
     */
    void js_init() {
        retro_set_environment(_retro_environment_callback);
        retro_set_video_refresh(_retro_video_callback);
        retro_set_input_poll(_retro_input_poll_callback);
        retro_set_input_state(_retro_input_state_callback);
        retro_set_audio_sample(_retro_audio_sample);
        retro_set_audio_sample_batch(_retro_audio_sample_batch);
        retro_init();
    }

    /**
     * Deinitialize the library.
     */
    void js_deinit() {
        // Free frame buffers
        for (size_t i = 0; i < RGBA_BUFFERS; i++) {
            if (_retro_bgra8888data[i]) {
                free(_retro_bgra8888data[i]);
            }
        }

        // Free audio buffers
        for (size_t i = 0; i < AUDIO_BUFFERS; i++) {
            if (_retro_audiodata[i]) {
                free(_retro_audiodata[i]);
            }
        }
    }

    void load_game(const char* path) {
        // Get the data and size
        struct retro_game_info game;
        game.path = (char*)malloc(sizeof(char) * (strlen(path) + 1));
        strncpy((char*)game.path, path, strlen(path));

        // Get file size
        FILE* f = fopen(path, "rb");
        fseek(f, 0, SEEK_END);
        game.size = ftell(f);
        fseek(f, 0, SEEK_SET);

        // Get file data
        game.data = (void*)malloc(sizeof(char) * game.size);
        fread((void*)game.data, game.size, 1, f);

        // Close file
        fclose(f);

        // Send it off
        retro_load_game(&game);

        // Free data
        free((void*)game.data);
        free((void*)game.path);
    }

    /**
     * Returns the number of controller ports.
     */
    unsigned int controller_port_count() {
        return controller_ports.size();
    }

    /**
     * Returns the number of controller types for the given port.
     */
    unsigned int controller_port_type_count(unsigned int port_id) {
        if (controller_ports_item_state.size() <= port_id) {
            return 0;
        }

        return controller_ports[port_id].size();
    }

    /**
     * Returns the port type identifier.
     */
    unsigned int controller_port_type_id(unsigned int port_id, unsigned int type_id) {
        if (controller_ports_item_state.size() <= port_id) {
            return 0;
        }

        if (controller_ports_item_state[port_id].size() <= type_id) {
            return 0;
        }

        return controller_ports[port_id][type_id];
    }

    /**
     * Returns the port type name.
     */
    const char* controller_port_type_name(unsigned int port_id, unsigned int type_id) {
        if (controller_ports_item_state.size() <= port_id) {
            return "";
        }

        if (controller_ports_item_state[port_id].size() <= type_id) {
            return "";
        }

        return controller_ports_names[port_id][type_id].c_str();
    }

    /**
     * Returns the number of bind items for the device.
     */
    unsigned int controller_port_type_item_count(unsigned int port_id, unsigned int type_id) {
        if (controller_ports_item_state.size() <= port_id) {
            return 0;
        }

        if (controller_ports_item_state[port_id].size() <= type_id) {
            return 0;
        }

        return controller_ports_item_names[port_id][type_id].size();
    }

    /**
     * Returns the bind item device type for the given port, type, and bind item index.
     */
    unsigned int controller_port_type_item_device(unsigned int port_id, unsigned int type_id, unsigned int item_id) {
        if (controller_ports_item_state.size() <= port_id) {
            return 0;
        }

        if (controller_ports_item_state[port_id].size() <= type_id) {
            return 0;
        }

        if (controller_ports_item_state[port_id][type_id].size() <= item_id) {
            return 0;
        }

        return controller_ports_item_devices[port_id][type_id][item_id];
    }

    /**
     * Returns the bind item id for the given port, type, and bind item index.
     */
    unsigned int controller_port_type_item_id(unsigned int port_id, unsigned int type_id, unsigned int item_id) {
        if (controller_ports_item_state.size() <= port_id) {
            return 0;
        }

        if (controller_ports_item_state[port_id].size() <= type_id) {
            return 0;
        }

        if (controller_ports_item_state[port_id][type_id].size() <= item_id) {
            return 0;
        }

        return controller_ports_item_ids[port_id][type_id][item_id];
    }

    /**
     * Returns the bind item name for the given port, type, and bind item index.
     */
    const char* controller_port_type_item_name(unsigned int port_id, unsigned int type_id, unsigned int item_id) {
        if (controller_ports_item_state.size() <= port_id) {
            return "";
        }

        if (controller_ports_item_state[port_id].size() <= type_id) {
            return "";
        }

        if (controller_ports_item_state[port_id][type_id].size() <= item_id) {
            return "";
        }

        return controller_ports_item_names[port_id][type_id][item_id].c_str();
    }

    /**
     * Updates the state of a controller button or other input item.
     */
    void controller_port_type_item_set_state(unsigned int port_id, unsigned int type_id, unsigned int item_id, int16_t state) {
        if (controller_ports_item_state.size() <= port_id) {
            return;
        }

        if (controller_ports_item_state[port_id].size() <= type_id) {
            return;
        }

        if (controller_ports_item_state[port_id][type_id].size() <= item_id) {
            return;
        }

        controller_ports_item_state[port_id][type_id][item_id] = state;
    }

    /**
     * Retrieves a register listing.
     */
    void get_registers(void* data);

    /**
     * Updates the register listing.
     */
    void set_registers(void* data);

    /**
     * Reads an 8-bit word of memory at the given address.
     */
    uint8_t read_memory8(uint64_t address);

    /**
     * Writes an 8-bit word of memory at the given address.
     */
    void write_memory8(uint64_t address, uint8_t value);

    /**
     * Reads a 16-bit word of memory at the given address.
     */
    uint16_t read_memory16(uint64_t address);

    /**
     * Writes a 16-bit word of memory at the given address.
     */
    void write_memory16(uint64_t address, uint16_t value);

    /**
     * Reads a 32-bit word of memory at the given address.
     */
    uint32_t read_memory32(uint64_t address);

    /**
     * Writes a 32-bit word of memory at the given address.
     */
    void write_memory32(uint64_t address, uint32_t value);
}
